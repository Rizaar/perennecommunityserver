/*
 * Copyright (C) 2004-2017 L2J Server
 * 
 * This file is part of L2J Server.
 * 
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.custom.quests.messages;

/**
 * @author 17062
 */
public class MessageQuest
{
	/**
	 * @return the _sendBySystem
	 */
	public int get_sendBySystem()
	{
		return _sendBySystem;
	}
	
	/**
	 * @param _sendBySystem the _sendBySystem to set
	 */
	public void set_sendBySystem(int _sendBySystem)
	{
		this._sendBySystem = _sendBySystem;
	}
	
	/**
	 * @return the level
	 */
	public int getLevel()
	{
		return level;
	}
	
	/**
	 * @return the classes
	 */
	public int getClasses()
	{
		return classes;
	}
	
	/**
	 * @return the _senderId
	 */
	public int get_senderId()
	{
		return _senderId;
	}
	
	/**
	 * @return the _subject
	 */
	public String get_subject()
	{
		return _subject;
	}
	
	/**
	 * @return the _content
	 */
	public String get_content()
	{
		return _content;
	}
	
	/**
	 * @return the _expiration
	 */
	public long get_expiration()
	{
		return _expiration;
	}
	
	private final int level;
	private final int classes;
	private final int _senderId;
	private final String _subject;
	private final String _content;
	private final long _expiration;
	private int _sendBySystem = 0;
	private final String _attachement;
	
	/**
	 * @return the _attachement
	 */
	public String get_attachement()
	{
		return _attachement;
	}
	
	public enum SendBySystem
	{
		PLAYER,
		NEWS,
		NONE,
		ALEGRIA
	}
	
	public MessageQuest(int level, int classes, String _subject, String _content, String attachement)
	{
		this.level = level;
		this.classes = classes;
		this._senderId = -1;
		this._subject = _subject;
		this._content = _content;
		this._expiration = 360;
		this._attachement = attachement;
		_sendBySystem = 1;
	}
	
}
