/*
 * Copyright (C) 2004-2017 L2J Server
 * 
 * This file is part of L2J Server.
 * 
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.custom.quests.messages;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.gameserver.instancemanager.MailManager;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.entity.Message;
import com.l2jserver.gameserver.model.entity.Message.SendBySystem;

/**
 * @author Rizaar
 */
public class MessageManager
{
	private static final Logger LOG = LoggerFactory.getLogger(MessageManager.class);
	private static final MessageManagerParser messageParser = new MessageManagerParser();
	
	protected MessageManager()
	{
		init();
	}
	
	/**
	 * singleton de la classe, qui gérera toute l'économie du serveur
	 * @return
	 */
	public static MessageManager getInstance()
	{
		return SingletonHolder._instance;
	}
	
	/**
	 * classe pour gérer les singleton
	 * @author Rizaar
	 */
	private static class SingletonHolder
	{
		protected static final MessageManager _instance = new MessageManager();
	}
	
	/**
	 * initialise la classe
	 */
	private static void init()
	{
		messageParser.load();
	}
	
	public static void checkForQuest(L2PcInstance player)
	{
		for (MessageQuest msg : messageParser.getLevelTable())
		{
			if ((msg.getLevel() <= player.getLevel()) && player.getPerennePlayer().getVariableTemporaire(msg.get_subject()).equalsIgnoreCase("0"))
			{
				Message message = new Message(player.getObjectId(), msg.get_subject(), msg.get_content(), SendBySystem.NEWS);
				message.createAttachments();
				for (String s : msg.get_attachement().split(";"))
				{
					Integer id = Integer.parseInt(s.split("-")[0]);
					Long nb = Long.parseLong(s.split("-")[1]);
					message.getAttachments().addItem("", id, nb, null, null);
				}
				MailManager.getInstance().sendMessage(message);
				
				player.getPerennePlayer().addOrUpdateVariableTemporaire(msg.get_subject(), "1");
			}
		}
	}
	
}
