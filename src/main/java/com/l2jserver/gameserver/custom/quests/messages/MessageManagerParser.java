/*
 * Copyright (C) 2004-2017 L2J Server
 * 
 * This file is part of L2J Server.
 * 
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.custom.quests.messages;

import java.util.ArrayList;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import com.l2jserver.util.data.xml.IXmlReader;

/**
 * @author 17062
 */
public class MessageManagerParser implements IXmlReader
{
	private final ArrayList<MessageQuest> _levelTable = new ArrayList<>();
	
	/*
	 * (non-Javadoc)
	 * @see com.l2jserver.util.data.xml.IXmlReader#load()
	 */
	@Override
	public void load()
	{
		_levelTable.clear();
		parseDatapackFile("data/stats/questMessage.xml");
		LOG.info("{}: Loaded {} Custom data mail", getClass().getSimpleName(), _levelTable.size());
	}
	
	@Override
	public void parseDocument(Document doc)
	{
		final Node table = doc.getFirstChild();
		LOG.info("test messageManager");
		for (Node n = table.getFirstChild(); n != null; n = n.getNextSibling())
		{
			if ("quete".equals(n.getNodeName()))
			{
				NamedNodeMap attrs = n.getAttributes();
				MessageQuest message = new MessageQuest(parseInteger(attrs, "level"), parseInteger(attrs, "classes"), parseString(attrs, "titre"), n.getTextContent(), parseString(attrs, "attachement"));
				_levelTable.add(message);
			}
		}
	}
	
	public ArrayList<MessageQuest> getLevelTable()
	{
		return _levelTable;
	}
	
}
