/*
 * Copyright (C) 2004-2017 L2J Server
 * 
 * This file is part of L2J Server.
 * 
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.custom.economie.ville.batiment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.Future;

import com.l2jserver.Config;
import com.l2jserver.gameserver.ThreadPoolManager;
import com.l2jserver.gameserver.custom.economie.ville.BatimentInstance;
import com.l2jserver.gameserver.custom.economie.ville.GardeInstance;
import com.l2jserver.gameserver.custom.economie.ville.VilleInstance;
import com.l2jserver.gameserver.instancemanager.MailManager;
import com.l2jserver.gameserver.model.entity.Message;
import com.l2jserver.gameserver.model.entity.Message.SendBySystem;

/**
 * @author Rizaar
 */
public class PosteDeGarde extends BatimentInstance
{
	protected Future<?> _posteDeGarde = null;
	protected Future<?> _reportMatin = null;
	protected Future<?> _reportSoir = null;
	
	private final int production = 1;
	private Calendar _nextModeChange = null;
	private Calendar _nextModeChangeMorningReport = null;
	private Calendar _nextModeChangeEveningReport = null;
	
	/**
	 * @param id
	 * @param nom
	 * @param level
	 * @param ville
	 */
	public PosteDeGarde(int id, BatimentType nom, int level, VilleInstance ville)
	{
		super(id, nom, level, ville);
		_log.info("initialisation du post de garde de la ville de " + ville.getNom());
		scheduleModeChange();
		scheduleMorningReport();
		scheduleEveningReport();
	}
	
	private final void scheduleModeChange()
	{
		// Calculate next mode change
		_nextModeChange = Calendar.getInstance();
		_nextModeChange.set(Calendar.SECOND, 0);
		_nextModeChange.set(Calendar.HOUR_OF_DAY, Config.CUSTOM_VILLE_PRODUCTION_TIME_HOUR);
		_nextModeChange.set(Calendar.MINUTE, Config.CUSTOM_VILLE_PRODUCTION_TIME_MINUTE);
		if (_nextModeChange.before(Calendar.getInstance()))
		{
			_nextModeChange.add(Calendar.DATE, 1);
		}
		// Schedule mode change
		_posteDeGarde = ThreadPoolManager.getInstance().scheduleGeneral(this::productionGarde, (_nextModeChange.getTimeInMillis() - System.currentTimeMillis()));
	}
	
	private final void scheduleMorningReport()
	{
		// Calculate next mode change
		_nextModeChangeMorningReport = Calendar.getInstance();
		_nextModeChangeMorningReport.set(Calendar.SECOND, 0);
		_nextModeChangeMorningReport.set(Calendar.HOUR_OF_DAY, 7);
		_nextModeChangeMorningReport.set(Calendar.MINUTE, 0);
		if (_nextModeChangeMorningReport.before(Calendar.getInstance()))
		{
			_nextModeChangeMorningReport.add(Calendar.DATE, 1);
		}
		// Schedule mode change
		_reportMatin = ThreadPoolManager.getInstance().scheduleGeneral(this::reportMatin, (_nextModeChangeMorningReport.getTimeInMillis() - System.currentTimeMillis()));
	}
	
	private final void scheduleEveningReport()
	{
		// Calculate next mode change
		_nextModeChangeEveningReport = Calendar.getInstance();
		_nextModeChangeEveningReport.set(Calendar.SECOND, 0);
		_nextModeChangeEveningReport.set(Calendar.HOUR_OF_DAY, 19);
		_nextModeChangeEveningReport.set(Calendar.MINUTE, 0);
		if (_nextModeChangeEveningReport.before(Calendar.getInstance()))
		{
			_nextModeChangeEveningReport.add(Calendar.DATE, 1);
		}
		// Schedule mode change
		_reportSoir = ThreadPoolManager.getInstance().scheduleGeneral(this::reportMatin, (_nextModeChangeEveningReport.getTimeInMillis() - System.currentTimeMillis()));
	}
	
	public void productionGarde()
	{
		try
		{
			if (_posteDeGarde == null)
			{
				return;
			}
			this.ville.addGardeCount(production);
		}
		catch (Exception e)
		{
			_log.warn("{}: There has been a problem running the follow task!", getClass().getSimpleName(), e);
		}
	}
	
	public void reportMatin()
	{
		try
		{
			if (_reportMatin == null)
			{
				return;
			}
			ArrayList<GardeInstance> list = ville.getGardeList();
			int nbGardeTot = list.size();
			ArrayList<GardeInstance> toDelete = new ArrayList<>();
			for (GardeInstance g : list)
			{
				if (g.getGarde() == null)
				{
					toDelete.add(g);
				}
			}
			ville.getGardeList().removeAll(toDelete);
			int newNbGarde = ville.getGardeList().size();
			int nbGuardesMissings = nbGardeTot - newNbGarde;
			Date d = new Date();
			Message message = new Message(this.ville.getOwner().getLeader().getObjectId(), "Rapport des effectifs de la garde", nbGuardesMissings + " manquent à l'appel. Ce qui porte l'effectif total de la garde à " + newNbGarde + "\r\n date du rapport: " + d.toString(), SendBySystem.NEWS);
			MailManager.getInstance().sendMessage(message);
		}
		catch (Exception e)
		{
			_log.warn("{}: There has been a problem running the follow task!", getClass().getSimpleName(), e);
		}
	}
	
	public void reportSoir()
	{
		try
		{
			if (_reportSoir == null)
			{
				return;
			}
			int nbGuardesMissings = 0;
			ArrayList<GardeInstance> list = ville.getGardeList();
			for (GardeInstance g : list)
			{
				if (g.getGarde() != null)
				{
					continue;
				}
				ville.getGardeList().remove(g);
			}
			int nbGuardesTotal = this.getVille().getGardeList().size();
			nbGuardesMissings = this.getVille().getEffectifGardeReleve() - nbGuardesTotal;
			_log.info("Rapport de garde de " + ville.getNom() + " : " + nbGuardesMissings + " gardes manquants");
			Date d = new Date();
			Message message = new Message(this.ville.getOwner().getLeader().getObjectId(), "Rapport des effectifs de la garde", nbGuardesMissings + " manquent à l'appel. Ce qui porte l'effectif total de la garde à " + nbGuardesTotal + "\r\n date du rapport: " + d.toString(), SendBySystem.NEWS);
			MailManager.getInstance().sendMessage(message);
		}
		catch (Exception e)
		{
			_log.warn("{}: There has been a problem running the follow task!", getClass().getSimpleName(), e);
		}
	}
}
