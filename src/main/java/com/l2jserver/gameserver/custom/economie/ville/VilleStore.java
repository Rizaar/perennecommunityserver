/*
 * Copyright (C) 2004-2017 L2J Server
 * 
 * This file is part of L2J Server.
 * 
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.custom.economie.ville;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.pool.impl.ConnectionFactory;
import com.l2jserver.gameserver.data.sql.impl.ClanTable;
import com.l2jserver.gameserver.instancemanager.CastleManager;
import com.l2jserver.gameserver.model.Location;

/**
 * @author Rizaar
 */
public class VilleStore
{
	private static final String QUERY_GET_GARDE_LIST = "SELECT * FROM perenne_garde WHERE id_ville = ?";
	private static final String QUERY_UPDATE_GARDE_LIST = "UPDATE perenne_garde SET position_x=?,position_y=?,position_z=?,point=? WHERE id=? && id_ville=?";
	private static final String QUERY_INSERT_GARDE_LIST = "REPLACE INTO `perenne_garde`(`id`, `template_id`, `id_ville`, `position_x`, `position_y`, `position_z`, `point`) VALUES (?,?,?,?,?,?,?)";
	private static final String QUERY_DELETE_GARDE = "DELETE FROM perenne_garde WHERE id=? && id_ville=?";
	private static final String QUERY_GET_BATIMENT_LIST = "SELECT * FROM perenne_batiment WHERE id = ?";
	private static final String QUERY_UPDATE_BATIMENT_LIST = "UPDATE `perenne_batiment` SET `nom`=?,`level`=?,`ville`=? WHERE id=?";
	private static final String QUERY_INSERT_BATIMENT_LIST = "INSERT INTO `perenne_batiment`(`id`, `nom`, `level`, `ville`) VALUES (?,?,?,?)";
	private static final String QUERY_GET_VILLE = "SELECT * FROM perenne_ville WHERE id = ?";
	private static final String QUERY_UPDATE_VILLE = "UPDATE `perenne_ville` SET `id`=?,`nom`=?,`garde_prod_max`=?,`garde_prod_queu`=?,`garde_current_item`=?,`dirigeant`=?,`effectif_garde_releve`=? WHERE id=?";
	private static final String QUERY_GET_VILLE_ALL = "SELECT * FROM perenne_ville";
	
	private static final Logger LOG = LoggerFactory.getLogger(VilleStore.class);
	
	/**
	 * @param ville
	 * @return
	 */
	public static ArrayList<GardeInstance> getGardeByCity(VilleInstance ville)
	{
		ArrayList<GardeInstance> tab = new ArrayList<>();
		try (Connection con = ConnectionFactory.getInstance().getConnection();
			PreparedStatement ps = con.prepareStatement(QUERY_GET_GARDE_LIST))
		{
			// Retrieve all skills of this L2PcInstance from the database
			ps.setInt(1, ville.getId());
			try (ResultSet rs = ps.executeQuery())
			{
				while (rs.next())
				{
					tab.add(new GardeInstance(rs.getInt("id"), rs.getInt("template_id"), ville, new Location(rs.getInt("position_x"), rs.getInt("position_y"), rs.getInt("position_z")), rs.getString("point")));
				}
			}
		}
		catch (Exception e)
		{
			LOG.error("Could not retreive info for perenne ville garde : {}", e);
		}
		return tab;
	}
	
	/**
	 * @param idVille
	 */
	public static void updateGardeByCity(int idVille)
	{
		try (Connection con = ConnectionFactory.getInstance().getConnection())
		{
			for (GardeInstance g : Ville.getVilleFromId(idVille).getGardeList())
			{
				try (PreparedStatement ps = con.prepareStatement(QUERY_UPDATE_GARDE_LIST))
				{
					ps.setInt(5, g.getId());
					ps.setInt(6, g.getVille().getId());
					ps.setInt(1, g.getGarde().getX());
					ps.setInt(2, g.getGarde().getY());
					ps.setInt(3, g.getGarde().getZ());
					ps.setString(4, g.getPoint());
					ps.execute();
				}
			}
		}
		catch (Exception e)
		{
			LOG.error("Error could not update garde for this city :" + idVille, e);
		}
	}
	
	public static void updateGardeByCity(VilleInstance ville)
	{
		updateGardeByCity(ville.getId());
	}
	
	/**
	 * @param garde
	 */
	public static void createThisGardInBdd(GardeInstance garde)
	{
		try (Connection con = ConnectionFactory.getInstance().getConnection();
			PreparedStatement ps = con.prepareStatement(QUERY_INSERT_GARDE_LIST))
		{
			ps.setInt(1, garde.getId());
			ps.setInt(2, garde.getGarde().getId());
			ps.setInt(3, garde.getVille().getId());
			ps.setInt(4, garde.getPosition().getX());
			ps.setInt(5, garde.getPosition().getY());
			ps.setInt(6, garde.getPosition().getZ());
			ps.setString(7, garde.getPoint());
			ps.execute();
		}
		catch (Exception e)
		{
			LOG.error("Error could not insert garde for this id :" + garde.getId() + " : with template ID: " + garde.getGarde().getId(), garde.getId(), e);
		}
	}
	
	public static void deleteThisGardeInBdd(GardeInstance garde)
	{
		LOG.info("Garde to delete :" + garde.getId() + " : " + garde.getGarde().isDead());
		try (Connection con = ConnectionFactory.getInstance().getConnection();
			PreparedStatement ps = con.prepareStatement(QUERY_DELETE_GARDE))
		{
			ps.setInt(1, garde.getId());
			ps.setInt(2, garde.getVille().getId());
			ps.execute();
		}
		catch (Exception e)
		{
			LOG.error("Error could not insert garde for this id :" + garde.getId() + " : with template ID: " + garde.getGarde().getId(), garde.getId(), e);
		}
	}
	
	/**
	 * @param idVille
	 */
	public static void updateBatimentByCity(int idVille)
	{
		try (Connection con = ConnectionFactory.getInstance().getConnection())
		{
			for (BatimentInstance g : Ville.getVilleFromId(idVille).getBatimentList())
			{
				try (PreparedStatement ps = con.prepareStatement(QUERY_UPDATE_BATIMENT_LIST))
				{
					ps.setString(1, g.getNom().toString());// nom
					ps.setInt(2, g.getLevel());// level
					ps.setInt(3, g.getVille().getId());// ville
					ps.setInt(4, g.getId());// id
					ps.execute();
				}
			}
		}
		catch (Exception e)
		{
			LOG.error("Error could not update garde for this city :" + idVille, e);
		}
	}
	
	/**
	 * @param batiment
	 */
	public static void createThisBatimentInBdd(BatimentInstance batiment)
	{
		try (Connection con = ConnectionFactory.getInstance().getConnection();
			PreparedStatement ps = con.prepareStatement(QUERY_INSERT_BATIMENT_LIST))
		{
			ps.setInt(1, batiment.getId());
			ps.setString(2, batiment.getNom().toString());
			ps.setInt(3, batiment.getLevel());
			ps.setInt(4, batiment.getVille().getId());
			ps.execute();
		}
		catch (Exception e)
		{
			LOG.error("Error could not insert batiment for this id :" + batiment.getId() + " : with template ID: " + batiment.getNom().toString(), e);
		}
	}
	
	/**
	 * @param idVille
	 * @return
	 */
	public static VilleInstance getVilleById(int idVille)
	{
		VilleInstance ville = null;
		try (Connection con = ConnectionFactory.getInstance().getConnection();
			PreparedStatement ps = con.prepareStatement(QUERY_GET_VILLE))
		{
			// Retrieve all skills of this L2PcInstance from the database
			ps.setInt(1, idVille);
			try (ResultSet rs = ps.executeQuery())
			{
				while (rs.next())
				{
					ville = new VilleInstance(rs.getInt("id"), rs.getString("nom"), rs.getInt("garde_prod_max"), rs.getInt("garde_prod_queu"), rs.getInt("garde_current_item"), null, CastleManager.getInstance().getCastleById(idVille), ClanTable.getInstance().getClan(rs.getInt("dirigeant")), rs.getInt("effectif_garde_releve"));
				}
			}
		}
		catch (Exception e)
		{
			LOG.error("Could not retreive info for perenne ville garde : {}", e);
		}
		return ville;
	}
	
	public static ArrayList<VilleInstance> getVilles()
	{
		ArrayList<VilleInstance> villes = new ArrayList<>();
		try (Connection con = ConnectionFactory.getInstance().getConnection();
			PreparedStatement ps = con.prepareStatement(QUERY_GET_VILLE_ALL))
		{
			// Retrieve all skills of this L2PcInstance from the database
			try (ResultSet rs = ps.executeQuery())
			{
				while (rs.next())
				{
					villes.add(new VilleInstance(rs.getInt("id"), rs.getString("nom"), rs.getInt("garde_prod_max"), rs.getInt("garde_prod_queu"), rs.getInt("garde_current_item"), null, null, ClanTable.getInstance().getClan(rs.getInt("dirigeant")), rs.getInt("effectif_garde_releve")));
					
				}
			}
		}
		catch (Exception e)
		{
			LOG.error("Could not retreive info for perenne ville garde : {}", e);
		}
		return villes;
	}
	
	/**
	 * private static final String QUERY_UPDATE_VILLE = "UPDATE `perenne_ville` SET `nom`=?,`garde_prod_max`=?,`garde_prod_queu`=?,`garde_current_item`=?,`dirigeant`=? WHERE id=?";
	 */
	public static void updateCity()
	{
		try (Connection con = ConnectionFactory.getInstance().getConnection())
		{
			for (VilleInstance v : Ville.getVilles())
			{
				try (PreparedStatement ps = con.prepareStatement(QUERY_UPDATE_VILLE))
				{
					ps.setInt(1, v.getId());
					ps.setString(2, v.getNom());
					ps.setInt(3, v.getGardeProdMax());
					ps.setInt(4, v.getGardeProdQueu());
					ps.setInt(5, v.getCurrentGardeItem());
					ps.setInt(6, v.getOwner() != null ? v.getOwner().getId() : -1);
					ps.setInt(7, v.getEffectifGardeReleve());
					ps.setInt(8, v.getId());
					ps.execute();
				}
			}
		}
		catch (Exception e)
		{
			LOG.error("Error could not update city ", e);
		}
	}
	
	public static void updateCity(VilleInstance v)
	{
		try (Connection con = ConnectionFactory.getInstance().getConnection())
		{
			try (PreparedStatement ps = con.prepareStatement(QUERY_UPDATE_VILLE))
			{
				ps.setInt(1, v.getId());
				ps.setString(2, v.getNom());
				ps.setInt(3, v.getGardeProdMax());
				ps.setInt(4, v.getGardeProdQueu());
				ps.setInt(5, v.getCurrentGardeItem());
				ps.setInt(6, v.getOwner() != null ? v.getOwner().getId() : -1);
				ps.setInt(7, v.getEffectifGardeReleve());
				ps.setInt(8, v.getId());
				ps.execute();
			}
		}
		catch (Exception e)
		{
			LOG.error("Error could not update city ", e);
		}
	}
	
	/**
	 * @return the queryGetGardeList
	 */
	public static String getQueryGetGardeList()
	{
		return QUERY_GET_GARDE_LIST;
	}
	
	/**
	 * @return the querySetGardeList
	 */
	public static String getQuerySetGardeList()
	{
		return QUERY_UPDATE_GARDE_LIST;
	}
	
	/**
	 * @return the queryInsertGardeList
	 */
	public static String getQueryInsertGardeList()
	{
		return QUERY_INSERT_GARDE_LIST;
	}
	
	/**
	 * @return the queryGetBatimentList
	 */
	public static String getQueryGetBatimentList()
	{
		return QUERY_GET_BATIMENT_LIST;
	}
	
	/**
	 * @return the querySetBatimentList
	 */
	public static String getQuerySetBatimentList()
	{
		return QUERY_UPDATE_BATIMENT_LIST;
	}
	
	/**
	 * @return the queryInsertBatimentList
	 */
	public static String getQueryInsertBatimentList()
	{
		return QUERY_INSERT_BATIMENT_LIST;
	}
	
	/**
	 * @return the queryGetVille
	 */
	public static String getQueryGetVille()
	{
		return QUERY_GET_VILLE;
	}
	
	/**
	 * @return the querySetVille
	 */
	public static String getQuerySetVille()
	{
		return QUERY_UPDATE_VILLE;
	}
	
	/**
	 * @return the queryGetVilleAll
	 */
	public static String getQueryGetVilleAll()
	{
		return QUERY_GET_VILLE_ALL;
	}
	
}