/*
 * Copyright (C) 2004-2017 L2J Server
 * 
 * This file is part of L2J Server.
 * 
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.custom.economie.hotelDesVentes;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.gameserver.custom.economie.ville.VilleStore;

/**
 * @author 17062
 */
public class ObjetAVendre
{
	private long prix;
	private final long prixUnit;
	private long taxe;
	private int id;
	private int villeId;
	private int itemId;
	private long quantite;
	private int vendeurId;
	private String vendeurName;
	private int enchantLevel;
	private Map<Integer, Integer> elements = new HashMap<>();
	private static final Logger LOG = LoggerFactory.getLogger(VilleStore.class);
	
	public ObjetAVendre(long prix, int id, int marchandId, int itemId, long quantite, int vendeurId, String vendeurName, int enchantLevel, Map<Integer, Integer> elements)
	{
		this.prix = prix;
		this.prixUnit = prix / quantite;
		this.id = id;
		this.villeId = marchandId;
		this.itemId = itemId;
		this.quantite = quantite;
		this.vendeurId = vendeurId;
		this.vendeurName = vendeurName;
		this.enchantLevel = enchantLevel;
		this.elements = elements;
	}
	
	/**
	 * @return the prix
	 */
	public long getPrix()
	{
		return prix;
	}
	
	/**
	 * @param prix the prix to set
	 */
	public void setPrix(long prix)
	{
		this.prix = prix;
	}
	
	/**
	 * @return the item ObjectId
	 */
	public int getId()
	{
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(int id)
	{
		this.id = id;
	}
	
	/**
	 * @return the villeId
	 */
	public int getVilleId()
	{
		return villeId;
	}
	
	/**
	 * @param villeId the villeId to set
	 */
	public void setVilleId(int villeId)
	{
		this.villeId = villeId;
	}
	
	/**
	 * @return the itemId
	 */
	public int getItemId()
	{
		return itemId;
	}
	
	/**
	 * @param itemId the itemId to set
	 */
	public void setItemId(int itemId)
	{
		this.itemId = itemId;
	}
	
	/**
	 * @return the quantite
	 */
	public long getQuantite()
	{
		return quantite;
	}
	
	/**
	 * @param quantite the quantite to set
	 */
	public void setQuantite(long quantite)
	{
		this.quantite = quantite;
	}
	
	/**
	 * @return the vendeurId
	 */
	public int getVendeurId()
	{
		return vendeurId;
	}
	
	/**
	 * @param vendeurId the vendeurId to set
	 */
	public void setVendeurId(int vendeurId)
	{
		this.vendeurId = vendeurId;
	}
	
	/**
	 * @return the vendeurName
	 */
	public String getVendeurName()
	{
		return vendeurName;
	}
	
	/**
	 * @param vendeurName the vendeurName to set
	 */
	public void setVendeurName(String vendeurName)
	{
		this.vendeurName = vendeurName;
	}
	
	/**
	 * @return the taxe
	 */
	public long getTaxe()
	{
		return taxe;
	}
	
	/**
	 * @param taxe the taxe to set
	 */
	public void setTaxe(long taxe)
	{
		this.taxe = taxe;
	}
	
	/**
	 * @return the enchantLevel
	 */
	public int getEnchantLevel()
	{
		return enchantLevel;
	}
	
	/**
	 * @param enchantLevel the enchantLevel to set
	 */
	public void setEnchantLevel(int enchantLevel)
	{
		this.enchantLevel = enchantLevel;
	}
	
	public long getPrixUnit()
	{
		return prixUnit;
	}
	
	public Map<Integer, Integer> getElements()
	{
		return elements;
	}
	
	public void setElements(Map<Integer, Integer> elements)
	{
		this.elements = elements;
	}
	
}
