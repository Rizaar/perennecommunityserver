/*
 * Copyright (C) 2004-2017 L2J Server
 * 
 * This file is part of L2J Server.
 * 
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.custom.economie.ville;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.gameserver.ai.AbstractAI;
import com.l2jserver.gameserver.custom.economie.ville.batiment.BatimentType;

/**
 * @author Rizaar
 */
public class BatimentInstance
{
	protected int id;
	protected BatimentType nom;
	protected int level;
	public final VilleInstance ville;
	public static final Logger _log = LoggerFactory.getLogger(AbstractAI.class);
	
	protected BatimentInstance(int id, BatimentType nom, int level, VilleInstance ville)
	{
		this.id = id;
		this.nom = nom;
		this.level = level;
		this.ville = ville;
	}
	
	/**
	 * @return the id
	 */
	public int getId()
	{
		return id;
	}
	
	/**
	 * @return the nom
	 */
	public BatimentType getNom()
	{
		return nom;
	}
	
	/**
	 * @return the level
	 */
	public int getLevel()
	{
		return level;
	}
	
	/**
	 * @return the ville
	 */
	public VilleInstance getVille()
	{
		return ville;
	}
	
	public int incLevel()
	{
		return ++level;
	}
}
