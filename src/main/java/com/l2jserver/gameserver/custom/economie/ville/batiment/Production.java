/*
 * Copyright (C) 2004-2017 L2J Server
 * 
 * This file is part of L2J Server.
 * 
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.custom.economie.ville.batiment;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.Future;

import com.l2jserver.Config;
import com.l2jserver.gameserver.ThreadPoolManager;
import com.l2jserver.gameserver.custom.economie.ville.BatimentInstance;
import com.l2jserver.gameserver.custom.economie.ville.VilleInstance;
import com.l2jserver.gameserver.instancemanager.MailManager;
import com.l2jserver.gameserver.model.entity.Message;
import com.l2jserver.gameserver.model.entity.Message.SendBySystem;

/**
 * @author Rizaar
 */
public class Production extends BatimentInstance
{
	private final int[][] excavationMiniere =
	{
		{
			0
		},
		{
			1869, // iron Ore
			1870, // Coal
			1871 // charcoal
		},
		{
			1873, // Silver Nugget
			1874, // Oriharukon Ore
			1880, // Steel
			1876 // Mithril Ore
		},
		{
			1879, // codes
			1877, // Adamantite Nugget
			1890, // Mithril Alloy
			1895 // metallic fiber
		
		}
		
	};
	
	private final int[][] agricultureEtElevate =
	{
		{
			0
		},
		{
			1895, // animal Skin
			1872, // animal bone
			1864, // stem
			1868// thread
		},
		{
			1881, // coarse Bone Powder
			1882, // Leather
			1865, // Varnish
			1878 // braided hemp
		},
		{
			1894, // crafted Leather
			1884, // cord
			1889, // compound braid
			5550 // DMP
		}
	};
	private final int[][] exploitationZoneRisque =
	{
		{
			0
		},
		{
			4043, // Asofe
			4042, // Enria
			4044, // Thons
			1866 // suede
		},
		{
			4039, // Mold glud
			4041, // Mold Hardener
			4040, // Mold Lubricant
			1875 // Stone Of Purity
		},
		{
			1887, // Varnish of Purity
			5549, // Metallic Thread
			1885, // Hight-grad Sued
			1888 // Synthetic Cokes
		}
	};
	
	private final int amicaleDesOuvriersMine = 0; // adamantine
	private final int amicaleDesOuvriersFerme = 0; // leonard
	private final int amicaleDesOuvriersDanger = 0; // Orichalcum
	
	protected Future<?> _posteDeGarde = null;
	protected Future<?> _report = null;
	private final int production = 1;
	private Calendar _nextModeChange = null;
	private Calendar _nextModeChangeMorningReport = null;
	
	/**
	 * @param id
	 * @param nom
	 * @param level
	 * @param ville
	 */
	public Production(int id, BatimentType nom, int level, VilleInstance ville)
	{
		super(id, nom, level, ville);
		_log.info("initialisation de la production de la ville de " + ville.getNom());
		scheduleModeChange();
		scheduleMorningReport();
	}
	
	private final void scheduleModeChange()
	{
		// Calculate next mode change
		_nextModeChange = Calendar.getInstance();
		_nextModeChange.set(Calendar.SECOND, 0);
		_nextModeChange.set(Calendar.HOUR_OF_DAY, Config.CUSTOM_VILLE_PRODUCTION_TIME_HOUR);
		_nextModeChange.set(Calendar.MINUTE, Config.CUSTOM_VILLE_PRODUCTION_TIME_MINUTE);
		if (_nextModeChange.before(Calendar.getInstance()))
		{
			_nextModeChange.add(Calendar.DATE, 1);
		}
		// Schedule mode change
		ThreadPoolManager.getInstance().scheduleGeneral(this::productionRessource, (_nextModeChange.getTimeInMillis() - System.currentTimeMillis()));
	}
	
	private final void scheduleMorningReport()
	{
		// Calculate next mode change
		_nextModeChangeMorningReport = Calendar.getInstance();
		_nextModeChangeMorningReport.set(Calendar.SECOND, 0);
		_nextModeChangeMorningReport.set(Calendar.HOUR_OF_DAY, 7);
		_nextModeChangeMorningReport.set(Calendar.MINUTE, 0);
		if (_nextModeChangeMorningReport.before(Calendar.getInstance()))
		{
			_nextModeChangeMorningReport.add(Calendar.DATE, 1);
		}
		// Schedule mode change
		ThreadPoolManager.getInstance().scheduleGeneral(this::productionRessource, (_nextModeChangeMorningReport.getTimeInMillis() - System.currentTimeMillis()));
	}
	
	public void productionRessource()
	{
		try
		{
			if (_posteDeGarde == null)
			{
				return;
			}
			giveDaylyProduction();
		}
		catch (Exception e)
		{
			_log.warn("{}: There has been a problem running the follow task!", getClass().getSimpleName(), e);
		}
	}
	
	public void report()
	{
		try
		{
			if (_report == null)
			{
				return;
			}
			Date d = new Date();
			giveDaylyProduction();
			Message message = new Message(this.ville.getOwner().getLeader().getObjectId(), "Production", "Votre production vient d'être livrée", SendBySystem.NEWS);
			MailManager.getInstance().sendMessage(message);
		}
		catch (Exception e)
		{
			_log.warn("{}: There has been a problem running the follow task!", getClass().getSimpleName(), e);
		}
	}
	
	private void addItemToWarehouse(int itemId, long number)
	{
		this.ville.getOwner().getWarehouse().addItem("", itemId, number, this.ville.getOwner().getLeader().getPlayerInstance(), null);
	}
	
	private void giveDaylyProduction()
	{
		for (int i : agricultureEtElevate[this.ville.getSkills().getAgricultureEtElevage()])
		{
			addItemToWarehouse(i, Config.CUSTOM_VILLE_PRODUCTION_PALIER.get(this.ville.getSkills().getAgricultureEtElevage() * (this.ville.getSkills().isContreMaitriseAvencee() ? 2 : 1)));
		}
		for (int i : excavationMiniere[this.ville.getSkills().getExcavationMiniere()])
		{
			addItemToWarehouse(i, Config.CUSTOM_VILLE_PRODUCTION_PALIER.get(this.ville.getSkills().getExcavationMiniere()) * (this.ville.getSkills().isContreMaitriseAvencee() ? 2 : 1));
		}
		for (int i : exploitationZoneRisque[this.ville.getSkills().getZoneARisque()])
		{
			addItemToWarehouse(i, Config.CUSTOM_VILLE_PRODUCTION_PALIER.get(this.ville.getSkills().getZoneARisque()) * (this.ville.getSkills().isContreMaitriseAvencee() ? 2 : 1));
		}
		if (this.ville.getSkills().isAmicalDesOuvriers())
		{
			if (this.ville.getSkills().getAgricultureEtElevage() == 3)
			{
				addItemToWarehouse(amicaleDesOuvriersFerme, Config.CUSTOM_VILLE_PRODUCTION_PALIER.get(this.ville.getSkills().getZoneARisque()) * (this.ville.getSkills().isContreMaitriseAvencee() ? 2 : 1));
			}
			if (this.ville.getSkills().getExcavationMiniere() == 3)
			{
				addItemToWarehouse(amicaleDesOuvriersMine, Config.CUSTOM_VILLE_PRODUCTION_PALIER.get(this.ville.getSkills().getZoneARisque()) * (this.ville.getSkills().isContreMaitriseAvencee() ? 2 : 1));
			}
			if (this.ville.getSkills().getZoneARisque() == 3)
			{
				addItemToWarehouse(amicaleDesOuvriersDanger, Config.CUSTOM_VILLE_PRODUCTION_PALIER.get(this.ville.getSkills().getZoneARisque()) * (this.ville.getSkills().isContreMaitriseAvencee() ? 2 : 1));
			}
		}
	}
}
