/*
 * Copyright (C) 2004-2017 L2J Server
 * 
 * This file is part of L2J Server.
 * 
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.custom.economie.recuperateur;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.gameserver.custom.economie.hotelDesVentes.HotelDesVentesSaver;
import com.l2jserver.gameserver.custom.economie.ville.VilleInstance;
import com.l2jserver.gameserver.model.Location;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2RecuperateurInstance;

/**
 * @author 17062
 */
public class Recuperateur
{
	private L2Npc Recuperateur;
	private VilleInstance ville;
	private static final Logger LOG = LoggerFactory.getLogger(Recuperateur.class);
	
	public Recuperateur(VilleInstance ville)
	{
		final Logger LOG = LoggerFactory.getLogger(Recuperateur.class);
		this.ville = ville;
		init(ville);
		if (ville.getSkills().getRecuperation() > 0)
		{
			spawnRecuperateur();
		}
		LOG.info("Chargement du recuperateur pour la ville de " + this.ville.getNom() + " terminé");
	}
	
	private void init(VilleInstance ville)
	{
		this.Recuperateur = new L2RecuperateurInstance(HotelDesVentesSaver.getIdNpcRecuperateur(ville));
	}
	
	private void spawnRecuperateur()
	{
		Location a = HotelDesVentesSaver.getLocalisationRecuperateur(ville);
		Recuperateur.spawnMe(a.getX(), a.getY(), a.getZ());
	}
	
	public Location localisationRecuperateur()
	{
		return HotelDesVentesSaver.getLocalisationRecuperateur(ville);
	}
	
	/**
	 * @return the vendeur
	 */
	public L2Npc getVendeur()
	{
		return Recuperateur;
	}
	
	/**
	 * @param vendeur the vendeur to set
	 */
	public void setVendeur(L2Npc vendeur)
	{
		this.Recuperateur = vendeur;
	}
	
	/**
	 * @return the ville
	 */
	public VilleInstance getVille()
	{
		return ville;
	}
	
	/**
	 * @param ville the ville to set
	 */
	public void setVille(VilleInstance ville)
	{
		this.ville = ville;
	}
}
