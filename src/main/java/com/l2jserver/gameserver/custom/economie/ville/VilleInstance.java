/*
 * Copyright (C) 2004-2017 L2J Server
 * 
 * This file is part of L2J Server.
 * 
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.custom.economie.ville;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.Config;
import com.l2jserver.gameserver.ThreadPoolManager;
import com.l2jserver.gameserver.custom.clan.ClanPerenne;
import com.l2jserver.gameserver.custom.economie.Economie;
import com.l2jserver.gameserver.custom.economie.hotelDesVentes.HotelDesVentes;
import com.l2jserver.gameserver.custom.economie.recuperateur.Recuperateur;
import com.l2jserver.gameserver.data.sql.impl.ClanTable;
import com.l2jserver.gameserver.data.xml.impl.NpcData;
import com.l2jserver.gameserver.instancemanager.MailManager;
import com.l2jserver.gameserver.model.L2Clan;
import com.l2jserver.gameserver.model.actor.instance.L2MonsterInstance;
import com.l2jserver.gameserver.model.entity.Castle;
import com.l2jserver.gameserver.model.entity.Message;
import com.l2jserver.gameserver.model.entity.Message.SendBySystem;
import com.l2jserver.gameserver.model.items.instance.L2ItemInstance;
import com.l2jserver.gameserver.util.Util;

/**
 * @author Rizaar
 */

public class VilleInstance
{
	private static final int[] id_item_gardes =
	{
		50000,
		60000,
		60001,
		60002,
		60003,
		60004,
		60005,
		60006,
		60007,
		60008,
		60009,
		60010,
		60011,
		60012,
		60013,
		60014,
		60015,
		60016
	};
	
	protected Future<?> _posteDeGarde = null;
	protected Future<?> _reportMorning = null;
	protected Future<?> _reportEvening = null;
	private final int production = 1;
	private Calendar _nextModeChange = null;
	private Calendar _nextModeChangeMorningReport = null;
	private Calendar _nextModeChangeEveningReport = null;
	private static final Logger LOG = LoggerFactory.getLogger(Ville.class);
	private int id;
	private String nom;
	private int gardeProdMax;
	private int gardeProdQueu;
	private int currentGardeItem;
	private int currentGardeItemInGame;
	
	private ArrayList<GardeInstance> gardeList = new ArrayList<>();
	private int effectifGardeReleve;
	private ArrayList<BatimentInstance> batimentAction = new ArrayList<>();
	private Castle chateau;
	private L2Clan dirigeant;
	
	private HotelDesVentes hotelDesVentes;
	private Recuperateur recuperateur;
	private ClanPerenne skills;
	
	private final int[][] excavationMiniere =
	{
		{
			0
		},
		{
			1869, // iron Ore
			1870, // Coal
			1871 // charcoal
		},
		{
			1873, // Silver Nugget
			1874, // Oriharukon Ore
			1880, // Steel
			1876 // Mithril Ore
		},
		{
			1879, // codes
			1877, // Adamantite Nugget
			1890, // Mithril Alloy
			1895 // metallic fiber
		
		}
		
	};
	
	private final int[][] agricultureEtElevage =
	{
		{
			0
		},
		{
			1895, // animal Skin
			1872, // animal bone
			1864, // stem
			1868// thread
		},
		{
			1881, // coarse Bone Powder
			1882, // Leather
			1865, // Varnish
			1878 // braided hemp
		},
		{
			1894, // crafted Leather
			1884, // cord
			1889, // compound braid
			5550 // DMP
		}
	};
	private final int[][] exploitationZoneRisque =
	{
		{
			0
		},
		{
			4043, // Asofe
			4042, // Enria
			4044, // Thons
			1866 // suede
		},
		{
			4039, // Mold glud
			4041, // Mold Hardener
			4040, // Mold Lubricant
			1875 // Stone Of Purity
		},
		{
			1887, // Varnish of Purity
			5549, // Metallic Thread
			1885, // Hight-grad Sued
			1888 // Synthetic Cokes
		}
	};
	
	private final int amicaleDesOuvriersMine = 0; // adamantine
	private final int amicaleDesOuvriersFerme = 0; // leonard
	private final int amicaleDesOuvriersDanger = 0; // Orichalcum
	
	protected Future<?> _production = null;
	protected Future<?> _reportProd = null;
	private Calendar _nextModeChangeProd = null;
	private Calendar _nextModeChangeMorningReportProd = null;
	
	VilleInstance(int id, String nom, int gardeProdMax, int gardeProdQueu, int currentGardeItem, ArrayList<GardeInstance> gardeList, Castle chateau, L2Clan dirigeant, int effectifGardeReleve)
	{
		this.id = id;
		this.nom = nom;
		this.gardeProdMax = gardeProdMax;
		this.gardeProdQueu = gardeProdQueu;
		this.currentGardeItem = currentGardeItem;
		this.gardeList = gardeList;
		this.chateau = chateau;
		this.dirigeant = dirigeant;
		if (dirigeant != null)
		{
			setSkills(dirigeant.getSkillClan());
		}
		else
		{
			setSkills(new ClanPerenne());
		}
		this.setEffectifGardeReleve(effectifGardeReleve);
	}
	
	VilleInstance(int id)
	{
		VilleInstance ville = VilleStore.getVilleById(id);
		LOG.info("initialisation de " + ville.getNom());
		this.id = ville.id;
		this.nom = ville.nom;
		this.gardeProdMax = ville.gardeProdMax;
		this.gardeProdQueu = ville.gardeProdQueu;
		this.currentGardeItem = ville.currentGardeItem;
		this.gardeList = ville.gardeList;
		this.chateau = ville.chateau;
		if (ville.dirigeant != null)
		{
			this.dirigeant = ville.dirigeant;
			this.skills = dirigeant.getSkillClan();
		}
		else
		{
			this.dirigeant = ClanTable.getInstance().getClan(-1);
			this.skills = new ClanPerenne();
		}
		batimentAction();
		this.gardeList = VilleStore.getGardeByCity(this);
		spawnGards();
		this.hotelDesVentes = new HotelDesVentes(this);
		this.recuperateur = new Recuperateur(this);
	}
	
	/**
	 * @return the id
	 */
	public int getId()
	{
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(int id)
	{
		this.id = id;
	}
	
	/**
	 * @return the nom
	 */
	public String getNom()
	{
		return nom;
	}
	
	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom)
	{
		this.nom = nom;
	}
	
	/**
	 * @return the gardeProdQueu
	 */
	public int getGardeProdQueu()
	{
		return gardeProdQueu;
	}
	
	/**
	 * @param gardeProdQueu the gardeProdQueu to set
	 */
	public void setGardeProdQueu(int gardeProdQueu)
	{
		this.gardeProdQueu = gardeProdQueu;
	}
	
	/**
	 * @return the currentGardeItem
	 */
	public int getCurrentGardeItem()
	{
		return currentGardeItem;
	}
	
	/**
	 * @param currentGardeItem the currentGardeItem to set
	 */
	public void setCurrentGardeItem(int currentGardeItem)
	{
		this.currentGardeItem = currentGardeItem;
	}
	
	/**
	 * @return the gardeProdMax
	 */
	public int getGardeProdMax()
	{
		return gardeProdMax;
	}
	
	/**
	 * @param gardeProdMax the gardeProdMax to set
	 */
	public void setGardeProdMax(int gardeProdMax)
	{
		this.gardeProdMax = gardeProdMax;
	}
	
	/**
	 * @return the gardeList
	 */
	public ArrayList<GardeInstance> getGardeList()
	{
		return gardeList;
	}
	
	/**
	 * @param gardeList the gardeList to set
	 */
	public void setGardeList(ArrayList<GardeInstance> gardeList)
	{
		this.gardeList = gardeList;
	}
	
	/**
	 * @return the batimentList
	 */
	public ArrayList<BatimentInstance> getBatimentList()
	{
		return batimentAction;
	}
	
	/**
	 * @param batimentList the batimentList to set
	 */
	public void setBatimentList(ArrayList<BatimentInstance> batimentList)
	{
		this.batimentAction = batimentList;
	}
	
	/**
	 * @return the castel
	 */
	public Castle getCastel()
	{
		return chateau;
	}
	
	/**
	 * @param castel the castel to set
	 */
	public void setCastel(Castle castel)
	{
		this.chateau = castel;
	}
	
	/**
	 * @return the owner
	 */
	public L2Clan getOwner()
	{
		return dirigeant;
	}
	
	/**
	 * @param owner the owner to set
	 */
	public void setOwner(L2Clan owner)
	{
		this.dirigeant = owner;
	}
	
	public void addGardeCount(int count)
	{
		int gardeItems = 0;
		for (L2ItemInstance i : dirigeant.getLeader().getPlayerInstance().getInventory().getItems())
		{
			if (Util.contains(id_item_gardes, i.getId()))
			{
				gardeItems++;
			}
		}
		
		LOG.info("nombres d'objet de garde " + (count + currentGardeItem + gardeItems + gardeList.size()) + "  | nombres de gardes max : " + gardeProdMax);
		currentGardeItem = Math.min(count + currentGardeItem + gardeItems + gardeList.size(), gardeProdMax);
	}
	
	public int subGardeCount(int count)
	{
		currentGardeItem = Math.min((currentGardeItem) - count, 0);
		return currentGardeItem;
	}
	
	/**
	 * @return the effectifGardeReleve
	 */
	public int getEffectifGardeReleve()
	{
		return gardeList.size();
	}
	
	/**
	 * @param effectifGardeReleve the effectifGardeReleve to set
	 */
	public void setEffectifGardeReleve(int effectifGardeReleve)
	{
		this.effectifGardeReleve = effectifGardeReleve;
	}
	
	public void checkCurrentGardItem(int itemId)
	{
		int count = 0;
		count += Economie.getItemCountFromBdd(itemId);
		count += Economie.getItemCountOnGround(itemId);
		currentGardeItemInGame = count;
	}
	
	/**
	 * @return the currentGardeItemInGame
	 */
	public int getCurrentGardeItemInGame()
	{
		return currentGardeItemInGame;
	}
	
	/**
	 * @param currentGardeItemInGame the currentGardeItemInGame to set
	 */
	public void setCurrentGardeItemInGame(int currentGardeItemInGame)
	{
		this.currentGardeItemInGame = currentGardeItemInGame;
	}
	
	public HotelDesVentes getHotelDesVentes()
	{
		return hotelDesVentes;
	}
	
	public void setHotelDesVentes(HotelDesVentes hotelDesVentes)
	{
		this.hotelDesVentes = hotelDesVentes;
	}
	
	public ClanPerenne getSkills()
	{
		return skills;
	}
	
	public void setSkills(ClanPerenne skills)
	{
		this.skills = skills;
	}
	
	/**
	 * @param id
	 * @param nom
	 * @param level
	 * @param ville
	 */
	public void batimentAction()
	{
		LOG.info("initialisation du post de garde de la ville de " + this.getNom());
		scheduleModeChange();
		scheduleEveningReport();
		scheduleMorningReport();
		LOG.info("initialisation de la production de la ville de " + this.getNom());
		scheduleMorningReportProd();
		scheduleProduction();
	}
	
	private final void scheduleProduction()
	{
		LOG.info("programation de la production de garde");
		// Calculate next mode change
		_nextModeChange = Calendar.getInstance();
		_nextModeChange.set(Calendar.SECOND, 0);
		_nextModeChange.set(Calendar.HOUR_OF_DAY, Config.CUSTOM_VILLE_PRODUCTION_TIME_HOUR);
		_nextModeChange.set(Calendar.MINUTE, Config.CUSTOM_VILLE_PRODUCTION_TIME_MINUTE);
		_nextModeChange.add(Calendar.DATE, 1);
		// Schedule mode change
		_posteDeGarde = ThreadPoolManager.getInstance().scheduleGeneral(this::productionGarde, (_nextModeChange.getTimeInMillis() - System.currentTimeMillis()));
	}
	
	private final void scheduleMorningReport()
	{
		LOG.info("programation du raport matinal de la garde");
		// Calculate next mode change
		_nextModeChangeMorningReport = Calendar.getInstance();
		_nextModeChangeMorningReport.set(Calendar.SECOND, 0);
		_nextModeChangeMorningReport.set(Calendar.HOUR_OF_DAY, 7);
		_nextModeChangeMorningReport.set(Calendar.MINUTE, 0);
		_nextModeChangeMorningReport.add(Calendar.DATE, 1);
		// Schedule mode change
		_reportMorning = ThreadPoolManager.getInstance().scheduleGeneral(this::reportMorning, (_nextModeChangeMorningReport.getTimeInMillis() - System.currentTimeMillis()));
	}
	
	private final void scheduleEveningReport()
	{
		LOG.info("programation du raport du soir de la garde");
		// Calculate next mode change
		_nextModeChangeEveningReport = Calendar.getInstance();
		_nextModeChangeEveningReport.set(Calendar.SECOND, 0);
		_nextModeChangeEveningReport.set(Calendar.HOUR_OF_DAY, 19);
		_nextModeChangeEveningReport.set(Calendar.MINUTE, 0);
		_nextModeChangeEveningReport.add(Calendar.DATE, 1);
		// Schedule mode change
		_reportEvening = ThreadPoolManager.getInstance().scheduleGeneral(this::reportEvening, (_nextModeChangeEveningReport.getTimeInMillis() - System.currentTimeMillis()));
	}
	
	public void productionGarde()
	{
		try
		{
			this.addGardeCount(production);
			scheduleProduction();
		}
		catch (Exception e)
		{
			LOG.warn("{}: There has been a problem running the follow task!", getClass().getSimpleName(), e);
		}
	}
	
	public void reportMorning()
	{
		try
		{
			if (_reportMorning == null)
			{
				return;
			}
			int nbGuardesMissings = 0;
			ArrayList<GardeInstance> list = this.getGardeList();
			for (GardeInstance g : list)
			{
				if (g.getGarde() != null)
				{
					continue;
				}
				this.getGardeList().remove(g);
			}
			int nbGuardesTotal = getGardeList().size();
			nbGuardesMissings = this.getEffectifGardeReleve() - nbGuardesTotal;
			LOG.info("Rapport de garde de " + this.getNom() + " : " + nbGuardesMissings + " gardes manquants");
			Date d = new Date();
			Message message = new Message(this.getOwner().getLeader().getObjectId(), "Rapport des effectifs de la garde", nbGuardesMissings + " manquent à l'appel. Ce qui porte l'effectif total de la garde à " + nbGuardesTotal + "\r\n date du rapport: " + d.toString(), SendBySystem.NEWS);
			MailManager.getInstance().sendMessage(message);
			scheduleMorningReport();
		}
		catch (Exception e)
		{
			LOG.warn("{}: There has been a problem running the follow task!", getClass().getSimpleName(), e);
		}
	}
	
	public void reportEvening()
	{
		try
		{
			if (_reportMorning == null)
			{
				return;
			}
			int nbGuardesMissings = 0;
			ArrayList<GardeInstance> list = this.getGardeList();
			for (GardeInstance g : list)
			{
				if (g.getGarde() != null)
				{
					continue;
				}
				this.getGardeList().remove(g);
			}
			int nbGuardesTotal = getGardeList().size();
			nbGuardesMissings = this.getEffectifGardeReleve() - nbGuardesTotal;
			LOG.info("Rapport de garde de " + this.getNom() + " : " + nbGuardesMissings + " gardes manquants");
			Date d = new Date();
			Message message = new Message(this.getOwner().getLeader().getObjectId(), "Rapport des effectifs de la garde", nbGuardesMissings + " manquent à l'appel. Ce qui porte l'effectif total de la garde à " + nbGuardesTotal + "\r\n date du rapport: " + d.toString(), SendBySystem.NEWS);
			MailManager.getInstance().sendMessage(message);
			scheduleEveningReport();
		}
		catch (Exception e)
		{
			LOG.warn("{}: There has been a problem running the follow task!", getClass().getSimpleName(), e);
		}
	}
	
	private final void scheduleModeChange()
	{
		LOG.info("Programation de la production");
		// Calculate next mode change
		_nextModeChangeProd = Calendar.getInstance();
		_nextModeChangeProd.set(Calendar.SECOND, 0);
		_nextModeChangeProd.set(Calendar.HOUR_OF_DAY, Config.CUSTOM_VILLE_PRODUCTION_TIME_HOUR);
		_nextModeChangeProd.set(Calendar.MINUTE, Config.CUSTOM_VILLE_PRODUCTION_TIME_MINUTE);
		_nextModeChangeProd.add(Calendar.DATE, 1);
		// Schedule mode change
		_production = ThreadPoolManager.getInstance().scheduleGeneral(this::productionRessource, (_nextModeChangeProd.getTimeInMillis() - System.currentTimeMillis()));
		
	}
	
	private final void scheduleMorningReportProd()
	{
		LOG.info("Programation du raport de la production");
		// Calculate next mode change
		_nextModeChangeMorningReportProd = Calendar.getInstance();
		_nextModeChangeMorningReportProd.set(Calendar.SECOND, 0);
		_nextModeChangeMorningReportProd.set(Calendar.HOUR_OF_DAY, 7);
		_nextModeChangeMorningReportProd.set(Calendar.MINUTE, 0);
		_nextModeChangeMorningReport.add(Calendar.DATE, 1);
		// Schedule mode change
		_reportProd = ThreadPoolManager.getInstance().scheduleGeneral(this::reportProd, (_nextModeChangeMorningReport.getTimeInMillis() - System.currentTimeMillis()));
	}
	
	public void productionRessource()
	{
		try
		{
			if (_posteDeGarde == null)
			{
				return;
			}
			giveDaylyProduction();
			scheduleModeChange();
		}
		catch (Exception e)
		{
			LOG.warn("{}: There has been a problem running the follow task!", getClass().getSimpleName(), e);
		}
	}
	
	public void reportProd()
	{
		try
		{
			if (_reportProd == null)
			{
				return;
			}
			Date d = new Date();
			giveDaylyProduction();
			Message message = new Message(this.getOwner().getLeader().getObjectId(), "Production", "Votre production vient d'être livrée", SendBySystem.NEWS);
			MailManager.getInstance().sendMessage(message);
			scheduleMorningReportProd();
		}
		catch (Exception e)
		{
			LOG.warn("{}: There has been a problem running the follow task!", getClass().getSimpleName(), e);
		}
	}
	
	private void addItemToWarehouse(int itemId, long number)
	{
		this.getOwner().getWarehouse().addItem("", itemId, number, this.getOwner().getLeader().getPlayerInstance(), null);
	}
	
	private void giveDaylyProduction()
	{
		for (int i : agricultureEtElevage[this.getSkills().getAgricultureEtElevage()])
		{
			addItemToWarehouse(i, Config.CUSTOM_VILLE_PRODUCTION_PALIER.get(this.getSkills().getAgricultureEtElevage() * (this.getSkills().isContreMaitriseAvencee() ? 2 : 1)));
		}
		for (int i : excavationMiniere[this.getSkills().getExcavationMiniere()])
		{
			addItemToWarehouse(i, Config.CUSTOM_VILLE_PRODUCTION_PALIER.get(this.getSkills().getExcavationMiniere()) * (this.getSkills().isContreMaitriseAvencee() ? 2 : 1));
		}
		for (int i : exploitationZoneRisque[this.getSkills().getZoneARisque()])
		{
			addItemToWarehouse(i, Config.CUSTOM_VILLE_PRODUCTION_PALIER.get(this.getSkills().getZoneARisque()) * (this.getSkills().isContreMaitriseAvencee() ? 2 : 1));
		}
		if (this.getSkills().isAmicalDesOuvriers())
		{
			if (this.getSkills().getAgricultureEtElevage() == 3)
			{
				addItemToWarehouse(amicaleDesOuvriersFerme, Config.CUSTOM_VILLE_PRODUCTION_PALIER.get(this.getSkills().getZoneARisque()) * (this.getSkills().isContreMaitriseAvencee() ? 2 : 1));
			}
			if (this.getSkills().getExcavationMiniere() == 3)
			{
				addItemToWarehouse(amicaleDesOuvriersMine, Config.CUSTOM_VILLE_PRODUCTION_PALIER.get(this.getSkills().getZoneARisque()) * (this.getSkills().isContreMaitriseAvencee() ? 2 : 1));
			}
			if (this.getSkills().getZoneARisque() == 3)
			{
				addItemToWarehouse(amicaleDesOuvriersDanger, Config.CUSTOM_VILLE_PRODUCTION_PALIER.get(this.getSkills().getZoneARisque()) * (this.getSkills().isContreMaitriseAvencee() ? 2 : 1));
			}
		}
	}
	
	public Recuperateur getRecuperateur()
	{
		return recuperateur;
	}
	
	public void setRecuperateur(Recuperateur recuperateur)
	{
		this.recuperateur = recuperateur;
	}
	
	public void saveGuardsList()
	{
		for (GardeInstance g : gardeList)
		{
			if ((g.getGarde() != null) && (g.getGarde().isDead() == false))
			{
				VilleStore.createThisGardInBdd(g); // create or update
			}
			else
			{
				VilleStore.deleteThisGardeInBdd(g);
			}
		}
	}
	
	public void spawnGards()
	{
		for (GardeInstance g : gardeList)
		{
			L2MonsterInstance garde = new L2MonsterInstance(NpcData.getInstance().getTemplate(g.getTemplateId()));
			garde.spawnMe(g.getPosition());
			g.setGarde(garde);
		}
	}
}
