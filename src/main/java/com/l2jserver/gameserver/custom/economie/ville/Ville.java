/*
 * Copyright (C) 2004-2017 L2J Server
 * 
 * This file is part of L2J Server.
 * 
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.custom.economie.ville;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Rizaar
 */
public class Ville
{
	private static final Logger LOG = LoggerFactory.getLogger(Ville.class);
	private static ArrayList<VilleInstance> Villes = new ArrayList<>();
	
	/**
	 * @return the villes
	 */
	public static ArrayList<VilleInstance> getVilles()
	{
		return Villes;
	}
	
	/**
	 * @param villes the villes to set
	 */
	public static void setVilles(ArrayList<VilleInstance> villes)
	{
		Villes = villes;
	}
	
	protected Ville()
	{
		init();
	}
	
	/**
	 * singleton de la classe, qui gérera toutes les villes du serveur
	 * @return
	 */
	public static Ville getInstance()
	{
		return SingletonHolder._instance;
	}
	
	/**
	 * classe pour gérer les singleton
	 * @author Rizaar
	 */
	private static class SingletonHolder
	{
		protected static final Ville _instance = new Ville();
	}
	
	/**
	 * @param idVille
	 * @return
	 */
	public static VilleInstance getVilleFromId(int idVille)
	{
		for (VilleInstance v : Ville.getVilles())
		{
			if (v.getId() == idVille)
			{
				return v;
			}
		}
		return null;
	}
	
	/**
	 * @param nomVille
	 * @return
	 */
	public static VilleInstance getVilleFromName(String nomVille)
	{
		for (VilleInstance v : Ville.getVilles())
		{
			if (v.getNom().equalsIgnoreCase(nomVille))
			{
				return v;
			}
		}
		return null;
	}
	
	/**
	 * initialise la classe
	 */
	private static void init()
	{
		LOG.info("initialisation des villes:");
		Villes.add(new VilleInstance(1));
		Villes.add(new VilleInstance(2));
		Villes.add(new VilleInstance(3));
		Villes.add(new VilleInstance(4));
		Villes.add(new VilleInstance(5));
		Villes.add(new VilleInstance(6));
		Villes.add(new VilleInstance(7));
		Villes.add(new VilleInstance(8));
		Villes.add(new VilleInstance(9));
		LOG.info("Fin du chargement des villes. Nombre de villes chargées : " + Villes.size());
	}
	
}