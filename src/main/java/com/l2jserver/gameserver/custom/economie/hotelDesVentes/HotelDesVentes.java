/*
 * Copyright (C) 2004-2017 L2J Server
 * 
 * This file is part of L2J Server.
 * 
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.custom.economie.hotelDesVentes;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.gameserver.custom.clan.ClanPerenne;
import com.l2jserver.gameserver.custom.economie.ville.VilleInstance;
import com.l2jserver.gameserver.instancemanager.CastleManager;
import com.l2jserver.gameserver.instancemanager.MailManager;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.Location;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.entity.Message;
import com.l2jserver.gameserver.model.entity.Message.SendBySystem;
import com.l2jserver.gameserver.model.items.instance.L2ItemInstance;

/**
 * @author 17062
 */
public class HotelDesVentes
{
	ArrayList<ObjetAVendre> objetsAVendre = new ArrayList<>();
	private L2Npc vendeur;
	private VilleInstance ville;
	private static final Logger LOG = LoggerFactory.getLogger(HotelDesVentes.class);
	
	public HotelDesVentes(VilleInstance ville)
	{
		this.ville = ville;
		init(ville);
		if (ville.getSkills().getHotelDesVente() > 0)
		{
			spawnVendeurs();
		}
		LOG.info("Chargement de l'hotel des ventes pour la ville de " + this.ville.getNom() + " terminé");
	}
	
	private void init(VilleInstance ville)
	{
		this.vendeur = new L2Npc(HotelDesVentesSaver.getIdNpcVendeur(ville));
		this.objetsAVendre = HotelDesVentesSaver.getItemByCity(ville.getId());
	}
	
	private void spawnVendeurs()
	{
		Location a = HotelDesVentesSaver.getLocalisationVendeur(ville);
		vendeur.spawnMe(a.getX(), a.getY(), a.getZ());
	}
	
	public boolean addItemToList(ObjetAVendre item, L2PcInstance player)
	{
		ClanPerenne skills = ville.getOwner().getSkillClan();
		if (skills.getHotelDesVente() == 0)
		{
			player.sendMessage("La ville n'a pas d'hotel des Ventes");
			return false;
		}
		if (isHDVFull())
		{
			player.sendMessage("L'hotel des ventes est complet.");
			return false;
		}
		
		if (takeFee(item.getPrix(), player))
		{
			applyTax(item);
			objetsAVendre.add(item);
			player.sendMessage("Votre objet a bien été enregistré");
			return true;
		}
		return false;
	}
	
	public boolean isHDVFull()
	{
		ClanPerenne skills = ville.getOwner().getSkillClan();
		int nbItems = objetsAVendre.size();
		if ((nbItems > (50 + (skills.isStrategieCommercial() ? 50 : 0))) && (this.getVille().getSkills().getHotelDesVente() == 1))
		{
			return true;
		}
		if ((nbItems > (100 + (skills.isStrategieCommercial() ? 50 : 0))) && (this.getVille().getSkills().getHotelDesVente() == 2))
		{
			return true;
		}
		if ((nbItems > (200 + (skills.isStrategieCommercial() ? 50 : 0))) && (this.getVille().getSkills().getHotelDesVente() == 3))
		{
			return true;
		}
		return false;
	}
	
	public void subItemToList(ObjetAVendre item)
	{
		objetsAVendre.remove(item);
	}
	
	public void sellThisItem(ObjetAVendre item, L2PcInstance buyer)
	{
		subItemToList(item);
		sendItemToBuyer(item, buyer);
		sendPriceToSeller(item);
	}
	
	public void applyTax(ObjetAVendre item)
	{
		item.setTaxe((item.getPrix() * ((ville.getCastel().getTaxPercent() / 100) + 1)));
	}
	
	public void sendPriceToSeller(ObjetAVendre item)
	{
		int id = L2World.getInstance().getPlayer(item.getVendeurName()).getObjectId();
		Message message = new Message(id, "Hotel des Ventes", "L'article que vous avez mis en vente chez nous à été vendue. Apprès application des taxes, voici ce qui vous revient de droit.", SendBySystem.PLAYER);
		L2ItemInstance a = new L2ItemInstance(57);
		a.setCount(item.getPrix());
		message.createAttachments().addItem("", a, null, null);
		MailManager.getInstance().sendMessage(message);
	}
	
	public void sendPriceToSeller(ObjetAVendre item, Long count)
	{
		int id = L2World.getInstance().getPlayer(item.getVendeurName()).getObjectId();
		Message message = new Message(id, "Hotel des Ventes", "L'article que vous avez mis en vente chez nous à été vendue. Apprès application des taxes, voici ce qui vous revient de droit.", SendBySystem.PLAYER);
		L2ItemInstance a = new L2ItemInstance(57);
		a.setCount(item.getPrixUnit() * count);
		message.createAttachments().addItem("", a, null, null);
		MailManager.getInstance().sendMessage(message);
	}
	
	public void sendTaxeToCastle(ObjetAVendre item)
	{
		ville.getOwner().getWarehouse().addItem("", 57, item.getTaxe(), null, null);
	}
	
	public void sendTaxeToCastle(ObjetAVendre item, Long count)
	{
		ville.getOwner().getWarehouse().addItem("", 57, item.getTaxe() * (count / item.getQuantite()), null, null);
		this.ville.getHotelDesVentes().getObjetsAVendre().remove(item);
		item.setTaxe(item.getTaxe() * ((item.getQuantite() - count) / item.getQuantite()));
		this.ville.getHotelDesVentes().getObjetsAVendre().add(item);
	}
	
	public void sendItemToSeller(ObjetAVendre item, L2PcInstance player)
	{
		Message message = new Message(player.getObjectId(), "Hotel des Ventes", "Voici l'article que vous avez demandé à retirer.", SendBySystem.PLAYER);
		L2ItemInstance a = new L2ItemInstance(item.getItemId());
		a.setEnchantLevel(item.getEnchantLevel());
		a.setCount(item.getQuantite());
		for (int key : item.getElements().keySet())
		{
			a.setElementAttr((byte) key, item.getElements().get(key));
		}
		message.createAttachments().addItem("", a, null, null);
		MailManager.getInstance().sendMessage(message);
	}
	
	public void sendPriceToBuyer(ObjetAVendre item, L2PcInstance buyer)
	{
		Message message = new Message(item.getVendeurId(), "Hotel des Ventes", "L'article que vous avez mis en vente chez nous à été vendue. Apprès application des taxes, voici ce qui vous revient de droit.", SendBySystem.PLAYER);
		message.createAttachments().addItem("", 57, item.getPrix(), null, null);
		MailManager.getInstance().sendMessage(message);
	}
	
	public void sendItemToBuyer(ObjetAVendre item, L2PcInstance buyer)
	{
		Message message = new Message(buyer.getObjectId(), "Hotel des Ventes", "Voici l'article dont vous avez fait l'aquisition.", SendBySystem.PLAYER);
		L2ItemInstance a = new L2ItemInstance(item.getItemId());
		a.setEnchantLevel(item.getEnchantLevel());
		a.setCount(item.getQuantite());
		for (int key : item.getElements().keySet())
		{
			a.setElementAttr((byte) key, item.getElements().get(key));
		}
		message.createAttachments().addItem("", a, null, null);
		MailManager.getInstance().sendMessage(message);
	}
	
	public void sendItemToBuyer(ObjetAVendre item, L2PcInstance buyer, Long count)
	{
		Message message = new Message(buyer.getObjectId(), "Hotel des Ventes", "Voici l'article dont vous avez fait l'aquisition.", SendBySystem.PLAYER);
		L2ItemInstance a = new L2ItemInstance(item.getItemId());
		a.setEnchantLevel(item.getEnchantLevel());
		a.setCount(count);
		for (int key : item.getElements().keySet())
		{
			a.setElementAttr((byte) key, item.getElements().get(key));
		}
		message.createAttachments().addItem("", a, null, null);
		MailManager.getInstance().sendMessage(message);
		this.ville.getHotelDesVentes().getObjetsAVendre().remove(item);
		item.setQuantite(item.getQuantite() - count);
		this.ville.getHotelDesVentes().getObjetsAVendre().add(item);
	}
	
	/**
	 * @param fee le cout de la mise en vente
	 * @param player le joueur qui met en vente l'objet
	 * @return boolean reponse si oui ou non le joueur peut vendre la somme
	 */
	public boolean takeFee(long fee, L2PcInstance player)
	{
		fee = (long) (fee * (CastleManager.getInstance().getCastleById(ville.getOwner().getCastleId()).getTaxRate()));
		if (player.getAdena() > fee)
		{
			player.reduceAdena("", fee, null, true);
			ville.getOwner().getWarehouse().addItem("", 57, fee, player, vendeur);
			return true;
		}
		return false;
	}
	
	public Location localisationVendeur()
	{
		return HotelDesVentesSaver.getLocalisationVendeur(ville);
	}
	
	/**
	 * @return the objetsAVendre
	 */
	public ArrayList<ObjetAVendre> getObjetsAVendre()
	{
		return objetsAVendre;
	}
	
	/**
	 * @param objetsAVendre the objetsAVendre to set
	 */
	public void setObjetsAVendre(ArrayList<ObjetAVendre> objetsAVendre)
	{
		this.objetsAVendre = objetsAVendre;
	}
	
	/**
	 * @return the vendeur
	 */
	public L2Npc getVendeur()
	{
		return vendeur;
	}
	
	/**
	 * @param vendeur the vendeur to set
	 */
	public void setVendeur(L2Npc vendeur)
	{
		this.vendeur = vendeur;
	}
	
	/**
	 * @return the ville
	 */
	public VilleInstance getVille()
	{
		return ville;
	}
	
	/**
	 * @param ville the ville to set
	 */
	public void setVille(VilleInstance ville)
	{
		this.ville = ville;
	}
	
	public void saveHotelDesVentes()
	{
		for (ObjetAVendre i : objetsAVendre)
		{
			HotelDesVentesSaver.saveObjectInBdd(i);
			HotelDesVentesSaver.saveElementInBdd(i);
		}
	}
}
