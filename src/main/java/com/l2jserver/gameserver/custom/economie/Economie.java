/*
 * Copyright (C) 2004-2017 L2J Server
 * 
 * This file is part of L2J Server.
 * 
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.custom.economie;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.Config;
import com.l2jserver.commons.database.pool.impl.ConnectionFactory;
import com.l2jserver.gameserver.instancemanager.AuctionManager;
import com.l2jserver.gameserver.instancemanager.CastleManager;
import com.l2jserver.gameserver.instancemanager.ItemsOnGroundManager;
import com.l2jserver.gameserver.model.actor.L2Attackable;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.entity.Auction;
import com.l2jserver.gameserver.model.entity.Auction.Bidder;
import com.l2jserver.gameserver.model.entity.Castle;
import com.l2jserver.gameserver.model.holders.ItemHolder;
import com.l2jserver.gameserver.model.items.instance.L2ItemInstance;
import com.l2jserver.util.Rnd;

/**
 * @author Rizaar
 */
public class Economie
{
	private static final Logger LOG = LoggerFactory.getLogger(Economie.class);
	
	private static long _adenaCount;
	private static Map<Integer, Long> _CorrespondancePrix = new HashMap<>();
	private final static int ADENA_ID = 57;
	private static String COUNT_ITEM = "SELECT count,owner_id FROM items WHERE item_id=?";
	private static String COUNT_ITEM_ON_GROUND = "SELECT count FROM itemsonground WHERE item_id=?";
	private static String CHECK_GM_STATUS = "SELECT accesslevel FROM characters WHERE charId=?";
	static List<Integer> dropListPlante = new ArrayList<>();
	static List<Integer> dropListUndead = new ArrayList<>();
	static List<Integer> dropListGiant = new ArrayList<>();
	static List<Integer> dropListPixy = new ArrayList<>();
	static List<Integer> dropListSpirit = new ArrayList<>();
	static List<Integer> dropListDeamon = new ArrayList<>();
	static List<Integer> dropListDragons = new ArrayList<>();
	static List<Integer> dropListAnge = new ArrayList<>();
	static List<Integer> dropListMagic = new ArrayList<>();
	static List<Integer> dropListBeast = new ArrayList<>();
	static List<Integer> dropListInsect = new ArrayList<>();
	static List<Integer> dropListAnimaux = new ArrayList<>();
	
	protected Economie()
	{
		init();
	}
	
	/**
	 * singleton de la classe, qui gérera toute l'économie du serveur
	 * @return
	 */
	public static Economie getInstance()
	{
		return SingletonHolder._instance;
	}
	
	/**
	 * classe pour gérer les singleton
	 * @author Rizaar
	 */
	private static class SingletonHolder
	{
		protected static final Economie _instance = new Economie();
	}
	
	/**
	 * initialise la classe
	 */
	private static void init()
	{
		LOG.info("initialisation de l'économie");
		getAndUpdateAdenaCount();
		pupulateCorrespondancePrix();
		dropListAnge();
		dropListAnimaux();
		dropListBeast();
		dropListDeamon();
		dropListDragons();
		dropListGiant();
		dropListInsect();
		dropListPixy();
		dropListPlantes();
		dropListSpirit();
		dropListUndead();
		dropListMagic();
	}
	
	/**
	 * Calcule le montant d'Adena présent dans le monde
	 * @return
	 */
	private static long getWorldAdena()
	{
		long count = 0L;
		count += getItemCountFromBdd(ADENA_ID);
		count += getItemCountOnGround(ADENA_ID);
		count += getCastleVaultAdenaCount();
		count += getAdenaBidder();
		return count;
	}
	
	public static long getAdenaCount()
	{
		return getWorldAdena();
	}
	
	public static long getAndUpdateAdenaCount()
	{
		_adenaCount = getWorldAdena();
		return _adenaCount;
	}
	
	public static void pupulateCorrespondancePrix()
	{
		_CorrespondancePrix.put(1864, 50L); // <!-- Stem -->
		_CorrespondancePrix.put(1865, 100L); // <!-- Varnish -->
		_CorrespondancePrix.put(1866, 150L); // <!-- Suede -->
		_CorrespondancePrix.put(1867, 75L); // <!-- Animal Skin -->
		_CorrespondancePrix.put(1868, 50L); // <!-- Thread -->
		_CorrespondancePrix.put(1869, 100L); // <!-- Iron Ore -->
		_CorrespondancePrix.put(1870, 100L); // <!-- Coal -->
		_CorrespondancePrix.put(1871, 100L); // <!-- Charcoal -->
		_CorrespondancePrix.put(1872, 75L); // <!-- Animal Bone -->
		_CorrespondancePrix.put(1873, 250L); // <!-- Silver Nugget -->
		_CorrespondancePrix.put(1874, 1500L); // <!-- Oriharukon Ore -->
		_CorrespondancePrix.put(1875, 1500L); // <!-- Stone of Purity -->
		_CorrespondancePrix.put(1876, 500L); // <!-- Mithril Ore -->
		_CorrespondancePrix.put(1877, 2500L); // <!-- Adamantite Nugget -->
		_CorrespondancePrix.put(1878, 250L); // <!-- Braided Hemp -->
		_CorrespondancePrix.put(1879, 600L); // <!-- Cokes -->
		_CorrespondancePrix.put(1880, 1000L); // <!-- Steel -->
		_CorrespondancePrix.put(1881, 750L); // <!-- Coarse Bone Powder -->
		_CorrespondancePrix.put(1882, 450L); // <!-- Leather -->
		_CorrespondancePrix.put(1884, 162L); // <!-- Cord -->
		_CorrespondancePrix.put(1885, 1700L); // <!-- High-Grade Suede -->
		_CorrespondancePrix.put(1887, 4050L); // <!-- Varnish of Purity -->
		_CorrespondancePrix.put(1888, 3300L); // <!-- Synthetic Cokes -->
		_CorrespondancePrix.put(1889, 1500L); // <!-- Compound Braid -->
		_CorrespondancePrix.put(1890, 6550L); // <!-- Mithril Alloy -->
		_CorrespondancePrix.put(1893, 12300L); // <!-- Oriharukon -->
		_CorrespondancePrix.put(1894, 2850L); // <!-- Crafted Leather -->
		_CorrespondancePrix.put(1895, 350L); // <!-- Metallic Fiber -->
		_CorrespondancePrix.put(4039, 3000L); // <!-- Mold Glue -->
		_CorrespondancePrix.put(4040, 5000L); // <!-- Mold Lubricant -->
		_CorrespondancePrix.put(4041, 11500L); // <!-- Mold Hardener -->
		_CorrespondancePrix.put(4042, 6000L); // <!-- Enria -->
		_CorrespondancePrix.put(4043, 3000L); // <!-- Asofe -->
		_CorrespondancePrix.put(4044, 3000L); // <!-- Thons -->
		_CorrespondancePrix.put(5549, 1000L); // <!-- Metallic Thread -->
		_CorrespondancePrix.put(5550, 7500L); // <!-- Durable Metal Plate -->
		_CorrespondancePrix.put(9628, 12750L); // <!-- Leonard -->
		_CorrespondancePrix.put(9629, 23000L); // <!-- Adamantine -->
		_CorrespondancePrix.put(9630, 19000L); // <!-- Orichalcum -->
	}
	
	public static void dropListPlantes()
	{
		
		dropListPlante.add(1864);// <!-- Stem -->
		dropListPlante.add(1868);// <!-- Thread -->
		dropListPlante.add(1871);// <!-- Charcoal -->
		dropListPlante.add(1878);// <!-- Braided Hemp -->
		dropListPlante.add(1884);// <!-- Cord -->
		dropListPlante.add(1889);// <!-- Compound Braid -->
	}
	
	public static void dropListUndead()
	{
		dropListUndead.add(1872);// <!-- Animal Bone -->
		dropListUndead.add(1881);// <!-- Coarse Bone Powder -->
		dropListUndead.add(4042);// <!-- Enria -->
		dropListUndead.add(4043);// <!-- Asofe -->
		dropListUndead.add(4044);// <!-- Thons -->
	}
	
	public static void dropListGiant()
	{
		
		dropListGiant.add(1865);// <!-- Varnish -->
		dropListGiant.add(1873);// <!-- Silver Nugget -->
		dropListGiant.add(1874);// <!-- Oriharukon Ore -->
		dropListGiant.add(1876);// <!-- Mithril Ore -->
		dropListGiant.add(1877);// <!-- Adamantite Nugget -->
		dropListGiant.add(1880);// <!-- Steel -->
		dropListGiant.add(1869);// <!-- Iron Ore -->
		dropListGiant.add(9628);// <!-- Leonard -->
		dropListGiant.add(9629);// <!-- Adamantine -->
		dropListGiant.add(9630);// <!-- Orichalcum -->
		dropListGiant.add(5550);// <!-- Durable Metal Plate -->
	}
	
	public static void dropListPixy()
	{
		
		dropListPixy.add(1866);// <!-- Suede -->
		dropListPixy.add(1885);// <!-- High-Grade Suede -->
		dropListPixy.add(1875);// <!-- Stone of Purity -->
		dropListPixy.add(4042);// <!-- Enria -->
		dropListPixy.add(4043);// <!-- Asofe -->
		dropListPixy.add(4044);// <!-- Thons -->
	}
	
	public static void dropListSpirit()
	{
		
		dropListSpirit.add(4039);// <!-- Mold Glue -->
		dropListSpirit.add(4040);// <!-- Mold Lubricant -->
		dropListSpirit.add(4041);// <!-- Mold Hardener -->
		dropListSpirit.add(1874);// <!-- Oriharukon Ore -->
	}
	
	public static void dropListDeamon()
	{
		
		dropListDeamon.add(1864);// <!-- Stem -->
		dropListDeamon.add(1865);// <!-- Varnish -->
		dropListDeamon.add(1866);// <!-- Suede -->
		dropListDeamon.add(1867);// <!-- Animal Skin -->
		dropListDeamon.add(1868);// <!-- Thread -->
		dropListDeamon.add(1869);// <!-- Iron Ore -->
		dropListDeamon.add(1870);// <!-- Coal -->
		dropListDeamon.add(1871);// <!-- Charcoal -->
		dropListDeamon.add(1872);// <!-- Animal Bone -->
		dropListDeamon.add(1873);// <!-- Silver Nugget -->
		dropListDeamon.add(1874);// <!-- Oriharukon Ore -->
		dropListDeamon.add(1875);// <!-- Stone of Purity -->
		dropListDeamon.add(1876);// <!-- Mithril Ore -->
		dropListDeamon.add(1877);// <!-- Adamantite Nugget -->
		dropListDeamon.add(1878);// <!-- Braided Hemp -->
		dropListDeamon.add(1879);// <!-- Cokes -->
		dropListDeamon.add(1880);// <!-- Steel -->
		dropListDeamon.add(1881);// <!-- Coarse Bone Powder -->
		dropListDeamon.add(1882);// <!-- Leather -->
		dropListDeamon.add(1884);// <!-- Cord -->
		dropListDeamon.add(1885);// <!-- High-Grade Suede -->
		dropListDeamon.add(1887);// <!-- Varnish of Purity -->
		dropListDeamon.add(1888);// <!-- Synthetic Cokes -->
		dropListDeamon.add(1889);// <!-- Compound Braid -->
		dropListDeamon.add(1890);// <!-- Mithril Alloy -->
		dropListDeamon.add(1893);// <!-- Oriharukon -->
		dropListDeamon.add(1894);// <!-- Crafted Leather -->
		dropListDeamon.add(1895);// <!-- Metallic Fiber -->
		dropListDeamon.add(4039);// <!-- Mold Glue -->
		dropListDeamon.add(4040);// <!-- Mold Lubricant -->
		dropListDeamon.add(4041);// <!-- Mold Hardener -->
		dropListDeamon.add(4042);// <!-- Enria -->
		dropListDeamon.add(4043);// <!-- Asofe -->
		dropListDeamon.add(4044);// <!-- Thons -->
		dropListDeamon.add(5549);// <!-- Metallic Thread -->
		dropListDeamon.add(5550);// <!-- Durable Metal Plate -->
		dropListDeamon.add(9628);// <!-- Leonard -->
		dropListDeamon.add(9629);// <!-- Adamantine -->
		dropListDeamon.add(9630);// <!-- Orichalcum -->
	}
	
	public static void dropListDragons()
	{
		
		dropListDragons.add(1864);// <!-- Stem -->
		dropListDragons.add(1865);// <!-- Varnish -->
		dropListDragons.add(1866);// <!-- Suede -->
		dropListDragons.add(1867);// <!-- Animal Skin -->
		dropListDragons.add(1868);// <!-- Thread -->
		dropListDragons.add(1869);// <!-- Iron Ore -->
		dropListDragons.add(1870);// <!-- Coal -->
		dropListDragons.add(1871);// <!-- Charcoal -->
		dropListDragons.add(1872);// <!-- Animal Bone -->
		dropListDragons.add(1873);// <!-- Silver Nugget -->
		dropListDragons.add(1874);// <!-- Oriharukon Ore -->
		dropListDragons.add(1875);// <!-- Stone of Purity -->
		dropListDragons.add(1876);// <!-- Mithril Ore -->
		dropListDragons.add(1877);// <!-- Adamantite Nugget -->
		dropListDragons.add(1878);// <!-- Braided Hemp -->
		dropListDragons.add(1879);// <!-- Cokes -->
		dropListDragons.add(1880);// <!-- Steel -->
		dropListDragons.add(1881);// <!-- Coarse Bone Powder -->
		dropListDragons.add(1882);// <!-- Leather -->
		dropListDragons.add(1884);// <!-- Cord -->
		dropListDragons.add(1885);// <!-- High-Grade Suede -->
		dropListDragons.add(1887);// <!-- Varnish of Purity -->
		dropListDragons.add(1888);// <!-- Synthetic Cokes -->
		dropListDragons.add(1889);// <!-- Compound Braid -->
		dropListDragons.add(1890);// <!-- Mithril Alloy -->
		dropListDragons.add(1893);// <!-- Oriharukon -->
		dropListDragons.add(1894);// <!-- Crafted Leather -->
		dropListDragons.add(1895);// <!-- Metallic Fiber -->
		dropListDragons.add(4039);// <!-- Mold Glue -->
		dropListDragons.add(4040);// <!-- Mold Lubricant -->
		dropListDragons.add(4041);// <!-- Mold Hardener -->
		dropListDragons.add(4042);// <!-- Enria -->
		dropListDragons.add(4043);// <!-- Asofe -->
		dropListDragons.add(4044);// <!-- Thons -->
		dropListDragons.add(5549);// <!-- Metallic Thread -->
		dropListDragons.add(5550);// <!-- Durable Metal Plate -->
		dropListDragons.add(9628);// <!-- Leonard -->
		dropListDragons.add(9629);// <!-- Adamantine -->
		dropListDragons.add(9630);// <!-- Orichalcum -->
	}
	
	public static void dropListAnge()
	{
		
		dropListAnge.add(1864);// <!-- Stem -->
		dropListAnge.add(1865);// <!-- Varnish -->
		dropListAnge.add(1866);// <!-- Suede -->
		dropListAnge.add(1867);// <!-- Animal Skin -->
		dropListAnge.add(1868);// <!-- Thread -->
		dropListAnge.add(1869);// <!-- Iron Ore -->
		dropListAnge.add(1870);// <!-- Coal -->
		dropListAnge.add(1871);// <!-- Charcoal -->
		dropListAnge.add(1872);// <!-- Animal Bone -->
		dropListAnge.add(1873);// <!-- Silver Nugget -->
		dropListAnge.add(1874);// <!-- Oriharukon Ore -->
		dropListAnge.add(1875);// <!-- Stone of Purity -->
		dropListAnge.add(1876);// <!-- Mithril Ore -->
		dropListAnge.add(1877);// <!-- Adamantite Nugget -->
		dropListAnge.add(1878);// <!-- Braided Hemp -->
		dropListAnge.add(1879);// <!-- Cokes -->
		dropListAnge.add(1880);// <!-- Steel -->
		dropListAnge.add(1881);// <!-- Coarse Bone Powder -->
		dropListAnge.add(1882);// <!-- Leather -->
		dropListAnge.add(1884);// <!-- Cord -->
		dropListAnge.add(1885);// <!-- High-Grade Suede -->
		dropListAnge.add(1887);// <!-- Varnish of Purity -->
		dropListAnge.add(1888);// <!-- Synthetic Cokes -->
		dropListAnge.add(1889);// <!-- Compound Braid -->
		dropListAnge.add(1890);// <!-- Mithril Alloy -->
		dropListAnge.add(1893);// <!-- Oriharukon -->
		dropListAnge.add(1894);// <!-- Crafted Leather -->
		dropListAnge.add(1895);// <!-- Metallic Fiber -->
		dropListAnge.add(4039);// <!-- Mold Glue -->
		dropListAnge.add(4040);// <!-- Mold Lubricant -->
		dropListAnge.add(4041);// <!-- Mold Hardener -->
		dropListAnge.add(4042);// <!-- Enria -->
		dropListAnge.add(4043);// <!-- Asofe -->
		dropListAnge.add(4044);// <!-- Thons -->
		dropListAnge.add(5549);// <!-- Metallic Thread -->
		dropListAnge.add(5550);// <!-- Durable Metal Plate -->
		dropListAnge.add(9628);// <!-- Leonard -->
		dropListAnge.add(9629);// <!-- Adamantine -->
		dropListAnge.add(9630);// <!-- Orichalcum -->
	}
	
	public static void dropListMagic()
	{
		
		dropListMagic.add(1869);// <!-- Iron Ore -->
		dropListMagic.add(1870);// <!-- Coal -->
		dropListMagic.add(1873);// <!-- Silver Nugget -->
		dropListMagic.add(1874);// <!-- Oriharukon Ore -->
		dropListMagic.add(5550);// <!-- Durable Metal Plate -->
		dropListMagic.add(1876);// <!-- Mithril Ore -->
		dropListMagic.add(1877);// <!-- Adamantite Nugget -->
		dropListMagic.add(1880);// <!-- Steel -->
		dropListMagic.add(1879);// <!-- Cokes -->
		dropListMagic.add(1888);// <!-- Synthetic Cokes -->
		dropListMagic.add(1890);// <!-- Mithril Alloy -->
		dropListMagic.add(1887);// <!-- Varnish of Purity -->
	}
	
	public static void dropListBeast()
	{
		dropListBeast.add(1872);// <!-- Animal Bone -->
		dropListBeast.add(1881);// <!-- Coarse Bone Powder -->
		dropListBeast.add(1867);// <!-- Animal Skin -->
		dropListBeast.add(1882);// <!-- Leather -->
		dropListBeast.add(1894);// <!-- Crafted Leather -->
	}
	
	public static void dropListInsect()
	{
		
		dropListInsect.add(1866);// <!-- Suede -->
		dropListInsect.add(1885);// <!-- High-Grade Suede -->
		dropListInsect.add(1882);// <!-- Leather -->
		dropListInsect.add(1874);// <!-- Oriharukon Ore -->
		dropListInsect.add(1867);// <!-- Animal Skin -->
		dropListInsect.add(1894);// <!-- Crafted Leather -->
		dropListInsect.add(1868);// <!-- Thread -->
		dropListInsect.add(5549);// <!-- Metallic Thread -->
	}
	
	public static void dropListAnimaux()
	{
		dropListAnimaux.add(1866);// <!-- Suede -->
		dropListAnimaux.add(1885);// <!-- High-Grade Suede -->
		dropListAnimaux.add(1882);// <!-- Leather -->
		dropListAnimaux.add(1874);// <!-- Oriharukon Ore -->
		dropListAnimaux.add(1867);// <!-- Animal Skin -->
		dropListAnimaux.add(1894);// <!-- Crafted Leather -->
		dropListAnimaux.add(1868);// <!-- Thread -->
		dropListAnimaux.add(5549);// <!-- Metallic Thread -->
	}
	
	/**
	 * check si la limite d'adena à été atteinte
	 * @return true si la limite est ateinte
	 */
	public static boolean noMoreAdena()
	{
		return getAdenaCount() >= Config.CUSTOM_ECONOMIE_MAX_ADENA;
	}
	
	/**
	 * recupère et calcule le montant d'adena présent sur tous les perso
	 * @param itemId
	 * @return
	 */
	public static long getItemCountFromBdd(int itemId)
	{
		long count = 0L;
		try (Connection con = ConnectionFactory.getInstance().getConnection();
			PreparedStatement ps = con.prepareStatement(COUNT_ITEM))
		{
			// Retrieve all skills of this L2PcInstance from the database
			ps.setInt(1, itemId);
			try (ResultSet rs = ps.executeQuery())
			{
				while (rs.next())
				{
					if (isNotGm(rs.getInt("owner_id")))
					{
						count += rs.getLong("count");
					}
					
				}
			}
		}
		catch (Exception e)
		{
			LOG.error("Could Not Count item " + itemId, getInstance(), e);
		}
		if (Config.CUSTOM_ECONOMIE_LOG_ACTIVATE)
		{
			LOG.info("adena Count : " + count);
		}
		return count;
	}
	
	/**
	 * recupère et calcule le montant d'adena présent sur le sol
	 * @param itemId
	 * @return
	 */
	public static long getItemCountOnGround(int itemId)
	{
		long count = 0L;
		for (L2ItemInstance item : ItemsOnGroundManager.getInstance().get_items())
		{
			if (item.getId() == ADENA_ID)
			{
				count += item.getCount();
			}
		}
		if (Config.CUSTOM_ECONOMIE_LOG_ACTIVATE)
		{
			LOG.info("adena Count on Ground : " + count);
		}
		return count;
	}
	
	public static long getCastleVaultAdenaCount()
	{
		long count = 0L;
		for (Castle c : CastleManager.getInstance().getCastles())
		{
			count += c.getTreasury();
			
		}
		if (Config.CUSTOM_ECONOMIE_LOG_ACTIVATE)
		{
			LOG.info("count intermediaire Castle Vault :" + count);
		}
		return count;
	}
	
	private static boolean isNotGm(int objectId)
	{
		try (Connection con = ConnectionFactory.getInstance().getConnection();
			PreparedStatement ps = con.prepareStatement(CHECK_GM_STATUS))
		{
			// Retrieve all skills of this L2PcInstance from the database
			ps.setInt(1, objectId);
			try (ResultSet rs = ps.executeQuery())
			{
				while (rs.next())
				{
					if (rs.getInt("accesslevel") > 0)
					{
						if (Config.CUSTOM_ECONOMIE_LOG_ACTIVATE)
						{
							LOG.info("This char is a GM : " + objectId);
						}
						return false;
					}
				}
			}
		}
		catch (Exception e)
		{
			LOG.error("Could Not retreive status of " + objectId, getInstance(), e);
		}
		return true;
	}
	
	/**
	 * @param adenaAmount
	 * @return true si la somme créée fait dépasser le max d'Adena autorisé
	 */
	public static boolean checkAdenaCountPrevision(long adenaAmount)
	{
		return (getAndUpdateAdenaCount() + adenaAmount) > Config.CUSTOM_ECONOMIE_MAX_ADENA;
	}
	
	public static long getAdenaBidder()
	{
		long adena = 0;
		for (Auction auction : AuctionManager.getInstance().getAuctions())
		{
			for (Bidder bid : auction.getBidders().values())
			{
				adena += bid.getBid();
			}
		}
		return adena;
	}
	
	/**
	 * @return the cOUNT_ITEM_ON_GROUND
	 */
	public static String getCOUNT_ITEM_ON_GROUND()
	{
		return COUNT_ITEM_ON_GROUND;
	}
	
	/**
	 * @param cOUNT_ITEM_ON_GROUND the cOUNT_ITEM_ON_GROUND to set
	 */
	public static void setCOUNT_ITEM_ON_GROUND(String cOUNT_ITEM_ON_GROUND)
	{
		COUNT_ITEM_ON_GROUND = cOUNT_ITEM_ON_GROUND;
	}
	
	public static void changeDropFromAdenaToMats(long count, L2PcInstance player, L2Attackable mob)
	{
		switch (mob.getRace())
		{
			case ANIMAL:
				chooseDrop(dropListAnimaux, mob, count, player);
				break;
			case BEAST:
				chooseDrop(dropListBeast, mob, count, player);
				break;
			case BUG:
				chooseDrop(dropListInsect, mob, count, player);
				break;
			case DEMONIC:
				chooseDrop(dropListDeamon, mob, count, player);
				break;
			case DIVINE:
				chooseDrop(dropListAnge, mob, count, player);
				break;
			case DRAGON:
				chooseDrop(dropListDragons, mob, count, player);
				break;
			case ELEMENTAL:
				chooseDrop(dropListSpirit, mob, count, player);
				break;
			case FAIRY:
				chooseDrop(dropListPixy, mob, count, player);
				break;
			case GIANT:
				chooseDrop(dropListGiant, mob, count, player);
				break;
			case PLANT:
				chooseDrop(dropListPlante, mob, count, player);
				break;
			case UNDEAD:
				chooseDrop(dropListUndead, mob, count, player);
				break;
			case CONSTRUCT:
				chooseDrop(dropListMagic, mob, count, player);
				break;
			default:
				rewardDefault(count, player, mob);
				break;
		}
	}
	
	public static void rewardDefault(Long count, L2PcInstance player, L2Attackable mob)
	{
		// si la limite est atteinte, aucun adena ne drop
		if (noMoreAdena())
		{
			player.sendMessage("");
			return;
		}
		ItemHolder item = new ItemHolder(57, count);
		mob.dropItem(player, item);
	}
	
	public static void chooseDrop(List<Integer> dropList, L2Attackable mob, Long count, L2PcInstance player)
	{
		Map<Integer, Long> l_map = new HashMap<>();
		Long lesserValue = 10000000000L;
		
		for (Integer item : dropList)
		{
			Long value = _CorrespondancePrix.get(item);
			if (value < lesserValue)
			{
				lesserValue = value;
			}
			l_map.put(item, value);
		}
		while (count > lesserValue)
		{
			int maxRandom = dropList.size() - 1;
			int minRandom = 0;
			int randomId = Rnd.get(minRandom, maxRandom);
			Long itemValue = l_map.get(dropList.get(randomId));
			int nbItems = 0;
			while (itemValue <= count)
			{
				nbItems++;
				count -= itemValue;
			}
			if (nbItems > 0)
			{
				ItemHolder drop = new ItemHolder(dropList.get(randomId), nbItems);
				mob.dropItem(player, drop);
			}
		}
	}
	
	public static ItemHolder chooseDrop(List<Integer> dropList, Long count, L2PcInstance player)
	{
		Map<Integer, Long> l_map = new HashMap<>();
		Long lesserValue = 10000000000L;
		
		for (Integer item : dropList)
		{
			Long value = _CorrespondancePrix.get(item);
			if (value < lesserValue)
			{
				lesserValue = value;
			}
			l_map.put(item, value);
		}
		
		int maxRandom = dropList.size() - 1;
		int minRandom = 0;
		int randomId = Rnd.get(minRandom, maxRandom);
		Long itemValue = l_map.get(dropList.get(randomId));
		int nbItems = 0;
		while (itemValue <= count)
		{
			nbItems++;
			count -= itemValue;
		}
		ItemHolder drop = new ItemHolder(dropList.get(randomId), nbItems);
		return drop;
	}
	
	/**
	 * @return the _CorrespondancePrix
	 */
	public static Map<Integer, Long> get_CorrespondancePrix()
	{
		return _CorrespondancePrix;
	}
	
	/**
	 * @param _CorrespondancePrix the _CorrespondancePrix to set
	 */
	public static void set_CorrespondancePrix(Map<Integer, Long> _CorrespondancePrix)
	{
		Economie._CorrespondancePrix = _CorrespondancePrix;
	}
	
	public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map)
	{
		return map.entrySet().stream().sorted(Map.Entry.comparingByValue(Collections.reverseOrder())).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
	}
	
	/**
	 * @param count
	 * @return
	 */
	public static ArrayList<ItemHolder> getItemCorrespondenseForQuest(long count, L2PcInstance player)
	{
		ArrayList<ItemHolder> items = new ArrayList<>();
		long subcount = Math.round(count / 4);
		if (subcount >= 50)
		{
			for (int i = 0; i < 4; i++)
			{
				items.add(chooseDrop(dropListAnge, count, player));
			}
		}
		return items;
	}
	
}