/*
 * Copyright (C) 2004-2017 L2J Server
 * 
 * This file is part of L2J Server.
 * 
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.custom.economie.hotelDesVentes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.pool.impl.ConnectionFactory;
import com.l2jserver.gameserver.custom.economie.ville.VilleInstance;
import com.l2jserver.gameserver.custom.economie.ville.VilleStore;
import com.l2jserver.gameserver.model.Location;

/**
 * @author 17062
 */
public class HotelDesVentesSaver
{
	private static final String QUERY_GET_ITEM_LIST_BY_CITY = "SELECT * FROM perenne_hv_item WHERE id_vendeur=?";
	private static final String QUERY_GET_VENDEUR_LOC_FOR_CITY = "SELECT * FROM perenne_hv_npc_vendeur WHERE ville_id=?";
	private static final String QUERY_GET_RECUPERATEUR_LOC_FOR_CITY = "SELECT * FROM perenne_hv_npc_recuperateur WHERE ville_id=?";
	private static final String QUERY_GET_ELEMENT_FOR_ITEM = "SELECT * FROM perenne_item_hv_element WHERE object_id=?";
	private static final String QUERY_SET_ELEMENT_FOR_ITEM = "REPLACE INTO perenne_item_hv_element (object_id,element_type,element_level) VALUES(?,?,?)";
	private static final String QUERY_DELET_ELEMENT_FOR_ITEM = "DELETE FROM `perenne_item_hv_element` WHERE objet_id =?";
	
	private static final String QUERY_UPDATE_ITEM_LIST = "REPLACE INTO `perenne_hv_item`(`id`, `prix`, `id_item`, `quantite`, `id_vendeur`, `nom_vendeur`, `ville_id`, `taxe`, `enchant_level`) VALUES (?,?,?,?,?,?,?,?,?)";
	
	private static final Logger LOG = LoggerFactory.getLogger(VilleStore.class);
	
	/**
	 * @param villeId int
	 * @return
	 */
	public static ArrayList<ObjetAVendre> getItemByCity(int villeId)
	{
		ArrayList<ObjetAVendre> tab = new ArrayList<>();
		try (Connection con = ConnectionFactory.getInstance().getConnection();
			PreparedStatement ps = con.prepareStatement(QUERY_GET_ITEM_LIST_BY_CITY))
		{
			// Retrieve all skills of this L2PcInstance from the database
			ps.setInt(1, villeId);
			try (ResultSet rs = ps.executeQuery())
			{
				while (rs.next())
				{
					ObjetAVendre obj = new ObjetAVendre(rs.getLong("prix"), rs.getInt("id"), rs.getInt("id_vendeur"), rs.getInt("id_item"), rs.getLong("quantite"), rs.getInt("ville_id"), rs.getString("nom_vendeur"), rs.getInt("enchant_level"), null);
					tab.add(getElementObjetAVendre(obj));
				}
			}
		}
		catch (Exception e)
		{
			LOG.error("Could not retreive info for perenne item : {}", e);
		}
		
		return tab;
	}
	
	/**
	 * @param item
	 */
	public static void createThisItemInBdd(ObjetAVendre item)
	{
		try (Connection con = ConnectionFactory.getInstance().getConnection();
			PreparedStatement ps = con.prepareStatement(QUERY_UPDATE_ITEM_LIST))
		// `id`, `prix`, `id_item`, `quantite`, `id_vendeur`, `nom_vendeur`, `ville_id`, `taxe`, `enchant_level`
		{
			ps.setInt(1, item.getId());
			ps.setLong(2, item.getPrix());
			ps.setInt(3, item.getItemId());
			ps.setLong(4, item.getQuantite());
			ps.setInt(5, item.getVendeurId());
			ps.setString(6, item.getVendeurName());
			ps.setInt(7, item.getVilleId());
			ps.setLong(8, item.getTaxe());
			ps.setInt(9, item.getEnchantLevel());
			ps.execute();
		}
		catch (Exception e)
		{
			LOG.error("Error could not insert item :" + item.getId() + " : with template ID: " + item.getItemId(), e);
		}
	}
	
	/**
	 * @param ville
	 * @return
	 */
	public static Location getLocalisationVendeur(VilleInstance ville)
	{
		Location loc = new Location(0, 0, 0);
		try (Connection con = ConnectionFactory.getInstance().getConnection();
			PreparedStatement ps = con.prepareStatement(QUERY_GET_VENDEUR_LOC_FOR_CITY))
		{
			// Retrieve all skills of this L2PcInstance from the database
			ps.setInt(1, ville.getId());
			try (ResultSet rs = ps.executeQuery())
			{
				while (rs.next())
				{
					loc = new Location(rs.getInt("loc_x"), rs.getInt("loc_y"), rs.getInt("loc_z"), rs.getInt("heading"));
				}
			}
		}
		catch (Exception e)
		{
			LOG.error("Could not retreive info for perenne hv seller for city : " + ville.getNom(), e);
		}
		return loc;
	}
	
	/**
	 * @param ville
	 * @return
	 */
	public static Location getLocalisationRecuperateur(VilleInstance ville)
	{
		Location loc = new Location(0, 0, 0);
		try (Connection con = ConnectionFactory.getInstance().getConnection();
			PreparedStatement ps = con.prepareStatement(QUERY_GET_RECUPERATEUR_LOC_FOR_CITY))
		{
			// Retrieve all skills of this L2PcInstance from the database
			ps.setInt(1, ville.getId());
			try (ResultSet rs = ps.executeQuery())
			{
				while (rs.next())
				{
					loc = new Location(rs.getInt("loc_x"), rs.getInt("loc_y"), rs.getInt("loc_z"), rs.getInt("heading"));
				}
			}
		}
		catch (Exception e)
		{
			LOG.error("Could not retreive info for perenne hv seller for city : " + ville.getNom(), e);
		}
		return loc;
	}
	
	public static int getIdNpcVendeur(VilleInstance ville)
	{
		int npc = 0;
		try (Connection con = ConnectionFactory.getInstance().getConnection();
			PreparedStatement ps = con.prepareStatement(QUERY_GET_VENDEUR_LOC_FOR_CITY))
		{
			// Retrieve all skills of this L2PcInstance from the database
			ps.setInt(1, ville.getId());
			try (ResultSet rs = ps.executeQuery())
			{
				while (rs.next())
				{
					npc = rs.getInt("templateId");
				}
			}
		}
		catch (Exception e)
		{
			LOG.error("Could not retreive info for perenne hv seller for city : " + ville.getNom(), e);
		}
		return npc;
	}
	
	public static int getIdNpcRecuperateur(VilleInstance ville)
	{
		int npc = 0;
		try (Connection con = ConnectionFactory.getInstance().getConnection();
			PreparedStatement ps = con.prepareStatement(QUERY_GET_RECUPERATEUR_LOC_FOR_CITY))
		{
			// Retrieve all skills of this L2PcInstance from the database
			ps.setInt(1, ville.getId());
			try (ResultSet rs = ps.executeQuery())
			{
				while (rs.next())
				{
					npc = rs.getInt("templateId");
				}
			}
		}
		catch (Exception e)
		{
			LOG.error("Could not retreive info for perenne hv seller for city : " + ville.getNom(), e);
		}
		return npc;
	}
	
	public static ObjetAVendre getElementObjetAVendre(ObjetAVendre obj)
	{
		Map<Integer, Integer> elements = new HashMap<>();
		try (Connection con = ConnectionFactory.getInstance().getConnection();
			PreparedStatement ps = con.prepareStatement(QUERY_GET_ELEMENT_FOR_ITEM))
		{
			// Retrieve all skills of this L2PcInstance from the database
			ps.setInt(1, obj.getId());
			try (ResultSet rs = ps.executeQuery())
			{
				while (rs.next())
				{
					elements.put(rs.getInt("element_type"), rs.getInt("element_level"));
				}
			}
		}
		catch (Exception e)
		{
			LOG.error("Could not retreive elements for perenne hv item : " + obj.getId(), e);
		}
		obj.setElements(elements);
		return obj;
	}
	
	/**
	 * insert or update item element in bdd
	 * @param item
	 */
	public static void saveElementInBdd(ObjetAVendre item)
	{
		for (Integer k : item.getElements().keySet())
		{
			int element = item.getElements().get(k);
			try (Connection con = ConnectionFactory.getInstance().getConnection();
				PreparedStatement ps = con.prepareStatement(QUERY_SET_ELEMENT_FOR_ITEM))
			// `id`, `prix`, `id_item`, `quantite`, `id_vendeur`, `nom_vendeur`, `ville_id`, `taxe`, `enchant_level`
			{
				ps.setInt(1, item.getId());
				ps.setInt(2, k);
				ps.setInt(3, element);
				ps.execute();
			}
			catch (Exception e)
			{
				LOG.error("Error could not insert item :" + item.getId() + " : with template ID: " + item.getItemId(), e);
			}
		}
	}
	
	/**
	 * Delete item in bdd
	 * @param item
	 */
	public static void deleteElementInBdd(ObjetAVendre item)
	{
		for (@SuppressWarnings("unused")
		Integer k : item.getElements().keySet())
		{
			try (Connection con = ConnectionFactory.getInstance().getConnection();
				PreparedStatement ps = con.prepareStatement(QUERY_SET_ELEMENT_FOR_ITEM))
			// `id`, `prix`, `id_item`, `quantite`, `id_vendeur`, `nom_vendeur`, `ville_id`, `taxe`, `enchant_level`
			{
				ps.setInt(1, item.getId());
				ps.execute();
			}
			catch (Exception e)
			{
				LOG.error("Error could not insert item :" + item.getId() + " : with template ID: " + item.getItemId(), e);
			}
		}
	}
	
	/**
	 * update or insert item in bdd
	 * @param item
	 */
	public static void saveObjectInBdd(ObjetAVendre item)
	{
		try (Connection con = ConnectionFactory.getInstance().getConnection();
			PreparedStatement ps = con.prepareStatement(QUERY_UPDATE_ITEM_LIST))
		// `id`, `prix`, `id_item`, `quantite`, `id_vendeur`, `nom_vendeur`, `ville_id`, `taxe`, `enchant_level`
		{
			ps.setInt(1, item.getId());
			ps.setLong(2, item.getPrix());
			ps.setInt(3, item.getItemId());
			ps.setLong(4, item.getQuantite());
			ps.setInt(5, item.getVendeurId());
			ps.setString(6, item.getVendeurName());
			ps.setInt(7, item.getVilleId());
			ps.setLong(8, item.getTaxe());
			ps.setInt(9, item.getEnchantLevel());
			ps.execute();
		}
		catch (Exception e)
		{
			LOG.error("Error could not insert item :" + item.getId() + " : with template ID: " + item.getItemId(), e);
		}
	}
	
	/**
	 * Delete item in bdd
	 * @param item
	 */
	public static void deleteItemInBdd(ObjetAVendre item)
	{
		try (Connection con = ConnectionFactory.getInstance().getConnection();
			PreparedStatement ps = con.prepareStatement(QUERY_DELET_ELEMENT_FOR_ITEM))
		{
			ps.setInt(1, item.getId());
			ps.execute();
		}
		catch (Exception e)
		{
			LOG.error("Error could not delete item :" + item.getId() + " : with template ID: " + item.getItemId(), e);
		}
	}
	
	public static String getQuerySetElementForItem()
	{
		return QUERY_SET_ELEMENT_FOR_ITEM;
	}
}
