/*
 * Copyright (C) 2004-2017 L2J Server
 * 
 * This file is part of L2J Server.
 * 
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.custom.economie.ville;

import com.l2jserver.gameserver.model.Location;
import com.l2jserver.gameserver.model.actor.L2Npc;

/**
 * @author Rizaar
 */
public class GardeInstance
{
	private int id;
	private int templateId;
	private final VilleInstance ville;
	private final Location position;
	private String point;
	private L2Npc garde;
	
	public GardeInstance(int id, int templateId, VilleInstance ville, Location position, String point)
	{
		this.id = id;
		this.templateId = templateId;
		this.ville = ville;
		this.point = point;
		this.position = position;
	}
	
	public GardeInstance(int id, int templateId, VilleInstance ville, Location position, String point, L2Npc garde)
	{
		this.id = id;
		this.templateId = templateId;
		this.ville = ville;
		this.point = point;
		this.position = position;
		this.garde = garde;
	}
	
	/**
	 * @return the id
	 */
	public int getId()
	{
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(int id)
	{
		this.id = id;
	}
	
	/**
	 * @return the templateId
	 */
	public int getTemplateId()
	{
		return templateId;
	}
	
	/**
	 * @param templateId the templateId to set
	 */
	public void setTemplateId(int templateId)
	{
		this.templateId = templateId;
	}
	
	/**
	 * @return the point
	 */
	public String getPoint()
	{
		return point;
	}
	
	/**
	 * @param point the point to set
	 */
	public void setPoint(String point)
	{
		this.point = point;
	}
	
	/**
	 * @return the garde
	 */
	public L2Npc getGarde()
	{
		return garde;
	}
	
	/**
	 * @param garde the garde to set
	 */
	public void setGarde(L2Npc garde)
	{
		this.garde = garde;
	}
	
	/**
	 * @return the position
	 */
	public Location getPosition()
	{
		return position;
	}
	
	/**
	 * @return the ville
	 */
	public VilleInstance getVille()
	{
		return ville;
	}
}
