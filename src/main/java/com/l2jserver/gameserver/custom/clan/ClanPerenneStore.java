/*
 * Copyright (C) 2004-2017 L2J Server
 * 
 * This file is part of L2J Server.
 * 
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.custom.clan;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.pool.impl.ConnectionFactory;
import com.l2jserver.gameserver.custom.economie.ville.VilleStore;

/**
 * @author Rizaar
 */
public class ClanPerenneStore
{
	private static final Logger LOG = LoggerFactory.getLogger(VilleStore.class);
	public static String QUERY_UPDATE_CLAN_RP_SKILLS = "UPDATE `perenne_clan` SET `archer`=?,`mage`=?,`guerrier`=?,`quartier_general`=?,`deploiement_strategique`=?,`vente`=?,`caravane`=?,`recuperation`=?,`strategie_commercial`=?,`quartier_des_affaires`=?,`excavation_miniere`=?,`agriculture_et_elevage`=?,`zone_a_risque`=?,`amical_des_ouvriers`=?,`contre_maitrise_avencee`=?,`points`=? WHERE id=?";
	public static String QUERY_INSERT_CLAN_RP_SKILLS = "INSERT INTO `perenne_clan`(`id`, `archer`, `mage`, `guerrier`, `quartier_general`, `deploiement_strategique`, `vente`, `caravane`, `recuperation`, `strategie_commercial`, `quartier_des_affaires`, `excavation_miniere`, `agriculture_et_elevage`, `zone_a_risque`, `amical_des_ouvriers`, `contre_maitrise_avencee`,`points`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	public static String QUERY_DELETE_CLAN_RP_SKILLS = "DELETE FROM `perenne_clan` WHERE `id`=?";
	public static String QUERY_SELECT_CLAN_RP_SKILLS = "SELECT * from `perenne_clan`WHERE `id`=?";
	
	public static void updateClanRpSkills(ClanPerenne clan)
	{
		try (Connection con = ConnectionFactory.getInstance().getConnection())
		{
			try (PreparedStatement ps = con.prepareStatement(QUERY_UPDATE_CLAN_RP_SKILLS))
			{
				ps.setInt(1, clan.getArcher());
				ps.setInt(2, clan.getMage());
				ps.setInt(3, clan.getGuerrier());
				ps.setBoolean(4, clan.isQuartierGeneral());
				ps.setBoolean(5, clan.isDeploiementStrategique());
				ps.setInt(6, clan.getHotelDesVente());
				ps.setInt(7, clan.getCaravane());
				ps.setInt(8, clan.getRecuperation());
				ps.setBoolean(9, clan.isStrategieCommercial());
				ps.setBoolean(10, clan.isQuartierDesAffaires());
				ps.setInt(11, clan.getExcavationMiniere());
				ps.setInt(12, clan.getAgricultureEtElevage());
				ps.setInt(13, clan.getZoneARisque());
				ps.setBoolean(14, clan.isAmicalDesOuvriers());
				ps.setBoolean(15, clan.isContreMaitriseAvencee());
				ps.setInt(16, clan.getPoints());
				ps.setInt(17, clan.getClan().getId());
				ps.execute();
			}
		}
		catch (Exception e)
		{
			LOG.error("Error could not update rp skills for this clan :" + clan.getClan().getName(), e);
		}
	}
	
	public static void insertClanRpSkills(ClanPerenne clan)
	{
		try (Connection con = ConnectionFactory.getInstance().getConnection())
		{
			try (PreparedStatement ps = con.prepareStatement(QUERY_INSERT_CLAN_RP_SKILLS))
			{
				ps.setInt(1, clan.getClan().getId());
				ps.setInt(2, clan.getArcher());
				ps.setInt(3, clan.getMage());
				ps.setInt(4, clan.getGuerrier());
				ps.setBoolean(5, clan.isQuartierGeneral());
				ps.setBoolean(6, clan.isDeploiementStrategique());
				ps.setInt(7, clan.getHotelDesVente());
				ps.setInt(8, clan.getCaravane());
				ps.setInt(9, clan.getRecuperation());
				ps.setBoolean(10, clan.isStrategieCommercial());
				ps.setBoolean(11, clan.isQuartierDesAffaires());
				ps.setInt(12, clan.getExcavationMiniere());
				ps.setInt(13, clan.getAgricultureEtElevage());
				ps.setInt(14, clan.getZoneARisque());
				ps.setBoolean(15, clan.isAmicalDesOuvriers());
				ps.setBoolean(16, clan.isContreMaitriseAvencee());
				ps.setInt(17, clan.getPoints());
			}
		}
		catch (Exception e)
		{
			LOG.error("Error could not update rp skills for this clan :" + clan.getClan().getName(), e);
		}
	}
	
	public static void deleteClanRpSkills(ClanPerenne clan)
	{
		try (Connection con = ConnectionFactory.getInstance().getConnection())
		{
			try (PreparedStatement ps = con.prepareStatement(QUERY_DELETE_CLAN_RP_SKILLS))
			{
				ps.setInt(1, clan.getArcher());
				ps.setInt(2, clan.getMage());
				ps.setInt(3, clan.getGuerrier());
				ps.setBoolean(4, clan.isQuartierGeneral());
				ps.setBoolean(5, clan.isDeploiementStrategique());
				ps.setInt(6, clan.getHotelDesVente());
				ps.setInt(7, clan.getCaravane());
				ps.setInt(8, clan.getRecuperation());
				ps.setBoolean(9, clan.isStrategieCommercial());
				ps.setBoolean(10, clan.isQuartierDesAffaires());
				ps.setInt(11, clan.getExcavationMiniere());
				ps.setInt(12, clan.getAgricultureEtElevage());
				ps.setInt(13, clan.getZoneARisque());
				ps.setBoolean(14, clan.isAmicalDesOuvriers());
				ps.setBoolean(15, clan.isContreMaitriseAvencee());
				ps.setInt(16, clan.getClan().getId());
				ps.execute();
			}
		}
		catch (Exception e)
		{
			LOG.error("Error could not update rp skills for this clan :" + clan.getClan().getName(), e);
		}
	}
	
	public static ClanPerenne getClanPerenneFromDb(ClanPerenne clan)
	{
		if (clan.getClan() != null)
		{
			try (Connection con = ConnectionFactory.getInstance().getConnection();
				PreparedStatement ps = con.prepareStatement(QUERY_SELECT_CLAN_RP_SKILLS))
			{
				// Retrieve all skills of this L2PcInstance from the database
				ps.setInt(1, clan.getClan().getId());
				try (ResultSet rs = ps.executeQuery())
				{
					while (rs.next())
					{
						clan.setArcher(rs.getInt("archer"));
						clan.setMage(rs.getInt("mage"));
						clan.setGuerrier(rs.getInt("guerrier"));
						clan.setQuartierGeneral(rs.getBoolean("quartier_general"));
						clan.setDeploiementStrategique(rs.getBoolean("deploiement_strategique"));
						clan.setHotelDesVente(rs.getInt("vente"));
						clan.setCaravane(rs.getInt("caravane"));
						clan.setRecuperation(rs.getInt("recuperation"));
						clan.setStrategieCommercial(rs.getBoolean("strategie_commercial"));
						clan.setQuartierDesAffaires(rs.getBoolean("quartier_des_affaires"));
						clan.setExcavationMiniere(rs.getInt("excavation_miniere"));
						clan.setAgricultureEtElevage(rs.getInt("agriculture_et_elevage"));
						clan.setZoneARisque(rs.getInt("zone_a_risque"));
						clan.setAmicalDesOuvriers(rs.getBoolean("amical_des_ouvriers"));
						clan.setContreMaitriseAvencee(rs.getBoolean("contre_maitrise_avencee"));
						clan.setPoints(rs.getInt("points"));
					}
				}
			}
			catch (Exception e)
			{
				LOG.error("Could not retreive info for perenne_clan id:(" + clan.getClan().getId() + ") : {}", e);
			}
		}
		else
		{
			clan.setArcher(0);
			clan.setMage(0);
			clan.setGuerrier(0);
			clan.setQuartierGeneral(false);
			clan.setDeploiementStrategique(false);
			clan.setHotelDesVente(0);
			clan.setCaravane(0);
			clan.setRecuperation(0);
			clan.setStrategieCommercial(false);
			clan.setQuartierDesAffaires(false);
			clan.setExcavationMiniere(0);
			clan.setAgricultureEtElevage(0);
			clan.setZoneARisque(0);
			clan.setAmicalDesOuvriers(false);
			clan.setContreMaitriseAvencee(false);
			clan.setPoints(0);
		}
		return clan;
	}
	
}
