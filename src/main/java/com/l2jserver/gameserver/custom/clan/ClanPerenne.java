/*
 * Copyright (C) 2004-2017 L2J Server
 * 
 * This file is part of L2J Server.
 * 
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.custom.clan;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.gameserver.data.sql.impl.ClanTable;
import com.l2jserver.gameserver.model.L2Clan;

/**
 * @author Rizaar
 */
public class ClanPerenne
{
	private L2Clan clan;
	private static final Logger LOG = LoggerFactory.getLogger(ClanPerenne.class);
	// garde
	private int archer; // 3lvl
	private int mage;// 3lvl
	private int guerrier;// 3lvl
	private boolean quartierGeneral; // 1lvl
	private boolean deploiementStrategique;// 1lvl
	// commercial
	private int hotelDesVentes; // 3lvl
	private int caravane; // 3lvl
	private int centreRecuperation; // 3lvl
	private boolean strategieCommercial; // 1lvl
	private boolean quartierDesAffaires; // 1lvl
	// production
	private int excavationMiniere;
	private int agricultureEtElevage;
	private int zoneARisque;
	private boolean amicalDesOuvriers; // 1lvl
	private boolean contreMaitriseAvencee; // 1lvl
	private int points;
	
	public ClanPerenne(L2Clan clan)
	{
		this.clan = clan;
		ClanPerenne c = ClanPerenneStore.getClanPerenneFromDb(this);
		this.archer = c.archer; // 3lvl
		this.mage = c.mage;// 3lvl
		this.guerrier = c.guerrier;// 3lvl
		this.quartierGeneral = c.quartierGeneral; // 1lvl
		this.deploiementStrategique = c.deploiementStrategique;// 1lvl
		// commercial
		this.hotelDesVentes = c.hotelDesVentes; // 3lvl
		this.caravane = c.caravane; // 3lvl
		this.centreRecuperation = c.centreRecuperation; // 3lvl
		this.strategieCommercial = c.strategieCommercial; // 1lvl
		this.quartierDesAffaires = c.quartierDesAffaires; // 1lvl
		// production
		this.excavationMiniere = c.excavationMiniere;
		this.agricultureEtElevage = c.agricultureEtElevage;
		this.zoneARisque = c.zoneARisque;
		this.amicalDesOuvriers = c.amicalDesOuvriers; // 1lvl
		this.contreMaitriseAvencee = c.contreMaitriseAvencee; // 1lvl
		this.points = c.points;
	}
	
	public ClanPerenne()
	{
		this.clan = ClanTable.getInstance().getClan(-1);
		this.archer = 0; // 3lvl
		this.mage = 0;// 3lvl
		this.guerrier = 0;// 3lvl
		this.quartierGeneral = false; // 1lvl
		this.deploiementStrategique = false;// 1lvl
		// commercial
		this.hotelDesVentes = 0; // 3lvl
		this.caravane = 0; // 3lvl
		this.centreRecuperation = 0; // 3lvl
		this.strategieCommercial = false; // 1lvl
		this.quartierDesAffaires = false; // 1lvl
		// production
		this.excavationMiniere = 0;
		this.agricultureEtElevage = 0;
		this.zoneARisque = 0;
		this.amicalDesOuvriers = false; // 1lvl
		this.contreMaitriseAvencee = false; // 1lvl
		this.points = 0;
	}
	
	@Override
	public String toString()
	{
		String str = "";
		str += "Archer[" + this.archer + "] - "; // 3lvl
		str += "mage[" + this.mage + "] - ";// 3lvl
		str += "guerrier[" + this.guerrier + "] - ";// 3lvl
		str += "quartierGeneral[" + this.quartierGeneral + "] - "; // 1lvl
		str += "deploiementStrategique[" + this.deploiementStrategique + "] - ";// 1lvl
		// commercial
		str += "hotelDesVentes[" + this.hotelDesVentes + "] - ";// 3lvl
		str += "caravane[" + this.caravane + "] - ";// 3lvl
		str += "centreRecuperation[" + this.centreRecuperation + "] - ";// 3lvl
		str += "strategieCommercial[" + this.strategieCommercial + "] - ";// 1lvl
		str += "quartierDesAffaires[" + this.quartierDesAffaires + "] - "; // 1lvl
		// production
		str += "excavationMiniere[" + this.excavationMiniere + "] - ";
		str += "agricultureEtElevage[" + this.agricultureEtElevage + "] - ";
		str += "zoneARisque[" + this.zoneARisque + "] - ";
		str += "amicalDesOuvriers[" + this.amicalDesOuvriers + "] - "; // 1lvl
		str += "contreMaitriseAvencee[" + this.contreMaitriseAvencee + "] - ";// 1lvl
		str += "points[" + this.points + "] - ";
		return str;
	}
	
	/**
	 * @return the archer
	 */
	public int getArcher()
	{
		return archer;
	}
	
	/**
	 * @param archer the archer to set
	 */
	public void setArcher(int archer)
	{
		this.archer = archer;
	}
	
	/**
	 * @return the mage
	 */
	public int getMage()
	{
		return mage;
	}
	
	/**
	 * @param mage the mage to set
	 */
	public void setMage(int mage)
	{
		this.mage = mage;
	}
	
	/**
	 * @return the guerrier
	 */
	public int getGuerrier()
	{
		return guerrier;
	}
	
	/**
	 * @param guerrier the guerrier to set
	 */
	public void setGuerrier(int guerrier)
	{
		this.guerrier = guerrier;
	}
	
	/**
	 * @return the quartierGeneral
	 */
	public boolean isQuartierGeneral()
	{
		return quartierGeneral;
	}
	
	/**
	 * @param quartierGeneral the quartierGeneral to set
	 */
	public void setQuartierGeneral(boolean quartierGeneral)
	{
		this.quartierGeneral = quartierGeneral;
	}
	
	/**
	 * @return the deploiementStrategique
	 */
	public boolean isDeploiementStrategique()
	{
		return deploiementStrategique;
	}
	
	/**
	 * @param deploiementStrategique the deploiementStrategique to set
	 */
	public void setDeploiementStrategique(boolean deploiementStrategique)
	{
		this.deploiementStrategique = deploiementStrategique;
	}
	
	/**
	 * @return the vente
	 */
	public int getHotelDesVente()
	{
		return hotelDesVentes;
	}
	
	/**
	 * @param hotelDesVentes the hotelDesVentes to set
	 */
	public void setHotelDesVente(int hotelDesVentes)
	{
		this.hotelDesVentes = hotelDesVentes;
	}
	
	/**
	 * @return the caravane
	 */
	public int getCaravane()
	{
		return caravane;
	}
	
	/**
	 * @param caravane the caravane to set
	 */
	public void setCaravane(int caravane)
	{
		this.caravane = caravane;
	}
	
	/**
	 * @return the recuperation
	 */
	public int getRecuperation()
	{
		return centreRecuperation;
	}
	
	/**
	 * @param recuperation the recuperation to set
	 */
	public void setRecuperation(int recuperation)
	{
		this.centreRecuperation = recuperation;
	}
	
	/**
	 * @return the strategieCommercial
	 */
	public boolean isStrategieCommercial()
	{
		return strategieCommercial;
	}
	
	/**
	 * @param strategieCommercial the strategieCommercial to set
	 */
	public void setStrategieCommercial(boolean strategieCommercial)
	{
		this.strategieCommercial = strategieCommercial;
	}
	
	/**
	 * @return the quartierDesAffaires
	 */
	public boolean isQuartierDesAffaires()
	{
		return quartierDesAffaires;
	}
	
	/**
	 * @param quartierDesAffaires the quartierDesAffaires to set
	 */
	public void setQuartierDesAffaires(boolean quartierDesAffaires)
	{
		this.quartierDesAffaires = quartierDesAffaires;
	}
	
	/**
	 * @return the excavationMiniere
	 */
	public int getExcavationMiniere()
	{
		return excavationMiniere;
	}
	
	/**
	 * @param excavationMiniere the excavationMiniere to set
	 */
	public void setExcavationMiniere(int excavationMiniere)
	{
		this.excavationMiniere = excavationMiniere;
	}
	
	/**
	 * @return the agricultureEtElevage
	 */
	public int getAgricultureEtElevage()
	{
		return agricultureEtElevage;
	}
	
	/**
	 * @param agricultureEtElevage the agricultureEtElevage to set
	 */
	public void setAgricultureEtElevage(int agricultureEtElevage)
	{
		this.agricultureEtElevage = agricultureEtElevage;
	}
	
	/**
	 * @return the zoneARisque
	 */
	public int getZoneARisque()
	{
		return zoneARisque;
	}
	
	/**
	 * @param zoneARisque the zoneARisque to set
	 */
	public void setZoneARisque(int zoneARisque)
	{
		this.zoneARisque = zoneARisque;
	}
	
	/**
	 * @return the amicalDesOuvriers
	 */
	public boolean isAmicalDesOuvriers()
	{
		return amicalDesOuvriers;
	}
	
	/**
	 * @param amicalDesOuvriers the amicalDesOuvriers to set
	 */
	public void setAmicalDesOuvriers(boolean amicalDesOuvriers)
	{
		this.amicalDesOuvriers = amicalDesOuvriers;
	}
	
	/**
	 * @return the contreMaitriseAvencee
	 */
	public boolean isContreMaitriseAvencee()
	{
		return contreMaitriseAvencee;
	}
	
	/**
	 * @param contreMaitriseAvencee the contreMaitriseAvencee to set
	 */
	public void setContreMaitriseAvencee(boolean contreMaitriseAvencee)
	{
		this.contreMaitriseAvencee = contreMaitriseAvencee;
	}
	
	/**
	 * @return the clan
	 */
	public L2Clan getClan()
	{
		return clan;
	}
	
	/**
	 * @param clan
	 */
	public void setClan(L2Clan clan)
	{
		this.clan = clan;
	}
	
	public int getPoints()
	{
		return points;
	}
	
	public void setPoints(int points)
	{
		this.points = points;
	}
	
	public int getNbPointsSpended()
	{
		int points = 0;
		points += agricultureEtElevage;
		points += amicalDesOuvriers ? 1 : 0;
		points += archer;
		points += caravane;
		points += centreRecuperation;
		points += contreMaitriseAvencee ? 1 : 0;
		points += deploiementStrategique ? 1 : 0;
		points += excavationMiniere;
		points += guerrier;
		points += hotelDesVentes;
		points += mage;
		points += quartierDesAffaires ? 1 : 0;
		points += quartierGeneral ? 1 : 0;
		points += strategieCommercial ? 1 : 0;
		points += zoneARisque;
		return points;
	}
	
}
