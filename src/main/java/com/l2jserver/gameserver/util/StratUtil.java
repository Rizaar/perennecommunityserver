/*
 * Copyright (C) 2004-2017 L2J Server
 * 
 * This file is part of L2J Server.
 * 
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.util;

import java.util.ArrayList;
import java.util.List;

import com.l2jserver.gameserver.model.L2Party;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;

/**
 * @author 17062
 */
public final class StratUtil
{
	public List<Integer> healerClassList = new ArrayList<>();
	public List<Integer> tankClassList = new ArrayList<>();
	public List<Integer> warriorClassList = new ArrayList<>();
	public List<Integer> bufferClassList = new ArrayList<>();
	public List<Integer> daggerClassList = new ArrayList<>();
	public List<Integer> ArcherClassList = new ArrayList<>();
	public List<Integer> MageClassList = new ArrayList<>();
	
	/**
	 * @param party
	 * @return
	 */
	public List<L2PcInstance> findHealerInParty(L2Party party)
	{
		List<L2PcInstance> list = new ArrayList<>();
		if (isHealerInParty(party))
		{
			return getHealerInParty(party);
		}
		return list;
	}
	
	public List<L2PcInstance> getHealerInParty(L2Party party)
	{
		List<L2PcInstance> list = new ArrayList<>();
		for (L2PcInstance member : party.getMembers())
		{
			if (isHealer(member))
			{
				list.add(member);
			}
		}
		return list;
	}
	
	public Boolean isHealerInParty(L2Party party)
	{
		for (L2PcInstance member : party.getMembers())
		{
			if (Util.contains(healerClassList.toArray(), member.getClassId()))
			{
				return true;
			}
		}
		return false;
	}
	
	public Boolean isHealer(L2PcInstance player)
	{
		return Util.contains(healerClassList.toArray(), player.getClassId());
	}
	
	////////////////////////////////////////////////////////////////////////////////
	/**
	 * @param party
	 * @return
	 */
	public List<L2PcInstance> findWarriorInParty(L2Party party)
	{
		List<L2PcInstance> list = new ArrayList<>();
		if (isWarriorInParty(party))
		{
			return getWarriorInParty(party);
		}
		return list;
	}
	
	public List<L2PcInstance> getWarriorInParty(L2Party party)
	{
		List<L2PcInstance> list = new ArrayList<>();
		for (L2PcInstance member : party.getMembers())
		{
			if (isWarrior(member))
			{
				list.add(member);
			}
		}
		return list;
	}
	
	public Boolean isWarriorInParty(L2Party party)
	{
		for (L2PcInstance member : party.getMembers())
		{
			if (Util.contains(warriorClassList.toArray(), member.getClassId()))
			{
				return true;
			}
		}
		return false;
	}
	
	public Boolean isWarrior(L2PcInstance player)
	{
		return Util.contains(warriorClassList.toArray(), player.getClassId());
	}
	
	//////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * @param party
	 * @return
	 */
	public List<L2PcInstance> findDaggerInParty(L2Party party)
	{
		List<L2PcInstance> list = new ArrayList<>();
		if (isDaggerInParty(party))
		{
			return getDaggerInParty(party);
		}
		return list;
	}
	
	public List<L2PcInstance> getDaggerInParty(L2Party party)
	{
		List<L2PcInstance> list = new ArrayList<>();
		for (L2PcInstance member : party.getMembers())
		{
			if (isDagger(member))
			{
				list.add(member);
			}
		}
		return list;
	}
	
	public Boolean isDaggerInParty(L2Party party)
	{
		for (L2PcInstance member : party.getMembers())
		{
			if (Util.contains(daggerClassList.toArray(), member.getClassId()))
			{
				return true;
			}
		}
		return false;
	}
	
	public Boolean isDagger(L2PcInstance player)
	{
		return Util.contains(daggerClassList.toArray(), player.getClassId());
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * @param party
	 * @return
	 */
	public List<L2PcInstance> findArcherInParty(L2Party party)
	{
		List<L2PcInstance> list = new ArrayList<>();
		if (isArcherInParty(party))
		{
			return getArcherInParty(party);
		}
		return list;
	}
	
	public List<L2PcInstance> getArcherInParty(L2Party party)
	{
		List<L2PcInstance> list = new ArrayList<>();
		for (L2PcInstance member : party.getMembers())
		{
			if (isArcher(member))
			{
				list.add(member);
			}
		}
		return list;
	}
	
	public Boolean isArcherInParty(L2Party party)
	{
		for (L2PcInstance member : party.getMembers())
		{
			if (Util.contains(ArcherClassList.toArray(), member.getClassId()))
			{
				return true;
			}
		}
		return false;
	}
	
	public Boolean isArcher(L2PcInstance player)
	{
		return Util.contains(ArcherClassList.toArray(), player.getClassId());
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	/**
	 * @param party
	 * @return
	 */
	public List<L2PcInstance> findBufferInParty(L2Party party)
	{
		List<L2PcInstance> list = new ArrayList<>();
		if (isBufferInParty(party))
		{
			return getBufferInParty(party);
		}
		return list;
	}
	
	public List<L2PcInstance> getBufferInParty(L2Party party)
	{
		List<L2PcInstance> list = new ArrayList<>();
		for (L2PcInstance member : party.getMembers())
		{
			if (isBuffer(member))
			{
				list.add(member);
			}
		}
		return list;
	}
	
	public Boolean isBufferInParty(L2Party party)
	{
		for (L2PcInstance member : party.getMembers())
		{
			if (Util.contains(bufferClassList.toArray(), member.getClassId()))
			{
				return true;
			}
		}
		return false;
	}
	
	public Boolean isBuffer(L2PcInstance player)
	{
		return Util.contains(bufferClassList.toArray(), player.getClassId());
	}
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * @param party
	 * @return
	 */
	public List<L2PcInstance> findMageInParty(L2Party party)
	{
		List<L2PcInstance> list = new ArrayList<>();
		if (isMageInParty(party))
		{
			return getMageInParty(party);
		}
		return list;
	}
	
	public List<L2PcInstance> getMageInParty(L2Party party)
	{
		List<L2PcInstance> list = new ArrayList<>();
		for (L2PcInstance member : party.getMembers())
		{
			if (isMage(member))
			{
				list.add(member);
			}
		}
		return list;
	}
	
	public Boolean isMageInParty(L2Party party)
	{
		for (L2PcInstance member : party.getMembers())
		{
			if (Util.contains(MageClassList.toArray(), member.getClassId()))
			{
				return true;
			}
		}
		return false;
	}
	
	public Boolean isMage(L2PcInstance player)
	{
		return Util.contains(MageClassList.toArray(), player.getClassId());
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * @param party
	 * @return
	 */
	public List<L2PcInstance> findTankInParty(L2Party party)
	{
		List<L2PcInstance> list = new ArrayList<>();
		if (isTankInParty(party))
		{
			return getTankInParty(party);
		}
		return list;
	}
	
	public List<L2PcInstance> getTankInParty(L2Party party)
	{
		List<L2PcInstance> list = new ArrayList<>();
		for (L2PcInstance member : party.getMembers())
		{
			if (isTank(member))
			{
				list.add(member);
			}
		}
		return list;
	}
	
	public Boolean isTankInParty(L2Party party)
	{
		for (L2PcInstance member : party.getMembers())
		{
			if (Util.contains(tankClassList.toArray(), member.getClassId()))
			{
				return true;
			}
		}
		return false;
	}
	
	public Boolean isTank(L2PcInstance player)
	{
		return Util.contains(tankClassList.toArray(), player.getClassId());
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////
	
	public L2PcInstance findLowPlayerHpInParty(L2Party party)
	{
		L2PcInstance low = party.getRandomPlayer();
		for (L2PcInstance member : party.getMembers())
		{
			low = member.getCurrentHp() > low.getCurrentHp() ? member : low;
		}
		return low;
	}
	
	public L2PcInstance findHighPlayerHpInParty(L2Party party)
	{
		L2PcInstance higher = party.getRandomPlayer();
		for (L2PcInstance member : party.getMembers())
		{
			higher = member.getCurrentHp() > higher.getCurrentHp() ? member : higher;
		}
		return higher;
	}
	
}
