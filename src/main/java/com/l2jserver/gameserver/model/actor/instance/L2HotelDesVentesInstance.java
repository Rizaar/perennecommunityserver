/*
 * Copyright (C) 2004-2016 L2J Server
 * 
 * This file is part of L2J Server.
 * 
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.actor.instance;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.gameserver.custom.economie.ville.VilleStore;
import com.l2jserver.gameserver.enums.InstanceType;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.templates.L2NpcTemplate;

/**
 * This class ...
 * @version $Revision: $ $Date: $
 * @author LBaldi
 */
public class L2HotelDesVentesInstance extends L2Npc
{
	private static final Logger LOG = LoggerFactory.getLogger(VilleStore.class);
	
	public L2HotelDesVentesInstance(L2NpcTemplate template)
	{
		super(template);
		setInstanceType(InstanceType.L2HotelDesVentesInstance);
	}
	
	@Override
	public String getHtmlPath(int npcId, int val)
	{
		String pom = "";
		LOG.info(pom);
		if (val == 0)
		{
			pom = "" + npcId;
			LOG.info("pom val == 0 : " + pom);
		}
		else
		{
			LOG.info("pom else : " + pom);
			pom = npcId + "-" + val;
		}
		return "data/html/custom/hotelDesVentes/npc/" + pom + ".htm";
	}
}