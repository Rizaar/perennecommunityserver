/*
 * Copyright (C) 2004-2016 L2J Server
 * 
 * This file is part of L2J Server.
 * 
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.custom.interf;

import com.l2jserver.gameserver.model.items.type.ArmorType;
import com.l2jserver.gameserver.model.items.type.EtcItemType;
import com.l2jserver.gameserver.model.items.type.ItemType;
import com.l2jserver.gameserver.model.items.type.WeaponType;

/**
 * @author Rizaar
 */
public enum CustomType
{
	/*
	 * public boolean isPotion() { return (getItemType() == EtcItemType.POTION); } public boolean isElixir() { return (getItemType() == EtcItemType.ELIXIR); } public boolean isScroll() { return (getItemType() == EtcItemType.SCROLL); }
	 */
	BESACE("Besace", new ItemType[]
	{
		EtcItemType.ARROW,
		EtcItemType.BOLT
	}),
	ALCHIMIE("Alchimie", new ItemType[]
	{
		EtcItemType.SHOT,
		EtcItemType.POTION,
		EtcItemType.ELIXIR,
		EtcItemType.DYE
	}),
	SCROLL("Scroll", new ItemType[]
	{
		EtcItemType.SCROLL,
		EtcItemType.BLESS_SCRL_ENCHANT_AM,
		EtcItemType.BLESS_SCRL_ENCHANT_WP,
		EtcItemType.SCRL_ENCHANT_AM,
		EtcItemType.SCRL_ENCHANT_WP
	}),
	RECIPE("Recipe", new ItemType[]
	{
		EtcItemType.RECIPE
	}),
	CRAFT("Craft", new ItemType[]
	{
		EtcItemType.MATERIAL
	}),
	QUETE("Quete", new ItemType[] {}),
	PET("Pet", new ItemType[]
	{
		EtcItemType.PET_COLLAR
	}),
	AGATHION("Agathion", new ItemType[] {}),
	STONE("Stone", new ItemType[]
	{
		EtcItemType.SCRL_ENCHANT_ATTR
	}),
	ARMURERIE("Armurerie", new ItemType[]
	{
		ArmorType.HEAVY,
		ArmorType.LIGHT,
		ArmorType.MAGIC,
		ArmorType.SHIELD,
		ArmorType.SIGIL,
		WeaponType.ANCIENTSWORD,
		WeaponType.BLUNT,
		WeaponType.BOW,
		WeaponType.CROSSBOW,
		WeaponType.DAGGER,
		WeaponType.DUAL,
		WeaponType.DUALDAGGER,
		WeaponType.DUALFIST,
		WeaponType.ETC,
		WeaponType.FISHINGROD,
		WeaponType.POLE,
		WeaponType.FIST,
		WeaponType.RAPIER,
		WeaponType.SWORD
	}),
	PENDRIE("Pendrie", new ItemType[] {}),
	DECO("Deco", new ItemType[] {}),
	ALL("Tout", new ItemType[] {}),
	OTHER("Autre", new ItemType[] {}),
	NONE("C est quoi ces truc", new ItemType[] {});
	
	private final String _name;
	private final ItemType[] _types;
	
	CustomType(String name, ItemType[] types)
	{
		_name = name;
		_types = types;
	}
	
	public ItemType[] getTypes()
	{
		return _types;
	}
	
	public String getName()
	{
		return _name;
	}
}
