/*
 * Copyright (C) 2004-2016 L2J Server
 * 
 * This file is part of L2J Server.
 * 
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.custom.interf;

/**
 * @author Rizaar
 */
public abstract class Custom_Storage
{
	
	public boolean _syncro = false;
	
	/**
	 * @return
	 */
	public boolean isSyncro()
	{
		return _syncro;
	}
	
	/**
	 * indique qu'une valeur a ete change
	 */
	public void setUnSyncro()
	{
		_syncro = false;
	}
	
	/**
	 * cette methode doit etre appeler à la fin de store si l'execution c'est bien passé
	 */
	public void setSyncro()
	{
		_syncro = true;
	}
	
	public abstract void restore();
	
	/**
	 * store droit check l'etat de _syncro pour savoir si il a besoin de seauv les données
	 */
	public abstract void store();
	
	/**
	 * @param objectId
	 * @return
	 */
	public abstract void restore(int objectId);
	
}
