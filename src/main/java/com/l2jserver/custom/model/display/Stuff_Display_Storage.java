/*
 * Copyright (C) 2004-2016 L2J Server
 * 
 * This file is part of L2J Server.
 * 
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.custom.model.display;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.l2jserver.commons.database.pool.impl.ConnectionFactory;
import com.l2jserver.custom.interf.Custom_Storage;
import com.l2jserver.custom.model.display.Custom_Stuff_Display.VisualArmors;

/**
 * @author Rizaar
 */
public class Stuff_Display_Storage extends Custom_Storage
{
	public static final String SELECT = "SELECT * FROM custom_display_stuff WHERE charId=?";
	public static final String INSERT = "INSERT INTO custom_display_stuff (charId, setId) values (?, ?)";
	public static final String UPDATE = "UPDATE custom_display_stuff SET oneHand=?,TwoHand=?,duals=?,bow=?,pole=?,lHand=?,armor=?,leggings=?,feet=?,gloves=?,cloak=?,shirt=? WHERE charId=? AND setId=?";
	
	public Custom_Stuff_Display _display;
	
	public Stuff_Display_Storage(Custom_Stuff_Display display)
	{
		_display = display;
	}
	
	@Override
	public void restore()
	{
		try (Connection con = ConnectionFactory.getInstance().getConnection())
		{
			PreparedStatement statement = con.prepareStatement(SELECT);
			statement.setInt(1, _display._pc.getObjectId());
			ResultSet rset = statement.executeQuery();
			int Nbset = 0;
			while (rset.next())
			{
				
				if (Nbset >= Custom_Stuff_Display.NB_SET)
				{
					Custom_Stuff_Display._log.info("[Custom Display] Found more visual set than expected for : " + _display._pc.getName());
					con.close();
					return;
				}
				
				int setId = rset.getInt("setId");
				
				_display.setVisualArmor(VisualArmors.OneHand, setId, rset.getInt("oneHand"));
				_display.setVisualArmor(VisualArmors.TwoHand, setId, rset.getInt("TwoHand"));
				_display.setVisualArmor(VisualArmors.Dual, setId, rset.getInt("duals"));
				_display.setVisualArmor(VisualArmors.Bow, setId, rset.getInt("bow"));
				_display.setVisualArmor(VisualArmors.Pole, setId, rset.getInt("pole"));
				_display.setVisualArmor(VisualArmors.LHand, setId, rset.getInt("lHand"));
				_display.setVisualArmor(VisualArmors.Armor, setId, rset.getInt("armor"));
				_display.setVisualArmor(VisualArmors.Legs, setId, rset.getInt("leggings"));
				_display.setVisualArmor(VisualArmors.Feet, setId, rset.getInt("feet"));
				_display.setVisualArmor(VisualArmors.Gloves, setId, rset.getInt("gloves"));
				_display.setVisualArmor(VisualArmors.Cloak, setId, rset.getInt("cloak"));
				_display.setVisualArmor(VisualArmors.Shirt, setId, rset.getInt("shirt"));
				setId++;
			}
			statement.close();
		}
		catch (Exception e)
		{
			Custom_Stuff_Display._log.info("Could not restore " + _display._pc.getObjectId() + " display data " + e.getMessage(), e);
		}
	}
	
	@Override
	public void store()
	{
		// si rien n'a ete change depuis la dernier store return
		/*
		 * if (!isSyncro()) { return; }
		 */
		if (!checkIfExistC())
		{
			insert();
		}
		
		for (int setId = 0; setId < Custom_Stuff_Display.NB_SET; setId++)
		{
			try (Connection con = ConnectionFactory.getInstance().getConnection())
			{
				try (PreparedStatement statement = con.prepareStatement(UPDATE))
				{
					int[] vi = _display.visualArmors[setId];
					int replaceIndex = 1;
					for (VisualArmors va : VisualArmors.values())
					{
						statement.setInt(replaceIndex++, vi[va.ordinal()]);
					}
					statement.setInt(replaceIndex++, _display._pc.getObjectId());
					statement.setInt(replaceIndex++, setId);
					statement.execute();
					
					statement.close();
				}
			}
			catch (Exception e)
			{
				Custom_Stuff_Display._log.info("Could not store character " + _display._pc.getObjectId() + " visual armors data: ", e);
			}
		}
		setSyncro();
	}
	
	public boolean checkIfExistC()
	{
		try (Connection con = ConnectionFactory.getInstance().getConnection())
		{
			try (PreparedStatement statement = con.prepareStatement(SELECT))
			{
				statement.setInt(1, _display._pc.getObjectId());
				ResultSet rset = statement.executeQuery();
				
				boolean result = rset.next();
				statement.close();
				return result;
			}
		}
		catch (Exception e)
		{
			Custom_Stuff_Display._log.info("Could checkIfExistC " + _display._pc.getObjectId() + " display data " + e.getMessage(), e);
		}
		return false;
	}
	
	public void insert()
	{
		for (int setId = 0; setId < Custom_Stuff_Display.NB_SET; setId++)
		{
			try (Connection con = ConnectionFactory.getInstance().getConnection())
			{
				try (PreparedStatement statement = con.prepareStatement(INSERT))
				{
					statement.setInt(1, _display._pc.getObjectId());
					statement.setInt(2, setId);
					statement.execute();
					statement.close();
				}
			}
			catch (Exception e)
			{
				Custom_Stuff_Display._log.info("Could not insert " + _display._pc.getObjectId() + " display data " + e.getMessage(), e);
			}
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.l2jserver.custom.interf.Custom_Storage#restore(int)
	 */
	@Override
	public void restore(int objectId)
	{
		// TODO Auto-generated method stub
		
	}
	
}
