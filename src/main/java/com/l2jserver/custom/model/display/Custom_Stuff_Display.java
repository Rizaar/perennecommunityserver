/*
 * Copyright (C) 2004-2016 L2J Server
 * 
 * This file is part of L2J Server.
 * 
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.custom.model.display;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.Config;
import com.l2jserver.gameserver.datatables.ItemTable;
import com.l2jserver.gameserver.enums.Race;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.base.ClassId;
import com.l2jserver.gameserver.model.itemcontainer.Inventory;
import com.l2jserver.gameserver.model.itemcontainer.PcInventory;
import com.l2jserver.gameserver.model.items.L2Item;
import com.l2jserver.gameserver.model.items.L2Weapon;
import com.l2jserver.gameserver.model.items.instance.L2ItemInstance;
import com.l2jserver.gameserver.model.items.type.EtcItemType;
import com.l2jserver.gameserver.model.items.type.ItemType;
import com.l2jserver.gameserver.model.items.type.WeaponType;

/**
 * @author Rizaar
 */
public class Custom_Stuff_Display extends CustomDisplay
{
	public static final int NB_SET = 3;
	/*
	 * <set name="type" value="HEAVY" /> <set name="type" value="LIGHT" /> <set name="type" value="MAGIC" />
	 */
	public static final Logger _log = LoggerFactory.getLogger(Custom_Stuff_Display.class);
	
	private final static Map<WeaponType, VisualArmors> WeaponTypeToVisual = new HashMap<>(14);
	private final static Map<WeaponType, VisualArmors> Weapon2HandTypeToVisual = new HashMap<>(2);
	private final static Map<VisualArmors, Integer> VisualToSlot = new HashMap<>(10);
	
	private final static HashMap<WeaponType, ArrayList<ClassId>> _canDisplay = new HashMap<>();
	private final static HashMap<WeaponType, Integer> _displayConflitId = new HashMap<>();
	
	private final static ArrayList<ItemType> _typeUnSkinable = new ArrayList<>();
	private static ArrayList<Integer> _idUnSkinable = new ArrayList<>();
	private static ArrayList<Integer> _hairCutId = new ArrayList<>();
	private final static HashMap<VisualArmors, String> _defaultIcon = new HashMap<>();
	
	private final static HashMap<Integer, VisualArmors> _armorSlotToVisual = new HashMap<>();
	
	private final Stuff_Display_Storage _displayStorage = new Stuff_Display_Storage(this);
	
	static
	{
		_armorSlotToVisual.put(Inventory.PAPERDOLL_CHEST, VisualArmors.Armor);
		_armorSlotToVisual.put(Inventory.PAPERDOLL_LEGS, VisualArmors.Legs);
		_armorSlotToVisual.put(Inventory.PAPERDOLL_GLOVES, VisualArmors.Gloves);
		_armorSlotToVisual.put(Inventory.PAPERDOLL_FEET, VisualArmors.Feet);
		_armorSlotToVisual.put(Inventory.PAPERDOLL_CLOAK, VisualArmors.Cloak);
		_armorSlotToVisual.put(Inventory.PAPERDOLL_UNDER, VisualArmors.Shirt);
		
		_defaultIcon.put(VisualArmors.Armor, "Dress.haut");
		_defaultIcon.put(VisualArmors.Legs, "Dress.bas");
		_defaultIcon.put(VisualArmors.Gloves, "Dress.gant");
		_defaultIcon.put(VisualArmors.Feet, "Dress.botte");
		_defaultIcon.put(VisualArmors.LHand, "Dress.shield");
		_defaultIcon.put(VisualArmors.Cloak, "Dress.cape");
		_defaultIcon.put(VisualArmors.Bow, "Dress.arc");
		_defaultIcon.put(VisualArmors.Dual, "Dress.fist");
		_defaultIcon.put(VisualArmors.OneHand, "Dress.1h");
		_defaultIcon.put(VisualArmors.TwoHand, "Dress.2h");
		_defaultIcon.put(VisualArmors.Pole, "Dress.lance");
		_defaultIcon.put(VisualArmors.Shirt, "Dress.g_co_sexy_underwear");
		
		// Need Opti
		
		WeaponTypeToVisual.put(WeaponType.BLUNT, VisualArmors.OneHand);
		WeaponTypeToVisual.put(WeaponType.SWORD, VisualArmors.OneHand);
		WeaponTypeToVisual.put(WeaponType.DAGGER, VisualArmors.OneHand);
		WeaponTypeToVisual.put(WeaponType.ETC, VisualArmors.OneHand);
		WeaponTypeToVisual.put(WeaponType.BOW, VisualArmors.Bow);
		WeaponTypeToVisual.put(WeaponType.CROSSBOW, VisualArmors.Bow);
		WeaponTypeToVisual.put(WeaponType.DUALDAGGER, VisualArmors.Dual);
		WeaponTypeToVisual.put(WeaponType.DUALFIST, VisualArmors.Dual);
		WeaponTypeToVisual.put(WeaponType.FIST, VisualArmors.Dual);
		WeaponTypeToVisual.put(WeaponType.POLE, VisualArmors.Pole);
		WeaponTypeToVisual.put(WeaponType.RAPIER, VisualArmors.OneHand);
		// WeaponTypeToVisual.put(WeaponType.NONE, VisualArmors.OneHand);
		
		Weapon2HandTypeToVisual.put(WeaponType.ANCIENTSWORD, VisualArmors.TwoHand);
		Weapon2HandTypeToVisual.put(WeaponType.BLUNT, VisualArmors.TwoHand);
		Weapon2HandTypeToVisual.put(WeaponType.SWORD, VisualArmors.TwoHand);
		Weapon2HandTypeToVisual.put(WeaponType.BOW, VisualArmors.Bow);
		Weapon2HandTypeToVisual.put(WeaponType.CROSSBOW, VisualArmors.Bow);
		Weapon2HandTypeToVisual.put(WeaponType.DUALDAGGER, VisualArmors.Dual);
		Weapon2HandTypeToVisual.put(WeaponType.DUALFIST, VisualArmors.Dual);
		Weapon2HandTypeToVisual.put(WeaponType.FIST, VisualArmors.Dual);
		Weapon2HandTypeToVisual.put(WeaponType.DUAL, VisualArmors.Dual);
		Weapon2HandTypeToVisual.put(WeaponType.POLE, VisualArmors.Pole);
		
		VisualToSlot.put(VisualArmors.OneHand, Inventory.PAPERDOLL_RHAND);
		VisualToSlot.put(VisualArmors.TwoHand, Inventory.PAPERDOLL_RHAND);
		VisualToSlot.put(VisualArmors.Bow, Inventory.PAPERDOLL_RHAND);
		VisualToSlot.put(VisualArmors.Dual, Inventory.PAPERDOLL_RHAND);
		VisualToSlot.put(VisualArmors.Pole, Inventory.PAPERDOLL_RHAND);
		VisualToSlot.put(VisualArmors.LHand, Inventory.PAPERDOLL_LHAND);
		VisualToSlot.put(VisualArmors.Armor, Inventory.PAPERDOLL_CHEST);
		
		VisualToSlot.put(VisualArmors.Shirt, Inventory.PAPERDOLL_UNDER);
		VisualToSlot.put(VisualArmors.Legs, Inventory.PAPERDOLL_LEGS);
		VisualToSlot.put(VisualArmors.Feet, Inventory.PAPERDOLL_FEET);
		VisualToSlot.put(VisualArmors.Gloves, Inventory.PAPERDOLL_GLOVES);
		VisualToSlot.put(VisualArmors.Cloak, Inventory.PAPERDOLL_CLOAK);
		
		// WeaponType.DUALDAGGER
		ArrayList<ClassId> canEquipDualDagge = new ArrayList<>();
		canEquipDualDagge.add(ClassId.fighter);
		canEquipDualDagge.add(ClassId.elvenFighter);
		canEquipDualDagge.add(ClassId.elvenMage);
		canEquipDualDagge.add(ClassId.darkFighter);
		canEquipDualDagge.add(ClassId.darkMage);
		canEquipDualDagge.add(ClassId.dwarvenFighter);
		
		// WeaponType.RAPIER
		ArrayList<ClassId> canEquipRapier = new ArrayList<>();
		canEquipRapier.add(ClassId.maleSoldier);
		canEquipRapier.add(ClassId.femaleSoldier);
		
		// WeaponType.CROSSBOW
		ArrayList<ClassId> canEquipCrossBow = new ArrayList<>();
		canEquipCrossBow.add(ClassId.maleSoldier);
		canEquipCrossBow.add(ClassId.femaleSoldier);
		
		_canDisplay.put(WeaponType.DUALDAGGER, canEquipDualDagge);
		_canDisplay.put(WeaponType.RAPIER, canEquipRapier);
		_canDisplay.put(WeaponType.CROSSBOW, canEquipCrossBow);
		
		_displayConflitId.put(WeaponType.DUALDAGGER, 2516);// saber*saber
		_displayConflitId.put(WeaponType.RAPIER, 2);// Long sword
		_displayConflitId.put(WeaponType.CROSSBOW, 13);// Short Bow
		
		_typeUnSkinable.add(EtcItemType.ARROW);
		_typeUnSkinable.add(EtcItemType.BOLT);
		
		_idUnSkinable = Config.CUSTOM_FASHION_NOT_TRANSMUTABLE;
		
		_hairCutId = Config.CUSTOM_FASHION_HAIR_CUT;
		
	}
	
	public enum VisualArmors
	{
		OneHand,
		TwoHand,
		Dual,
		Bow,
		Pole,
		LHand,
		Armor,
		Legs,
		Feet,
		Gloves,
		Cloak,
		Shirt
	}
	
	public int visualArmors[][] = new int[NB_SET][VisualArmors.values().length];
	
	public Custom_Stuff_Display(L2PcInstance pc)
	{
		super(pc);
		restore();
	}
	
	public boolean isUsingVisualArmors()
	{
		return _stuff_display_enabled;
	}
	
	public void setUsingVisualArmors(boolean _isUsingVisualArmors)
	{
		_stuff_display_enabled = _isUsingVisualArmors;
	}
	
	public void setVisualArmor(VisualArmors position, int setId, int itemId)
	{
		if (position == null)
		{
			return;
		}
		else if (visualArmors == null)
		{
			System.out.println("[Custom_Display] visualArmors[][] null 0_L");
			return;
		}
		visualArmors[setId][position.ordinal()] = itemId;
	}
	
	public int getArmorSet()
	{
		return _currentArmorSet;
	}
	
	public void setArmorSet(int set)
	{
		_currentArmorSet = set;
	}
	
	public int getWeaponSet()
	{
		return _currentWeaponSet;
	}
	
	public void setWeaponSet(int set)
	{
		_currentWeaponSet = set;
	}
	
	/**********************************************************************************
	 * BDD
	 *********************************************************************************/
	@Override
	public void restore()
	{
		super.restore();
		_displayStorage.restore();
	}
	
	@Override
	public void store()
	{
		super.store();
		_displayStorage.store();
	}
	
	/**********************************************************************************
	 * NETWORK
	 * @param InventorySlot
	 * @return
	 *********************************************************************************/
	
	@SuppressWarnings("null")
	public int getAssociateVisualArmor(int InventorySlot)
	{
		PcInventory pcInv = _pc.getInventory();
		if (!_stuff_display_enabled)
		{
			if (pcInv.getPaperdollItem(InventorySlot) == null)
			{
				return 0;
			}
			return pcInv.getPaperdollItem(InventorySlot).getDisplayId();
		}
		
		// L2ItemInstance equipedBody;
		int bodyType2 = 0;
		int assoChestVisualArmor = -1;
		
		final L2ItemInstance equipedItem = pcInv.getPaperdollItem(InventorySlot); // si equipedItem -> null
		ItemType equipedItemType = null; // alors equipedItemType -> null
		
		int[] viA = _currentArmorSet == -1 ? null : visualArmors[_currentArmorSet];
		int[] viW = _currentWeaponSet == -1 ? null : visualArmors[_currentWeaponSet];
		
		// si le slot n'est pas vide
		if (equipedItem != null)
		{
			// verif si l'apparence de l'item peu etre reskin
			if (_idUnSkinable.contains(Integer.toString(equipedItem.getId())))
			{
				return equipedItem.getDisplayId();
			}
			// recupe le type de l'item
			equipedItemType = equipedItem.getItemType();
		}
		
		// force display if equiped item is UnSkinable
		
		// Slot non surchargable (par un fullbody ou fulldress)
		// si pas d'item equipe retourne rien pour :
		switch (InventorySlot)
		{
			case Inventory.PAPERDOLL_HAIR:
			case Inventory.PAPERDOLL_HAIR2:
			case Inventory.PAPERDOLL_LHAND:
			case Inventory.PAPERDOLL_RHAND:
			case Inventory.PAPERDOLL_CLOAK:
				if (equipedItem == null)
				{
					return 0;
				}
				break;
			case Inventory.PAPERDOLL_CHEST:
				if (equipedItem == null)
				{
					if (viA != null)
					{
						return viA[VisualArmors.Shirt.ordinal()];
					}
					return 0;
				}
				break;
		}
		
		// Slot surchargable (par un fulldress)
		switch (InventorySlot)
		{
			case Inventory.PAPERDOLL_LEGS:
			case Inventory.PAPERDOLL_FEET:
			case Inventory.PAPERDOLL_GLOVES:
				assoChestVisualArmor = getAssociateVisualArmor(Inventory.PAPERDOLL_CHEST);// can be opti
				// si un chest est visible
				if (assoChestVisualArmor != 0)
				{
					bodyType2 = ItemTable.getInstance().getTemplate(assoChestVisualArmor).getBodyPart();
					// et que le type de ce chest visible est un alldress
					if (bodyType2 == L2Item.SLOT_ALLDRESS)
					{
						return 0;
					}
				}
		}
		
		// Slot surchargable (par un fulldbody)
		switch (InventorySlot)
		{
			case Inventory.PAPERDOLL_LEGS:
				if (assoChestVisualArmor != 0)
				{
					// et que le type de ce chest visible est une full armor ou alldress
					if (bodyType2 == L2Item.SLOT_FULL_ARMOR)
					{
						// retourne 0 (car le chest va utilise ce slot)
						return 0;
					}
				}
		}
		
		switch (InventorySlot)
		{
			case Inventory.PAPERDOLL_HAIR:// --------------------------------------------------------------------------------
			case Inventory.PAPERDOLL_HAIR2:
				return pcInv.getPaperdollItem(InventorySlot).getDisplayId();
			case Inventory.PAPERDOLL_FEET:
			case Inventory.PAPERDOLL_GLOVES:
				break;
			case Inventory.PAPERDOLL_LHAND:// --------------------------------------------------------------------------------
				// les Kamael
				if (getRace() == Race.KAMAEL)
				{
					// peuvent seulement afficher des fleches en LHAND
					if ((equipedItemType != EtcItemType.ARROW) && (equipedItemType != EtcItemType.BOLT))
					{
						return 0;// rien
					}
					return equipedItem.getId(); // retourne la fleche/bolt equipe
				}
				// les non kamael
				// ne peuvent pas afficher des bolt en LHand
				if (equipedItemType == EtcItemType.BOLT)
				{
					return Config.CUSTOM_DISPLAY_DEFAULT_ARROW_ID;// wooden arrow
				}
				else if (equipedItemType == EtcItemType.ARROW)
				{
					return equipedItem.getId(); // retourne la fleche equipe
				}
				
				// si un set est selectionné | et qu'il y a un visual reg en LHand | et que l'item dans ce slot n'est pas unskinable
				/*
				 * if ((viW != null) && (viW[VisualArmors.LHand.ordinal()] > 0)) { // retourne la valeur du visual return viW[VisualArmors.LHand.ordinal()]; }
				 */
				
				if (viW != null)
				{
					final int currentVisual = viW[VisualArmors.LHand.ordinal()];
					if (currentVisual > 0)
					{
						return currentVisual;
					}
				}
				return _pc.getInventory().getPaperdollItemId(Inventory.PAPERDOLL_LHAND);
			
			case Inventory.PAPERDOLL_RHAND:// --------------------------------------------------------------------------------
				// equipedItem == null à deja ete test sur le switch "Slot non surchargable"
				int equipedItemBodyPart = equipedItem.getItem().getBodyPart();
				
				// 2Hand
				if (equipedItemBodyPart == L2Item.SLOT_LR_HAND)
				{
					return getWeaponFilter(getWeaponVisualArmorItemId(Weapon2HandTypeToVisual.get(equipedItemType)));
				}
				// 1Hand
				return getWeaponFilter(getWeaponVisualArmorItemId(WeaponTypeToVisual.get(equipedItemType)));
			
			case Inventory.PAPERDOLL_CHEST:// --------------------------------------------------------------------------------
				// equipedItem == null à deja ete test sur le switch "Slot non surchargable"
				int equipedItemBodyPart2 = equipedItem.getItem().getBodyPart();
				
				// l'item equipe est un full armor
				if (equipedItemBodyPart2 == L2Item.SLOT_FULL_ARMOR)
				{
					// si un set est activé il n'y a pas de visual Legs mais il y a un chest
					if ((viA != null) && (viA[VisualArmors.Armor.ordinal()] > 0) && (viA[VisualArmors.Legs.ordinal()] == 0))
					{
						final int chestVisualItemBodyPart = ItemTable.getInstance().getTemplate(viA[VisualArmors.Armor.ordinal()]).getBodyPart();
						// si le visual du chest n'est pas un full armor ou un alldress
						if ((chestVisualItemBodyPart != L2Item.SLOT_FULL_ARMOR) && (chestVisualItemBodyPart != L2Item.SLOT_ALLDRESS))
						{
							// force l'affichage de l'item equipe
							return equipedItem.getDisplayId();
						}
					}
				}
				break;
			
			case Inventory.PAPERDOLL_LEGS:
				// si L'item equiper est un full body
				// retourne le leg visual
				final L2ItemInstance equipedChestItem = pcInv.getPaperdollItem(Inventory.PAPERDOLL_CHEST);
				
				if (viA != null)
				{
					if (equipedChestItem != null)
					{
						if (equipedChestItem.getItem().getBodyPart() == L2Item.SLOT_FULL_ARMOR)// si un fullbody est equipe
						{
							return viA[VisualArmors.Legs.ordinal()];
						}
					}
					if (equipedItem == null)// et si il n'y a pas d'item equipe
					{
						return viA[VisualArmors.Shirt.ordinal()];
					}
				}
				
				break;
			case Inventory.PAPERDOLL_CLOAK:// --------------------------------------------------------------------------------
				if (!_displayCloak)
				{
					return 0;
				}
				break;
			default:
				_log.info("Inventory Slot not found : " + InventorySlot);
				return 0;
		}
		return getAssociatVisualArmorType(viA, InventorySlot);
	}
	
	/**********************************************************************************
	 * Impl
	 *********************************************************************************/
	
	/**
	 * @param va
	 * @param setId
	 */
	public void updateVisual(VisualArmors va, int setId)
	{
		
		if (va.ordinal() <= 4)
		{
			updateVisualWeapon(va, setId, VisualToSlot.get(va));
		}
		else
		{
			updateVisualArmor(va, setId, VisualToSlot.get(va));
		}
	}
	
	/**
	 * @param position
	 * @param setId
	 * @return retourne l'icone de la visual armor donne ,icone.NOIMAGE si null
	 */
	public String getVisualArmorIcon(VisualArmors position, int setId)
	{
		if (visualArmors[setId][position.ordinal()] > 0)
		{
			L2Item item = ItemTable.getInstance().getTemplate(visualArmors[setId][position.ordinal()]);
			if (item != null)
			{
				return item.getIcon();
			}
			_pc.sendMessage("[Dressme] item not found id " + visualArmors[setId][position.ordinal()]);
		}
		return _defaultIcon.get(position);
		// return "icon.NOIMAGE";
	}
	
	/**********************************************************************************
	 * Tools
	 *********************************************************************************/
	
	/**
	 * cherche la visual armor ID, si < 0 retourne l'arme equipe par le joueur
	 * @param position
	 * @return visualArmor ID, ou l'arme equipe si null
	 */
	private final int getWeaponVisualArmorItemId(VisualArmors position)
	{
		
		if ((_currentWeaponSet == -1) || (position == null))
		{
			return _pc.getInventory().getPaperdollItemId(Inventory.PAPERDOLL_RHAND);
		}
		
		final int currentVisual = visualArmors[_currentWeaponSet][position.ordinal()];
		
		if (currentVisual > 0)
		{
			return currentVisual;
		}
		return _pc.getInventory().getPaperdollItemId(Inventory.PAPERDOLL_RHAND);
	}
	
	/**
	 * donne l'id du type d'item associe au visualArmors
	 * @param vi
	 * @param InventoryPos
	 * @return
	 */
	private Integer getAssociatVisualArmorType(int[] vi, int InventoryPos)
	{
		VisualArmors vT = _armorSlotToVisual.get(InventoryPos);
		
		PcInventory pcInv = _pc.getInventory();
		// TODO je suis pas sur que le test du fulldress soit util ici
		if ((pcInv.getPaperdollItem(InventoryPos) == null) /* && ((pcInv.getPaperdollItem(Inventory.PAPERDOLL_CHEST) == null) || (pcInv.getPaperdollItem(Inventory.PAPERDOLL_CHEST).getArmorItem().getBodyPart() != L2Item.SLOT_ALLDRESS)) */)
		{
			return 0;
		}
		if ((vi != null) && (vi[vT.ordinal()] > 0))
		{
			return ItemTable.getInstance().getTemplate(vi[vT.ordinal()]).getDisplayId();
		}
		return pcInv.getPaperdollItemId(InventoryPos);
	}
	
	public void updateVisualWeapon(VisualArmors position, int setId, int inventoryId)
	{
		PcInventory pcInv = _pc.getInventory();
		if (pcInv.getPaperdollItem(inventoryId) == null)
		{
			setVisualArmor(position, setId, 0);
			return;
		}
		
		L2Weapon weapon = pcInv.getPaperdollItem(inventoryId).getWeaponItem();
		if (weapon == null)
		{
			_log.info("Debug L2VisualArmors : updateVisualWeapon weapon null at");
			return;
		}
		
		WeaponType weaponType = weapon.getItemType();
		
		VisualArmors vi;
		if (weapon.getBodyPart() == L2Item.SLOT_LR_HAND)
		{
			vi = Weapon2HandTypeToVisual.get(weaponType);
		}
		else
		{
			vi = WeaponTypeToVisual.get(weaponType);
		}
		if (vi == null)
		{
			_log.info("Debug L2VisualArmors : updateVisualWeapon vi == null");
			return;
		}
		else if (vi != position)
		{
			_log.info("Debug L2VisualArmors : updateVisualWeapon vi != position 0_O -> vi " + vi.name() + " position " + position.name());
			return;
		}
		
		setVisualArmor(position, setId, pcInv.getPaperdollItem(inventoryId).getDisplayId());
	}
	
	public void updateVisualArmor(VisualArmors position, int setId, int inventoryId)
	{
		PcInventory pcInv = _pc.getInventory();
		L2ItemInstance item = pcInv.getPaperdollItem(inventoryId);
		if (item == null)
		{
			setVisualArmor(position, setId, 0);
			return;
		}
		else if (_idUnSkinable.contains(Integer.toString(item.getId())) || _typeUnSkinable.contains(item.getItemType()))
		{
			_pc.sendMessage("L'apparence de cette item ne peut etre enregistré");
			return;
		}
		setVisualArmor(position, setId, pcInv.getPaperdollItem(inventoryId).getDisplayId());
	}
	
	public int getWeaponFilter(int itemId)
	{
		L2Item item = ItemTable.getInstance().getTemplate(itemId);
		
		if (item == null)
		{
			return 0;
		}
		if (!(item instanceof L2Weapon))
		{
			return item.getDisplayId();
		}
		
		WeaponType weaponType = ((L2Weapon) item).getItemType();
		// conflit
		ArrayList<ClassId> canDisplay = _canDisplay.get(weaponType);
		
		if ((canDisplay != null) && !canDisplay.contains(getDisplayClass()))
		{
			return _displayConflitId.get(weaponType);
		}
		return item.getDisplayId();
	}
	
	public int getHairStyle()
	{
		int hair1 = getAssociateVisualArmor(Inventory.PAPERDOLL_HAIR);
		int hair2 = getAssociateVisualArmor(Inventory.PAPERDOLL_HAIR2);
		int chest = getAssociateVisualArmor(Inventory.PAPERDOLL_CHEST);
		if ((_hairCutId.contains(Integer.toString(hair1))) || (_hairCutId.contains(Integer.toString(hair2))) || _hairCutId.contains(Integer.toString(chest)))
		{
			return Config.CUSTOM_DISPLAY_HAIRCUT_ID;
		}
		return _pc.getAppearance().getHairStyle();
	}
	
	/**
	 * for custom sitting
	 * @return
	 */
	public int getFace()
	{
		int faceId = _pc.getAppearance().getFace();
		if (isOnCustomSit)
		{
			faceId += 10;
		}
		return faceId;
	}
}
