/*
 * Copyright (C) 2004-2016 L2J Server
 * 
 * This file is part of L2J Server.
 * 
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.custom.model.display;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.pool.impl.ConnectionFactory;
import com.l2jserver.custom.interf.Custom_Storage;
import com.l2jserver.gameserver.enums.Race;
import com.l2jserver.gameserver.enums.Sex;
import com.l2jserver.gameserver.model.base.ClassId;
import com.l2jserver.gameserver.network.serverpackets.CharInfo;
import com.l2jserver.gameserver.network.serverpackets.UserInfo;

/**
 * @author Rizaar
 */
public class Display_Storage extends Custom_Storage
{
	public static final String SELECT = "SELECT * FROM custom_display WHERE charId=?";
	public static final String INSERT = "INSERT INTO custom_display (charId) values (?)";
	public static final String UPDATE = "REPLACE INTO custom_display (charId,race,classId,sex) VALUE(?,?,?,?)";
	public static final Logger _log = LoggerFactory.getLogger(Custom_Stuff_Display.class);
	
	public CustomDisplay _display;
	
	public Display_Storage(CustomDisplay display)
	{
		_display = display;
	}
	
	@Override
	public void restore()
	{
		try (Connection con = ConnectionFactory.getInstance().getConnection())
		{
			try (PreparedStatement statement = con.prepareStatement(SELECT))
			{
				statement.setInt(1, _display._pc.getObjectId());
				ResultSet rset = statement.executeQuery();
				if (rset.next())
				{
					// race
					int raceId = rset.getInt("race");
					if (raceId == -1)
					{
						_display._race = null;
					}
					else
					{
						_display._race = Race.values()[raceId];
					}
					
					int classId = rset.getInt("classId");
					if (classId == -1)
					{
						_display._class = null;
					}
					else
					{
						_display._class = ClassId.values()[classId];
					}
					
					int sexId = rset.getInt("sex");
					if (sexId == -1)
					{
						_display._sex = null;
					}
					else
					{
						_display._sex = Sex.values()[sexId];
					}
					
				}
				statement.close();
				setSyncro();
				_display._pc.setBaseClass(_display.getClassId());
				_display._pc.getTemplate().setRace(_display.getRace());
				_display._pc.sendPacket(new UserInfo(_display._pc));
				_display._pc.broadcastPacket(new CharInfo(_display._pc));
			}
		}
		catch (Exception e)
		{
			CustomDisplay._log.info("Could not restore " + _display._pc.getObjectId() + " display data " + e.getMessage(), e);
		}
	}
	
	@Override
	public void restore(int objectId)
	{
		try (Connection con = ConnectionFactory.getInstance().getConnection())
		{
			try (PreparedStatement statement = con.prepareStatement(SELECT))
			{
				statement.setInt(1, objectId);
				ResultSet rset = statement.executeQuery();
				if (rset.next())
				{
					// race
					int raceId = rset.getInt("race");
					if (raceId == -1)
					{
						_display._race = null;
					}
					else
					{
						_display._race = Race.values()[raceId];
					}
					int classId = rset.getInt("classId");
					if (classId == -1)
					{
						_display._class = null;
					}
					else
					{
						_display._class = ClassId.values()[classId];
					}
					
					int sexId = rset.getInt("sex");
					if (sexId == -1)
					{
						_display._sex = null;
					}
					else
					{
						_display._sex = Sex.values()[sexId];
					}
					
				}
				statement.close();
				setSyncro();
			}
		}
		catch (Exception e)
		{
			_log.info("Could not restore " + _display._pc.getObjectId() + " display data " + e.getMessage(), e);
		}
	}
	
	@Override
	public void store()
	{
		// si rien n'a ete change depuis la dernier store return
		/*
		 * if (!isSyncro()) { return; }
		 */
		if (!checkIfExist())
		{
			insert();
		}
		try (Connection con = ConnectionFactory.getInstance().getConnection())
		{
			
			PreparedStatement statement = con.prepareStatement(UPDATE);
			
			statement.setInt(1, _display._pc.getObjectId());
			statement.setInt(2, _display._race == null ? -1 : _display._race.ordinal());
			statement.setInt(3, _display._class == null ? -1 : _display._class.ordinal());
			statement.setInt(4, _display._sex == null ? -1 : _display._sex.ordinal());
			statement.execute();
			con.close();
		}
		catch (Exception e)
		{
			CustomDisplay._log.info("Could not store " + _display._pc.getObjectId() + " display data " + e.getMessage(), e);
		}
		setSyncro();
	}
	
	public boolean checkIfExist()
	{
		try (Connection con = ConnectionFactory.getInstance().getConnection())
		{
			try (PreparedStatement statement = con.prepareStatement(SELECT))
			{
				statement.setInt(1, _display._pc.getObjectId());
				ResultSet rset = statement.executeQuery();
				
				boolean result = rset.next();
				statement.close();
				return result;
			}
		}
		catch (Exception e)
		{
			CustomDisplay._log.info("Could not checkifExist " + _display._pc.getObjectId() + " display data " + e.getMessage(), e);
		}
		return false;
	}
	
	public void insert()
	{
		try (Connection con = ConnectionFactory.getInstance().getConnection())
		{
			try (PreparedStatement statement = con.prepareStatement(INSERT))
			{
				statement.setInt(1, _display._pc.getObjectId());
				statement.execute();
				statement.close();
			}
		}
		catch (Exception e)
		{
			CustomDisplay._log.info("Could not insert " + _display._pc.getObjectId() + " display data " + e.getMessage(), e);
		}
	}
	
}
