/*
 * Copyright (C) 2004-2016 L2J Server
 * 
 * This file is part of L2J Server.
 * 
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.custom.model.display;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.custom.util.Seat;
import com.l2jserver.gameserver.data.xml.impl.PlayerTemplateData;
import com.l2jserver.gameserver.enums.Race;
import com.l2jserver.gameserver.enums.Sex;
import com.l2jserver.gameserver.model.Location;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.appearance.PcAppearance;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.actor.templates.L2PcTemplate;
import com.l2jserver.gameserver.model.base.ClassId;
import com.l2jserver.gameserver.network.serverpackets.CharInfo;
import com.l2jserver.gameserver.network.serverpackets.UserInfo;

/**
 * @author Rizaar
 */
public class CustomDisplay
{
	public static final Logger _log = LoggerFactory.getLogger(Custom_Stuff_Display.class);
	public static final String SELECT = "SELECT * FROM custom_display WHERE charId=?";
	public static final String INSERT = "INSERT INTO custom_display (charId) values (?)";
	public static final String UPDATE = "UPDATE custom_display SET race=?,classId=?,sex=? WHERE charId=?";
	
	public static double SIT_RANG = 50;
	
	public Race _race = null;
	public ClassId _class = null;
	public Sex _sex = null;
	public L2PcInstance _pc;
	public int objectId;
	
	public boolean _stuff_display_enabled = true;
	public int _currentArmorSet = -1;
	public int _currentWeaponSet = -1;
	public boolean _crestEnable = true;
	public boolean _displayCloak = true;
	public Display_Storage _storage = new Display_Storage(this);
	public double _height = -1;
	private L2PcTemplate template;
	private PcAppearance appearance;
	
	public boolean isOnCustomSit = false;
	
	public CustomDisplay(L2PcTemplate template, PcAppearance appearance, int objectId)
	{
		this.template = template;
		this.appearance = appearance;
		this.objectId = objectId;
		_storage = new Display_Storage(this);
		restore();
	}
	
	public CustomDisplay(L2PcInstance pc)
	{
		this._pc = pc;
		_storage = new Display_Storage(this);
	}
	
	public void onSit(boolean isOnSitDown)
	{
		if (!isOnSitDown)
		{
			if (isOnCustomSit)
			{
				isOnCustomSit = false;
				_pc.sendPacket(new UserInfo(_pc));
				_pc.sendPacket(new UserInfo(_pc));
				_pc.broadcastPacket(new CharInfo(_pc));
			}
			return;
		}
		
		if (_pc.getTarget() instanceof L2Npc)
		{
			L2Npc npc = (L2Npc) _pc.getTarget();
			if (npc.getTemplate().isSteatable())
			{
				double distance = npc.calculateDistance(_pc.getLocation(), true, false);
				if (distance < SIT_RANG)
				{
					
					int slot = npc.getEmptySeatSlot();
					if (slot == -1)
					{
						_pc.sendMessage("Il n'y a plus de place");
						return;
					}
					
					isOnCustomSit = true;
					// tp
					Location loc = npc.getLocation();
					Seat seat = npc.getTemplate().getSeats().get(slot);
					loc.setX(seat._diffX);// TODO radian h
					loc.setX(seat._diffY);// TODO radian h
					_pc.teleToLocation(loc);
					enableCustomFace();
					
					_pc.sendPacket(new UserInfo(_pc));
					_pc.sendPacket(new UserInfo(_pc));
					_pc.broadcastPacket(new CharInfo(_pc));
				}
			}
		}
	}
	
	public void enableCustomFace()
	{
		// Override by Stuff_Display
	}
	
	public boolean stuffDisplayEnabled()
	{
		return _stuff_display_enabled;
	}
	
	public void switchstuffDisplayEnabled()
	{
		_stuff_display_enabled = !_stuff_display_enabled;
	}
	
	public void switchcrestDisplayEnabled()
	{
		_crestEnable = !_crestEnable;
	}
	
	public boolean displayCloak()
	{
		return _displayCloak;
	}
	
	public void switchDisplayCloak()
	{
		_displayCloak = !_displayCloak;
	}
	
	public boolean crestEnabled()
	{
		return _crestEnable;
	}
	
	public int getClanCrestId()
	{
		if (!_crestEnable)
		{
			return 0;
		}
		return _pc.getClanCrestId();
	}
	
	public int getClanCrestLargeId()
	{
		if (!_crestEnable)
		{
			return 0;
		}
		return _pc.getClanCrestLargeId();
	}
	
	/*****************************************************************
	 * BDD
	 *****************************************************************/
	
	public void restore()
	{
		_storage.restore(objectId);
	}
	
	public void store()
	{
		_storage.store();
	}
	
	/*****************************************************************
	 * GET/SET
	 *****************************************************************/
	
	public void setClassId(ClassId classId)
	{
		_class = classId;
	}
	
	public void setHeight(double height)
	{
		_height = height;
	}
	
	public ClassId getDisplayClass()
	{
		if (_class == null)
		{
			return template.getClassId();
		}
		return _class;
	}
	
	public int getClassId()
	{
		if (_class == null)
		{
			return template.getClassId().ordinal();
		}
		return _class.getId();
	}
	
	public void setRace(Race race)
	{
		_race = race;
	}
	
	public Race getRace()
	{
		
		if (_race == null)
		{
			return template.getRace();
		}
		return _race;
	}
	
	public void setSex(Sex sex)
	{
		_sex = sex;
	}
	
	public boolean getSex()
	{
		if (_sex == null)
		{
			return appearance.getSex();
		}
		return _sex == Sex.FEMALE;
	}
	
	public double getRadius()
	{
		if ((_sex != null) && (_class != null))
		{
			L2PcTemplate temp = PlayerTemplateData.getInstance().getTemplate(_class);
			if (_sex == Sex.FEMALE)
			{
				return temp.getFCollisionRadiusFemale();
			}
			return temp.getfCollisionRadius();
		}
		return _pc.getCollisionRadius();
	}
	
	public double getHeight()
	{
		if (_height != -1)
		{
			return _height;
		}
		if ((_sex != null) && (_class != null))
		{
			L2PcTemplate temp = PlayerTemplateData.getInstance().getTemplate(_class);
			if (_sex == Sex.FEMALE)
			{
				return temp.getFCollisionHeightFemale();
			}
			return temp.getfCollisionHeight();
		}
		return _pc.getCollisionHeight();
	}
}
