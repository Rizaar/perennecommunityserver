/*
 * Copyright (C) 2004-2016 L2J Server
 * 
 * This file is part of L2J Server.
 * 
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.perenneCustom;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;

/**
 * @author Rizaar
 */

public class PerennePlayer
{
	/* Variable declaration */
	
	private final L2PcInstance _player;
	private static final Logger LOG = LoggerFactory.getLogger(PerennePlayer.class);
	private String desc = "";
	
	/**
	 * @return the desc
	 */
	public String getDesc()
	{
		return desc;
	}
	
	/**
	 * @param desc the desc to set
	 */
	public void setDesc(String desc)
	{
		desc = desc.replaceAll("action=", "");
		this.desc = desc;
	}
	
	private HashMap<String, String> variableTemporaire = new HashMap<>();
	
	/* Methodes */
	/**
	 * @param player
	 */
	public PerennePlayer(L2PcInstance player)
	{
		_player = player;
		
	}
	
	/**
	 * @return the _player
	 */
	public L2PcInstance get_player()
	{
		return _player;
	}
	
	/**
	 * @return the variableTemporaire
	 */
	public HashMap<String, String> getVariableTemporaire()
	{
		return variableTemporaire;
	}
	
	/**
	 * @return the variableTemporaire
	 */
	public HashMap<String, String> getMapVariableTemporaire()
	{
		return variableTemporaire;
	}
	
	public String getVariableTemporaire(String key)
	{
		return variableTemporaire.getOrDefault(key, "0");
	}
	
	/**
	 * @param variableTemporaire the variableTemporaire to set
	 */
	public void setVariableTemporaire(HashMap<String, String> variableTemporaire)
	{
		this.variableTemporaire = variableTemporaire;
	}
	
	public void addOrUpdateVariableTemporaire(String key, String value)
	{
		LOG.info(variableTemporaire.toString());
		variableTemporaire.put(key, value);
	}
	
	public void deletVariableTemporaire(String Key)
	{
		variableTemporaire.remove(Key);
	}
	
	public HashMap<String, String> restorPerennePlayer(int objectId)
	{
		return PerennePlayerStore.loadCustomVariable(objectId);
	}
	
	public void storPerennePlayer()
	{
		PerennePlayerStore.saveCustomVariable(_player);
		PerennePlayerStore.saveDesc(_player.getObjectId(), desc);
	}
	
	@Override
	public String toString()
	{
		String str = _player.getName();
		str += " " + _player.getAccountName();
		for (String g : variableTemporaire.keySet())
		{
			str += " " + variableTemporaire.get(g);
		}
		return str;
	}
	
	public void init(int objectId)
	{
		variableTemporaire = restorPerennePlayer(objectId);
		setDesc(PerennePlayerStore.loadDesc(objectId));
	}
	
}
