package com.perenneCustom.zonne;

import java.util.ArrayList;
import java.util.logging.Logger;

import com.l2jserver.gameserver.ThreadPoolManager;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.events.AbstractScript;
import com.l2jserver.gameserver.model.holders.SkillHolder;

public final class BuffVille
{
	public static final Logger _log = Logger.getLogger(AbstractScript.class.getName());
	static final SkillHolder REGEN_BOOST1 = new SkillHolder(50002, 1);
	static final SkillHolder REGEN_BOOST2 = new SkillHolder(50002, 2);
	static final SkillHolder REGEN_BOOST3 = new SkillHolder(50002, 3);
	static final SkillHolder REGEN_BOOST4 = new SkillHolder(50002, 4);
	static final SkillHolder REGEN_BOOST5 = new SkillHolder(50002, 5);
	static final SkillHolder REGEN_BOOST6 = new SkillHolder(50002, 6);
	
	private final ArrayList<L2PcInstance> playerList = new ArrayList<>();
	
	protected BuffVille()
	{
		ThreadPoolManager.getInstance().scheduleGeneralAtFixedRate(this::applyEffectOnPlayerList, 3000, 3000);
		_log.info("Ville : Regen task started");
	}
	
	public static BuffVille getInstance()
	{
		return SingletonHolder._instance;
	}
	
	private static class SingletonHolder
	{
		protected static final BuffVille _instance = new BuffVille();
	}
	
	public synchronized void applyEffectOnPlayerList()
	{
		for (L2PcInstance member : playerList)
		{
			switch (member.getPerennePlayer().getVariableTemporaire("regen_ville"))
			{
				case "1":
					REGEN_BOOST1.getSkill().applyEffects(member, member);
					break;
				case "2":
					REGEN_BOOST2.getSkill().applyEffects(member, member);
					break;
				case "3":
					REGEN_BOOST3.getSkill().applyEffects(member, member);
					break;
				case "4":
					REGEN_BOOST4.getSkill().applyEffects(member, member);
					break;
				case "5":
					REGEN_BOOST5.getSkill().applyEffects(member, member);
					break;
				case "6":
					REGEN_BOOST6.getSkill().applyEffects(member, member);
					break;
			}
		}
	}
	
	public ArrayList<L2PcInstance> getPlayerList()
	{
		return playerList;
	}
	
	public void addPlayerToPlayerList(L2PcInstance player)
	{
		this.playerList.add(player);
	}
	
	public void removePlayerFromPlayerList(L2PcInstance player)
	{
		this.playerList.remove(player);
	}
}
