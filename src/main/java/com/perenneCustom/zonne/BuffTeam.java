package com.perenneCustom.zonne;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import com.l2jserver.gameserver.ThreadPoolManager;
import com.l2jserver.gameserver.model.L2Party;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.base.ClassId;
import com.l2jserver.gameserver.model.holders.SkillHolder;

public final class BuffTeam
{
	static final SkillHolder MORALE_BOOST1 = new SkillHolder(6885, 1);
	static final SkillHolder MORALE_BOOST2 = new SkillHolder(6885, 2);
	static final SkillHolder MORALE_BOOST3 = new SkillHolder(6885, 3);
	// Misc
	private static final int MIN_DISTANCE = 1500;
	private static final int MIN_MEMBERS = 3;
	static final EnumMap<ClassId, Integer> CLASS_ROLE = new EnumMap<>(ClassId.class);
	{
		CLASS_ROLE.put(ClassId.adventurer, 0);
		CLASS_ROLE.put(ClassId.arcanaLord, 0);
		CLASS_ROLE.put(ClassId.archmage, 0);
		CLASS_ROLE.put(ClassId.cardinal, 1);
		CLASS_ROLE.put(ClassId.dominator, 2);
		CLASS_ROLE.put(ClassId.doombringer, 0);
		CLASS_ROLE.put(ClassId.doomcryer, 2);
		CLASS_ROLE.put(ClassId.dreadnought, 0);
		CLASS_ROLE.put(ClassId.duelist, 0);
		CLASS_ROLE.put(ClassId.elementalMaster, 0);
		CLASS_ROLE.put(ClassId.evaSaint, 1);
		CLASS_ROLE.put(ClassId.evaTemplar, 3);
		CLASS_ROLE.put(ClassId.femaleSoulhound, 0);
		CLASS_ROLE.put(ClassId.fortuneSeeker, 0);
		CLASS_ROLE.put(ClassId.ghostHunter, 0);
		CLASS_ROLE.put(ClassId.ghostSentinel, 0);
		CLASS_ROLE.put(ClassId.grandKhavatari, 0);
		CLASS_ROLE.put(ClassId.hellKnight, 3);
		CLASS_ROLE.put(ClassId.hierophant, 2);
		CLASS_ROLE.put(ClassId.judicator, 2);
		CLASS_ROLE.put(ClassId.moonlightSentinel, 0);
		CLASS_ROLE.put(ClassId.maestro, 0);
		CLASS_ROLE.put(ClassId.maleSoulhound, 0);
		CLASS_ROLE.put(ClassId.mysticMuse, 0);
		CLASS_ROLE.put(ClassId.phoenixKnight, 3);
		CLASS_ROLE.put(ClassId.sagittarius, 0);
		CLASS_ROLE.put(ClassId.shillienSaint, 1);
		CLASS_ROLE.put(ClassId.shillienTemplar, 3);
		CLASS_ROLE.put(ClassId.soultaker, 0);
		CLASS_ROLE.put(ClassId.spectralDancer, 2);
		CLASS_ROLE.put(ClassId.spectralMaster, 0);
		CLASS_ROLE.put(ClassId.stormScreamer, 0);
		CLASS_ROLE.put(ClassId.swordMuse, 2);
		CLASS_ROLE.put(ClassId.titan, 0);
		CLASS_ROLE.put(ClassId.trickster, 0);
		CLASS_ROLE.put(ClassId.windRider, 0);
	}
	static final EnumMap<ClassId, Double> CLASS_POINTS = new EnumMap<>(ClassId.class);
	{
		CLASS_POINTS.put(ClassId.adventurer, 0.2);
		CLASS_POINTS.put(ClassId.arcanaLord, 0.2);
		CLASS_POINTS.put(ClassId.archmage, 0.2);
		CLASS_POINTS.put(ClassId.cardinal, 1.0);
		CLASS_POINTS.put(ClassId.dominator, 0.5);
		CLASS_POINTS.put(ClassId.doombringer, 0.2);
		CLASS_POINTS.put(ClassId.doomcryer, 0.5);
		CLASS_POINTS.put(ClassId.dreadnought, 0.2);
		CLASS_POINTS.put(ClassId.duelist, 0.2);
		CLASS_POINTS.put(ClassId.elementalMaster, 0.2);
		CLASS_POINTS.put(ClassId.evaSaint, 1.0);
		CLASS_POINTS.put(ClassId.evaTemplar, 1.0);
		CLASS_POINTS.put(ClassId.femaleSoulhound, 0.2);
		CLASS_POINTS.put(ClassId.fortuneSeeker, 0.2);
		CLASS_POINTS.put(ClassId.ghostHunter, 0.2);
		CLASS_POINTS.put(ClassId.ghostSentinel, 0.2);
		CLASS_POINTS.put(ClassId.grandKhavatari, 0.2);
		CLASS_POINTS.put(ClassId.hellKnight, 1.0);
		CLASS_POINTS.put(ClassId.hierophant, 0.5);
		CLASS_POINTS.put(ClassId.judicator, 0.5);
		CLASS_POINTS.put(ClassId.moonlightSentinel, 0.2);
		CLASS_POINTS.put(ClassId.maestro, 0.2);
		CLASS_POINTS.put(ClassId.maleSoulhound, 0.2);
		CLASS_POINTS.put(ClassId.mysticMuse, 0.2);
		CLASS_POINTS.put(ClassId.phoenixKnight, 1.0);
		CLASS_POINTS.put(ClassId.sagittarius, 0.2);
		CLASS_POINTS.put(ClassId.shillienSaint, 1.0);
		CLASS_POINTS.put(ClassId.shillienTemplar, 1.0);
		CLASS_POINTS.put(ClassId.soultaker, 0.2);
		CLASS_POINTS.put(ClassId.spectralDancer, 0.5);
		CLASS_POINTS.put(ClassId.spectralMaster, 0.2);
		CLASS_POINTS.put(ClassId.stormScreamer, 0.2);
		CLASS_POINTS.put(ClassId.swordMuse, 0.5);
		CLASS_POINTS.put(ClassId.titan, 0.2);
		CLASS_POINTS.put(ClassId.trickster, 0.2);
		CLASS_POINTS.put(ClassId.windRider, 0.2);
	}
	@SuppressWarnings("unused")
	private static Logger _log = Logger.getLogger(BuffTeam.class.getName());
	
	protected BuffTeam()
	{
		ThreadPoolManager.getInstance().scheduleGeneralAtFixedRate(this::buffParty, 5000, 5000);
	}
	
	public static BuffTeam getInstance()
	{
		return SingletonHolder._instance;
	}
	
	public synchronized void buffParty()
	{
		// _log.info("run manageMoralBoost");
		double points = 0;
		boolean healer = false;
		boolean tank = false;
		Map<Integer, L2Party> partys = new HashMap<>();
		partys = L2World.getInstance().get_allPartys();
		for (L2Party p : partys.values())
		{
			points = 0;
			healer = false;
			tank = false;
			for (L2PcInstance player : p.getMembers())
			{
				if (player.isInParty() && (player.getParty().getMemberCount() >= MIN_MEMBERS) && (player.getPerennePlayer().getVariableTemporaire("moral_boost_check") == "1"))
				{
					for (L2PcInstance member : player.getParty().getMembers())
					{
						if (member.calculateDistance(p.getLeader(), true, false) < MIN_DISTANCE)
						{
							// Prise en compte de seulement un tank et un healer
							// pour éviter les abus :)
							switch (CLASS_ROLE.get(member.getClassId()))
							{
								case 0:
									points += CLASS_POINTS.get(member.getClassId());
									break;
								case 1:
									if (!healer)
									{
										points += CLASS_POINTS.get(member.getClassId());
										healer = true;
									}
									break;
								case 2:
									points += CLASS_POINTS.get(member.getClassId());
									break;
								case 3:
									if (!tank)
									{
										points += CLASS_POINTS.get(member.getClassId());
										tank = true;
									}
									break;
							}
						}
					}
					if (points >= 4)
					{
						applyEffectOnParty(p, 3);
						// _log.info("moraleBoostlv : " + moraleBoostLv);
					}
					else if (points >= 3)
					{
						applyEffectOnParty(p, 2);
						// _log.info("moraleBoostlv : " + moraleBoostLv);
					}
					else if (points >= 2)
					{
						applyEffectOnParty(p, 1);
						// _log.info("moraleBoostlv : " + moraleBoostLv);
					}
					
				}
			}
		}
		
	}
	
	private static class SingletonHolder
	{
		protected static final BuffTeam _instance = new BuffTeam();
	}
	
	public void applyEffectOnParty(L2Party party, int moralBoostLv)
	{
		for (L2PcInstance member : party.getMembers())
		{
			switch (moralBoostLv)
			{
				case 1:
					MORALE_BOOST1.getSkill().applyEffects(member, member);
					break;
				case 2:
					MORALE_BOOST2.getSkill().applyEffects(member, member);
					break;
				case 3:
					MORALE_BOOST3.getSkill().applyEffects(member, member);
					break;
			}
		}
		
	}
}
