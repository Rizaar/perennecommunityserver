/*
 * Copyright (C) 2004-2016 L2J Server
 * 
 * This file is part of L2J Server.
 * 
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.perenneCustom;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.pool.impl.ConnectionFactory;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;

/**
 * @author Rizaar
 */
public class PerennePlayerStore
{
	private static final String LOAD_CUSTOM_VARIABLE = "SELECT * FROM CUSTOM_VARIABLE WHERE ID = ?";
	private static final String LOAD_DESC = "SELECT * FROM CUSTOM_DESCRIPTION WHERE object_id = ?";
	private static final String UPDATE_CUSTOM_VARIABLE = "REPLACE INTO CUSTOM_VARIABLE (ID,name,value) VALUES(?,?,?)";
	private static final String UPDATE_DESC = "REPLACE INTO CUSTOM_DESCRIPTION (object_id, description) VALUES(?,?)";
	private static final String DELETE_QUERY = "DELETE FROM CUSTOM_VARIABLE WHERE ID = ?";
	
	private static final Logger LOG = LoggerFactory.getLogger(PerennePlayerStore.class);
	
	public static HashMap<String, String> loadCustomVariable(int objectId)
	{
		HashMap<String, String> variableTemporaire = new HashMap<>();
		try (Connection con = ConnectionFactory.getInstance().getConnection();
			PreparedStatement ps = con.prepareStatement(LOAD_CUSTOM_VARIABLE))
		{
			ps.setInt(1, objectId);
			try (ResultSet rs = ps.executeQuery())
			{
				while (rs.next())
				{
					variableTemporaire.put(rs.getString("name"), rs.getString("value"));
				}
			}
			catch (Exception e)
			{
				LOG.error("Could not restore custom Variable : {}", e);
			}
		}
		catch (Exception e)
		{
			LOG.error("Could not restore custom Variable : {}", e);
		}
		return variableTemporaire;
	}
	
	public static String loadDesc(int objectId)
	{
		String str = "";
		try (Connection con = ConnectionFactory.getInstance().getConnection();
			PreparedStatement ps = con.prepareStatement(LOAD_DESC))
		{
			// Retrieve all skills of this L2PcInstance from the database
			ps.setInt(1, objectId);
			try (ResultSet rs = ps.executeQuery())
			{
				while (rs.next())
				{
					str = rs.getString("description");
				}
			}
		}
		catch (Exception e)
		{
			LOG.error("Could not restore custom Desc : {}", e);
		}
		return str;
	}
	
	public static void saveDesc(int objectId, String desc)
	{
		try (Connection con = ConnectionFactory.getInstance().getConnection())
		{
			
			try (PreparedStatement ps = con.prepareStatement(UPDATE_DESC))
			{
				ps.setInt(1, objectId);
				ps.setString(2, desc);
				ps.execute();
			}
			
		}
		catch (Exception e)
		{
			LOG.warn("Error could not store custom Description: {}", e);
		}
	}
	
	public static void saveCustomVariable(L2PcInstance activeChar)
	{
		try (Connection con = ConnectionFactory.getInstance().getConnection())
		{
			try (PreparedStatement st = con.prepareStatement(DELETE_QUERY))
			{
				st.setInt(1, activeChar.getObjectId());
				st.execute();
			}
			
			for (String s : activeChar.getPerennePlayer().getVariableTemporaire().keySet())
			{
				try (PreparedStatement ps = con.prepareStatement(UPDATE_CUSTOM_VARIABLE))
				{
					ps.setInt(1, activeChar.getObjectId());
					ps.setString(2, s);
					ps.setString(3, activeChar.getPerennePlayer().getVariableTemporaire(s.toString()));
					ps.execute();
				}
				catch (Exception e)
				{
					LOG.error("Could not store custom Variable : {}", e);
				}
			}
		}
		catch (Exception e)
		{
			LOG.warn("Error could not store custom Variable: {}", e);
		}
	}
}
